jQuery(document).ready(function ($) {
	/**
	 * Register From
	 */
	if ($('form.register').length) {
		let paramsUrl = location.search;
		let params = new URLSearchParams(paramsUrl);
		
		let success = params.get('success');
		let error = params.get('error');
		if (success) {
			Swal.fire({
				icon: 'success',
				title: 'Success',
				text: success,
			}).then((result) => {
				if (result.isConfirmed) {
					location.href = $('#iwp_to_redirect').val();
				}
			})
		}
		
		if (error) {
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: error,
			}).then((result) => {
				if (result.isConfirmed) {
				}
			})
		}
	}
	
	/**
	 * Ajax Upload Avatar
	 */
	if ($('#add-photo').length) {
		$('#add-photo').change(function (e) {
			let formData = new FormData();
			formData.append('file', $(this)[ 0 ].files[ 0 ]);
			formData.append('action', 'upload_avatar');
			formData.append('userID', $(this).data('userid'));
			
			$.ajax({
				type: 'POST',
				url: iwp.ajaxURL,
				data: formData,
				contentType: false,
				async: true,
				cache: false,
				processData: false,
				timeout: 60000,
				success: function (data) {
					if (data.success) {
						$('#add-photo').hide();
						$('label.icon-photo').hide();
						$('.user-photo').append(`<img src='${data.data.urlAvatar}'>`);
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: data.data.message,
						});
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr)
				},
			});
		});
	}
	
	/**
	 * Remove Avatar
	 */
	if ($('#remove-avatar').length) {
		$('#remove-avatar').click(function (e) {
			e.preventDefault();
			
			let data = {
				action: 'remove_avatar',
				userID: $(this).data('userid'),
				attachmentID: $(this).data('avatarid')
			}
			
			$.ajax({
				type: 'POST',
				url: iwp.ajaxURL,
				data: data,
				success: function (data) {
					if (data.success) {
						location.reload();
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: data.data.message,
						});
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log('error...', xhr);
					//error logging
				},
			});
		});
	}
	
});