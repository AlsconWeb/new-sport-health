<?php
/**
 * Created 19.06.2021
 * Version 1.0.1
 * Last update 01.08.21
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */


use IWP\Admin\IWPAdminHelpers;
use IWP\Helpers\IWPBakeryHelpers;

$helpers            = new IWPAdminHelpers();
$params             = $helpers->getParamsParse( $_GET );
$countOutput        = 10;
$info               = $helpers->getUserList( $countOutput, (int) isset( $_GET['pages'] ) ? $_GET['pages'] : null, $params );
$day                = $helpers->userStatisticFromData( 'day' );
$month              = $helpers->userStatisticFromData( 'month' );
$year               = $helpers->userStatisticFromData( 'year' );
$country            = new IWPBakeryHelpers();
$uniqueOrganization = $helpers->getUniqueOrganization();


?>
<h1><?php _e( 'User Statistic', 'iwp' ); ?></h1>
<h4><?php _e( 'Day:', 'iwp' ); ?>
	<span class="badge bg-secondary"><?php echo $day; ?></span>
	<?php _e( 'Month:', 'iwp' ); ?> <span
	class="badge bg-secondary"><?php echo $month ?></span>
	<?php _e( 'Year:', 'iwp' ); ?> <span class="badge bg-secondary"><?php echo $year ?></span></h4>
<h1><?php _e( 'User List of Conference', 'iwp' ) ?></h1>
<form method="post" action="<?php echo admin_url( 'admin-post.php' ); ?>" class="mt-5 mb-3">
	<div class="row">
		<div class="col">
			<select class="form-select" name="iwp_filter[user_registered]">
				<option value="0" selected><?php _e( 'By registration date', 'iwp' ); ?></option>
				<option
				value="asc" <?php echo isset( $params['user_registered'] ) && 'asc' === $params['user_registered'] ? 'selected' : '' ?>><?php _e( 'From old to new', 'iwp' ); ?></option>
				<option
				value="desc" <?php echo isset( $params['user_registered'] ) && 'desc' === $params['user_registered'] ? 'selected' : '' ?>><?php _e( 'From new to old', 'iwp' ); ?></option>
			</select>
		</div>
		<div class="col">
			<select class="form-select" name="iwp_filter[country]">
				<option value="0" selected><?php _e( 'By Country', 'iwp' ); ?></option>
				<?php foreach ( $country->countryList() as $key => $item ): ?>
					<option
					value="<?php echo $key; ?>" <?php echo isset( $params['country'] ) && $key === $params['country'] ? 'selected' : '' ?>><?php echo $key; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="col">
			<select type="text" name="iwp_filter[form_participation]" class="form-select" id="iwp_form_participation"
			required>
				<option value="0" selected><?php _e( 'Choose a Form of participation', 'iwp' ); ?></option>
				<option
				value="full_time" <?php echo isset( $params['form_participation'] ) && 'full_time' === $params['form_participation'] ? 'selected' : '' ?>><?php _e( 'Full time', 'iwp' ); ?></option>
				<option
				value="extramural" <?php echo isset( $params['form_participation'] ) && 'extramural' === $params['form_participation'] ? 'selected' : '' ?>><?php _e( 'In-absentia', 'iwp' ); ?></option>
				<option
				value="listener" <?php echo isset( $params['form_participation'] ) && 'listener' === $params['form_participation'] ? 'selected' : '' ?>><?php _e( 'Online', 'iwp' ); ?></option>
			</select>
		</div>
		<div class="col">
			<select class="form-select" name="iwp_filter[organization]">
				<option value="0" selected><?php _e( 'Select Organization', 'iwp' ); ?></option>
				<?php foreach ( $uniqueOrganization as $item ): ?>
					<option
					value="<?php echo $item->meta_value ?>" <?php echo isset( $params['organization'] ) && $item->meta_value === $params['organization'] ? 'selected' : '' ?>><?php echo $item->meta_value ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="col">
			<input type="hidden" name="action" value="iwp_filter_user"/>
			<?php wp_nonce_field( 'iwp_filter_user_form', 'iwp_filter_user_nonce' ); ?>
			<input type="submit" class="btn btn-primary" value="<?php _e( 'Filter', 'iwp' ); ?>">
			<button type="reset" class="btn btn-secondary clean"><?php echo esc_html__( "Clean Filter", 'iwp' ); ?></button>
			<button type="button" class="btn btn-secondary generate-exel-user-list"><?php echo esc_html__( "Generate EXEL",
				'iwp' );
				?></button>
		</div>
	</div>
</form>
<table class="table table-hover">
	<thead>
	<tr>
		<th scope="col"><?php _e( 'User ID', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Full Name', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Payment', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Actions', 'iwp' ); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php if ( ! empty( $info ) ): ?>
		<?php foreach ( $info as $key => $item ): ?>
			<?php $paymentStatus = $helpers->getPaymentStatusByUserID( (int) $key );
			$userEmail           = get_userdata( $key )->user_email; ?>
			<tr>
				<th scope="row"><?php echo $key ?></th>
				<td><?php echo $item['first_name'] . ' ' . $item['last_name'] ?></td>
				<td>
					<?php if ( 'pending' === $paymentStatus ): ?>
						<span class="badge bg-primary"><?php echo $paymentStatus; ?></span>
					<?php endif; ?>
					<?php if ( 'completed' === $paymentStatus ): ?>
						<span class="badge bg-success"><?php echo $paymentStatus; ?></span>
					<?php endif; ?>
					<?php if ( 'rejects' === $paymentStatus ): ?>
						<span class="badge bg-danger"><?php echo $paymentStatus; ?></span>
					<?php endif; ?>
				</td>
				<td>
					<button type="button" class="btn btn-primary edit-btn"
					data-userid="<?php echo $key ?>"><?php _e( 'Edit', 'iwp' ); ?></button>
				</td>
			</tr>
			<tr class="info-user" style="display: none;" data-uid="<?php echo $key ?>">
				<td colspan="4">
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_fio_ru-<?php echo $key ?>"
							class="form-label"><?php _e( 'Last name, first name in Russian', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[fio_ru]" class="form-control" id="iwp_fio_ru-<?php echo $key ?>"
							value="<?php echo $item['first_name'] . ' ' . $item['last_name'] . ' ' . $item['patronymic'] ?>">
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_fio_en-<?php echo $key ?>"
							class="form-label"><?php _e( 'Last name, first name in English', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[fio_en]" class="form-control" id="iwp_fio_en-<?php echo $key ?>"
							value="<?php echo $item['fio_en'] ?? ''; ?>">
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_dob-<?php echo $key ?>"
							class="form-label"><?php _e( 'Date of Birth', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[dob]" id="iwp_dob-<?php echo $key ?>" class="form-control"
							value="<?php echo $item['dob'] ?? ''; ?>">
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_gender-<?php echo $key ?>"
							class="form-label"><?php _e( 'Gender', 'iwp' ); ?></label>
							<select name="iwp_userinfo[gender]" class="form-control" id="iwp_gender-<?php echo $key ?>">
								<option
								value="male" <?php echo isset( $item['gender'] ) && 'male' === $item['gender'] ? 'selected' : '' ?>><?php _e( 'Male', 'iwp' ); ?></option>
								<option
								value="female" <?php echo isset( $item['gender'] ) && 'female' === $item['gender'] ? 'selected' : '' ?>><?php _e( 'Female', 'iwp' ); ?></option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_appeal-<?php echo $key ?>"
							class="form-label"><?php _e( 'Appeal (Mr., Ms. or any other)', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[appeal]" class="form-control" id="iwp_appeal-<?php echo $key ?>"
							value="<?php echo $item['appeal'] ?? '' ?>">
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_country-<?php echo $key ?>"
							class="form-label"><?php _e( 'Country of Residence', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[country]" class="form-control" id="iwp_country-<?php echo $key ?>"
							value="<?php echo $item['country'] ?? ''; ?>">
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_province-<?php echo $key ?>"
							class="form-label"><?php _e( 'Province', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[province]" class="form-control" id="iwp_province-<?php echo $key ?>"
							value="<?php echo $item['province'] ?? '' ?>">
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_city-<?php echo $key ?>"
							class="form-label"><?php _e( 'City', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[city]" class="form-control" id="iwp_city-<?php echo $key ?>"
							value="<?php echo $item['city'] ?? ''; ?>">
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_phone-<?php echo $key ?>"
							class="form-label"><?php _e( 'Contact number', 'iwp' ); ?></label>
							<input type="tel" name="iwp_userinfo[phone]" class="form-control" id="iwp_phone-<?php echo $key ?>"
							value="<?php echo $item['phone'] ?? '' ?>" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_email-<?php echo $key ?>"
							class="form-label"><?php _e( 'Email', 'iwp' ); ?></label>
							<input type="email" name="iwp_userinfo[email]" class="form-control disabled"
							id="iwp_email-<?php echo $key ?>"
							value="<?php echo $userEmail ?? ''; ?>" disabled>
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_work-<?php echo $key ?>"
							class="form-label"><?php _e( 'Place of work / study (in full)', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[work]" class="form-control" id="iwp_work-<?php echo $key ?>"
							value="<?php echo $item['work'] ?? '' ?>">
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_organization-<?php echo $key ?>"
							class="form-label"><?php _e( 'Structural division of the organization (in full)', 'iwp' ) ?></label>
							<input type="text" name="iwp_userinfo[organization]" class="form-control"
							id="iwp_organization-<?php echo $key ?>"
							value="<?php echo $item['organization'] ?? ''; ?>">
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_degree-<?php echo $key ?>"
							class="form-label"><?php _e( 'Academic title / Degree / Honorary sports title', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[degree]" class="form-control" id="iwp_degree-<?php echo $key ?>"
							value="<?php echo $item['degree'] ?? '' ?>">
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_position-<?php echo $key ?>"
							class="form-label"><?php _e( 'Position (in full)', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[position]" class="form-control"
							id="iwp_position-<?php echo $key ?>"
							value="<?php echo $item['position'] ?? ''; ?>">
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_article_title-<?php echo $key ?>"
							class="form-label"><?php _e( 'Paper title (in full)', 'iwp' ); ?></label>
							<textarea name="iwp_userinfo[article_title]" class="form-control"
							id="iwp_article_title-<?php echo $key ?>"><?php echo $item['article_title'] ?? '' ?></textarea>
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_coauthors-<?php echo $key ?>"
							class="form-label"><?php _e( 'Coauthors separated by commas (full name in Russian and English)', 'iwp' ); ?></label>
							<textarea name="iwp_userinfo[coauthors]" class="form-control"
							id="iwp_coauthors-<?php echo $key ?>"><?php echo $item['coauthors'] ?? '' ?></textarea>
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_congress_section-<?php echo $key ?>"
							class="form-label"><?php _e( 'Number and title of the Congress section', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[congress_section]" class="form-control"
							id="iwp_congress_section-<?php echo $key ?>"
							value="<?php echo $item['congress_section'] ?? '' ?>">
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_form_participation-<?php echo $key ?>"
							class="form-label"><?php _e( 'Form of participation', 'iwp' ); ?></label>
							<select type="text" name="iwp_userinfo[form_participation]" class="form-control"
							id="iwp_form_participation-<?php echo $key ?>">
								<option value="0"><?php _e( 'Choose a Form of participation', 'iwp' ); ?></option>
								<option
								value="full_time" <?php echo isset( $item['form_participation'] ) && 'full_time' === $item['form_participation'] ? 'selected' : ''
								?>><?php _e(
									'Full time', 'iwp' ); ?></option>
								<option
								value="extramural" <?php echo isset( $item['form_participation'] ) && 'extramural' === $item['form_participation'] ? 'selected' : ''
								?>><?php _e( 'In-absentia', 'iwp' ); ?></option>
								<option
								value="listener" <?php echo isset( $item['form_participation'] ) && 'listener' === $item['form_participation'] ? 'selected' : '' ?>><?php _e( 'Online', 'iwp' ); ?></option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_form_performance-<?php echo $key ?>"
							class="form-label"><?php _e( 'Form of performance', 'iwp' ); ?></label>
							<select type="text" name="iwp_userinfo[form_performance]" class="form-control"
							id="iwp_form_performance-<?php echo $key ?>">
								<option value="0"><?php _e( 'Choose a presentation format', 'iwp' ); ?></option>
								<option
								value="oral" <?php echo isset( $item['form_performance'] ) && 'oral' === $item['form_performance'] ?
								'selected' : '' ?>><?php _e( 'Oral presentation', 'iwp' ); ?></option>
								<option
								value="poster" <?php echo isset( $item['form_performance'] ) && 'poster' === $item['form_performance'] ? 'selected' : '' ?>><?php _e( 'Poster presentation', 'iwp' ); ?></option>
							</select>
						</div>
						<div class="mb-3 col-6">
							<label for="iwp_technical-<?php echo $key ?>"
							class="form-label"><?php _e( 'The need for technical means', 'iwp' ) ?></label>
							<select type="text" name="iwp_userinfo[technical]" class="form-control"
							id="iwp_technical-<?php echo $key ?>">
								<option value="0"><?php _e( 'Select a technical', 'iwp' ); ?></option>
								<option
								value="yes" <?php echo isset( $item['technical'] ) && 'yes' === $item['technical'] ? 'selected' : ''
								?>><?php _e( 'Yes', 'iwp' );
									?></option>
								<option
								value="no" <?php echo isset( $item['technical'] ) && 'no' === $item['technical'] ? 'selected' : '' ?>><?php _e( 'No', 'iwp' );
									?></option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="mb-3 col-6">
							<label for="iwp_technical_list-<?php echo $key ?>"
							class="form-label"><?php _e( 'List of technical means', 'iwp' ); ?></label>
							<input type="text" name="iwp_userinfo[technical_list]" class="form-control"
							id="iwp_technical_list-<?php echo $key ?>"
							value="<?php echo $item['technical_list'] ?? '' ?>">
						</div>
					</div>
					<div class="row">
						<div class="col-3">
							<button type="button" class="btn btn-primary save-user-info"><?php _e( 'Save', 'iwp' ); ?></button>
						</div>
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
<nav>
	<?php $numberPage = $helpers->getUserListCountPage( $countOutput ) ?>
	<?php if ( $numberPage ): ?>
		<ul class="pagination">
			<?php for ( $i = 0; $i < $numberPage; $i ++ ): ?>
				<li class="page-item">
					<a class="page-link"
					href="/wp-admin/admin.php?page=iwp-user-list&pages=<?php echo $i + 1 ?>"><?php echo $i + 1; ?></a>
				</li>
			<?php endfor; ?>
		</ul>
	<?php endif; ?>
</nav>