<?php
/**
 * Created 17.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

use IWP\Admin\IWPAdminHelpers;

$helpers     = new IWPAdminHelpers();
$countOutput = 50;
$info        = $helpers->getReportDocs( $countOutput, (int) isset( $_GET['pages'] ) ? $_GET['pages'] : NULL )
?>
<h1><?php _e( 'Report Docs', 'iwp' ) ?></h1>
<div class="row">
	<div class="col">
		<button type="button" class="btn btn-secondary generate-exel-reports-doc"><?php echo esc_html__( "Generate EXEL",
				'iwp' );
			?></button>
	</div>
</div>
<table class="table table-hover" id="report-doc">
	<thead>
	<tr>
		<th scope="col"><?php _e( 'User ID', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Full Name', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Status', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'File Url', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Comment count', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Actions', 'iwp' ); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php if ( ! empty( $info ) ): ?>
		<?php foreach ( $info as $item ): ?>
			<?php
			$userHelpers = new \IWP\Helpers\User\IWPUserHelpers( $item->userID );
			$userInfo    = $userHelpers->getUserInfo();
			$url         = wp_get_attachment_url( $item->docID, 'full' );
			$comments    = $helpers->getCommentByDocID( $item->docID );
			?>
			<tr data-doc_comment="<?php echo ! empty( $comments ) ? count( $comments ) : 0; ?>"
			    data-doc="<?php echo $item->docID; ?>">
				<th scope="row"><?php echo $item->userID; ?></th>
				<td><?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name']; ?></td>
				<td>
					<?php if ( 'draft' === $item->status ): ?>
						<span class="badge bg-primary"><?php echo $item->status; ?></span>
					<?php endif; ?>
					<?php if ( 'publish' === $item->status ): ?>
						<span class="badge bg-success"><?php echo $item->status; ?></span>
					<?php endif; ?>
					<?php if ( 'rejects' === $item->status ): ?>
						<span class="badge bg-danger"><?php echo $item->status; ?></span>
					<?php endif; ?>
				</td>
				<td><a href="<?php echo $url; ?>" target="_blank"><?php _e( 'File Link', 'iwp' ); ?></a></td>
				<td><span class="badge bg-secondary"><?php echo ! empty( $comments ) ? count( $comments ) : 0; ?></span></td>
				<td>
					<div class="row">
						<div class="col-8">
							<select id="select-status-<?php echo $item->userID; ?>" class="form-select">
								<option selected><?php _e( 'Select a status', 'iwp' ); ?></option>
								<option value="draft"><?php _e( 'draft', 'iwp' ); ?></option>
								<option value="publish"><?php _e( 'publish', 'iwp' ); ?></option>
								<option value="rejects"><?php _e( 'rejects', 'iwp' ); ?></option>
								<option value="delete"><?php _e( 'delete', 'iwp' ); ?></option>
							</select>
						</div>
						<div class="col-4">
							<button type="button" data-docid="<?php echo $item->docID; ?>"
							        data-userid="<?php echo $item->userID; ?>"
							        class="btn btn-primary action-report-btn"><?php _e( 'Save', 'iwp' ); ?>
							</button>
						</div>
					</div>
				</td>
			</tr>
			<?php if ( ! empty( $comments ) ): ?>
				<tr class="comments" data-doc-id="<?php echo $item->docID ?>" data-userid="<?php echo $item->userID ?>"
				    style="display: none">
					<td colspan="4">
						<div class="list-group">
							<?php foreach ( $comments as $key => $comment ): ?>
								<div class="list-group-item list-group-item-action"
								     aria-current="true">
									<div class="d-flex w-100 justify-content-between">
										<?php if ( user_can( $comment->userID, 'manage_options' ) ): ?>
											<h5 class="mb-1"><?php _e( 'Moderator', 'iwp' ); ?></h5>
											<small><?php echo mysql2date( 'd.m.Y', $comment->date ); ?></small>
										<?php else: ?>
											<h5 class="mb-1"><?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name']; ?></h5>
											<small><?php echo mysql2date( 'd.m.Y', $comment->date ); ?></small>
										<?php endif; ?>
									</div>
									<p class="mb-1"><?php echo $comment->comment; ?></p>
									<?php if ( ! user_can( $comment->userID, 'manage_options' ) ): ?>
										<a href="#" class="repay-comment"
										   data-comment="<?php echo $comment->id; ?>">
											<?php _e( 'Replay', 'iwp' ); ?>
										</a>
										<div class="replay-comment-input mt-3" style="display: none">
										<textarea name="iwp_replay_comment" class="form-control mb-3" cols="30"
										          rows="5"></textarea>
											<button type="button" class="iwp_replay_comment_btn btn btn-primary"
											        data-comment="<?php echo $comment->id; ?>">
												<?php _e( 'Send', 'iwp' ); ?>
											</button>
										</div>
									<?php endif; ?>
								</div>
								
								<?php $childComments = $helpers->getChildComment( $comment->id ); ?>
								<?php if ( $childComments ): ?>
									<?php foreach ( $childComments as $childComment ): ?>
										<div class="list-group-item list-group-item-action ms-3"
										     aria-current="true">
											<div class="d-flex w-100 justify-content-between">
												<?php if ( user_can( $childComment->userID, 'manage_options' ) ): ?>
													<h5 class="mb-1"><?php _e( 'Moderator', 'iwp' ); ?></h5>
													<small><?php echo mysql2date( 'd.m.Y', $childComment->date ); ?></small>
												<?php else: ?>
													<h5 class="mb-1"><?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name']; ?></h5>
													<small><?php echo mysql2date( 'd.m.Y', $childComment->date ); ?></small>
												<?php endif; ?>
											</div>
											<p class="mb-1"><?php echo $childComment->comment; ?></p>
										</div>
									<?php endforeach; ?>
								<?php endif; ?>
							<?php endforeach; ?>
						</div>
					</td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
<nav>
	<?php $numberPage = $helpers->getReportsDocCountPage( $countOutput ) ?>
	<?php if ( $numberPage ): ?>
		<ul class="pagination">
			<?php for ( $i = 0; $i < $numberPage; $i ++ ): ?>
				<li class="page-item">
					<a class="page-link"
					   href="/wp-admin/admin.php?page=iwp-report-doc&pages=<?php echo $i + 1 ?>"><?php echo $i + 1; ?></a>
				</li>
			<?php endfor; ?>
		</ul>
	<?php endif; ?>
</nav>