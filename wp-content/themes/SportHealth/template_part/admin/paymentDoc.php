<?php
/**
 * Created 16.06.2021
 * Version 1.0.1
 * Last update 01.08.21
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

use IWP\Admin\IWPAdminHelpers;

$helpers     = new IWPAdminHelpers();
$countOutput = 50;
$info        = $helpers->getPaymentRequestInfo( $countOutput, (int) isset( $_GET['pages'] ) ? $_GET['pages'] : NULL );
?>
<h1><?php _e( 'Payment Docs Request', 'iwp' ); ?></h1>
<div class="row">
	<div class="col">
		<button type="button" class="btn btn-secondary generate-exel-payment-doc"><?php echo esc_html__( "Generate EXEL",
				'iwp' );
			?></button>
	</div>
</div>
<table class="table">
	<thead>
	<tr>
		<th scope="col"><?php _e( 'User ID', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Full Name', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Status', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'File Url', 'iwp' ); ?></th>
		<th scope="col"><?php _e( 'Actions', 'iwp' ); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php if ( ! empty( $info ) ): ?>
		<?php foreach ( $info as $item ): ?>
			<?php
			$userHelpers = new \IWP\Helpers\User\IWPUserHelpers( $item->userID );
			$userInfo    = $userHelpers->getUserInfo();
			$url         = wp_get_attachment_url( $item->file, 'full' );
			?>
			
			<tr>
				<th scope="row"><?php echo $item->userID; ?></th>
				<td><?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name']; ?></td>
				<td>
					<?php if ( 'pending' === $item->status ): ?>
						<span class="badge bg-primary"><?php echo $item->status; ?></span>
					<?php endif; ?>
					<?php if ( 'completed' === $item->status ): ?>
						<span class="badge bg-success"><?php echo $item->status; ?></span>
					<?php endif; ?>
					<?php if ( 'rejects' === $item->status ): ?>
						<span class="badge bg-danger"><?php echo $item->status; ?></span>
					<?php endif; ?>
				</td>
				<td><a href="<?php echo $url; ?>" target="_blank"><?php _e( 'File Link', 'iwp' ); ?></a></td>
				<td>
					<div class="row">
						<div class="col-8">
							<select id="select-status-<?php echo $item->userID; ?>" class="form-select">
								<option selected><?php _e( 'Select a status', 'iwp' ); ?></option>
								<option value="pending"><?php _e( 'pending', 'iwp' ); ?></option>
								<option value="completed"><?php _e( 'completed', 'iwp' ); ?></option>
								<option value="rejects"><?php _e( 'rejects', 'iwp' ); ?></option>
								<option value="delete"><?php _e( 'delete', 'iwp' ); ?></option>
							</select>
						</div>
						<div class="col-4">
							<button type="button" data-userid="<?php echo $item->userID; ?>"
							        class="btn btn-primary action-btn"><?php _e( 'Save', 'iwp' ); ?>
							</button>
						</div>
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
<nav>
	<?php $numberPage = $helpers->getPaymentRequestCountPage( $countOutput ) ?>
	<?php if ( $numberPage ): ?>
		<ul class="pagination">
			<?php for ( $i = 0; $i < $numberPage; $i ++ ): ?>
				<li class="page-item">
					<a class="page-link"
					   href="/wp-admin/admin.php?page=iwp-payment-doc&pages=<?php echo $i + 1 ?>"><?php echo $i + 1; ?></a>
				</li>
			<?php endfor; ?>
		</ul>
	<?php endif; ?>
</nav>
