<?php
/**
 * Created 09.06.2021
 * Version 1.0.1
 * Last update 01.08.21
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

use IWP\Helpers\User\IWPUserHelpers;
use IWP\User\IWPUserDocs;

$headLine    = get_theme_mod( 'customizer_profile_form_headline' );
$fileSize    = get_theme_mod( 'customizer_profile_form_file_size' );
$fileFormat  = get_theme_mod( 'customizer_profile_form_formats' );
$size        = (int) $fileSize * pow( 1024, 2 );
$email       = get_theme_mod( 'customizer_profile_form_email_to' );
$userID      = get_current_user_id();
$countOutput = 50;
$userDocs    = new IWPUserDocs();
$docs        = $userDocs->getUserDocs( $userID );
$userHelpers = new IWPUserHelpers( $userID );
$userInfo    = $userHelpers->getUserInfo();
?>

<div class="tab-pane fade" id="list-doc">
	<form name="send_doc_from" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post"
	      id="send_doc_from" enctype="multipart/form-data">
		<h4><?php echo $headLine ?? '' ?></h4>
		<p><?php _e( 'File size up to', 'iwp' ); ?> <?php echo $fileSize ?? '2' ?> <?php _e( 'MB', 'iwp' ); ?>
			. <?php echo $fileFormat ?? '' ?> </p>
		<div class="docs">
			<?php if ( ! empty( $docs ) ): ?>
				<?php foreach ( $docs as $doc ): ?>
					<?php
					$fileName = get_post( $doc->docID );
					$status   = $userDocs->getDocStatus( $doc->status );
					$count    = $userDocs->getCountNewComment( $userID, $doc->docID );
					$comments = $userDocs->getCommentsForDoc( $doc->docID );
					?>
					<div class="item">
						<div class="head">
							<h5><?php echo $fileName->post_title; ?></h5>
							<ul>
								<li><a class="icon-edit"
								       data-docid="<?php echo $doc->docID; ?>"
								       href="#"><?php _e( 'Leave a comment', 'iwp' ); ?></a></li>
								<li><a class="icon-delete" data-docid="<?php echo $doc->docID; ?>"
								       href="#"><?php _e( 'Delete', 'iwp' ); ?></a></li>
							</ul>
							<p class="status"><?php echo $status ?></p>
							<i class="icon-comments" data-comments="<?php echo $count ?>"></i>
						</div>
						<?php if ( $comments ): ?>
							<?php foreach ( $comments as $comment ): ?>
								
								<div class="content-message">
									<ul class="meta">
										<li><?php echo mysql2date( 'd.m.Y', $comment->date ); ?></li>
										<li><?php echo mysql2date( 'H:i', $comment->date ) ?></li>
										<li><?php echo user_can( $comment->userID, 'manage_options' ) ? __( 'Moderator', 'iwp' )
												: $userInfo['first_name'] . ' ' . $userInfo['last_name']; ?></li>
									</ul>
									<p><?php echo $comment->comment; ?></p>
									<?php if ( user_can( $comment->userID, 'manage_options' ) ): ?>
										<a href="#" class="replay-comment"
										   id="replay-comment-<?php echo $comment->id ?>"><?php _e( 'Replay', 'iwp' ); ?></a>
									<?php endif; ?>
								</div>
								<?php $childComments = $userDocs->getChildComment( $comment->id ); ?>
								<?php if ( $childComments ): ?>
									<?php foreach ( $childComments as $childComment ): ?>
										<div class="content-message" style="margin-left:20px; ">
											<ul class="meta">
												<li><?php echo mysql2date( 'd.m.Y', $childComment->date ); ?></li>
												<li><?php echo mysql2date( 'H:i', $childComment->date ) ?></li>
												<li><?php echo user_can( $childComment->userID, 'manage_options' ) ? __( 'Moderator', 'iwp' )
														: $userInfo['first_name'] . ' ' . $userInfo['last_name']; ?></li>
											</ul>
											<p><?php echo $childComment->comment; ?></p>
											<a href="#" class="repay-comment"
											   data-comment="<?php echo $comment->id; ?>">
												<?php _e( 'Replay', 'iwp' ); ?>
											</a>
											<div class="replay-comment-input mt-3" style="display: none">
												<textarea name="iwp_replay_comment" class="form-control mb-3" cols="30"
												          rows="5"></textarea>
												<button type="button" class="iwp_replay_comment_btn button"
												        data-docid="<?php echo $childComment->docID ?>"
												        data-userid="<?php echo $userID; ?>"
												        data-comment="<?php echo $comment->id; ?>">
													<?php _e( 'Send', 'iwp' ); ?>
												</button>
											</div>
										</div>
									<?php endforeach; ?>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<div class="input" style="display: none;" id="comment_field">
			<input type="text" maxlength="100" name="iwp_file_comment" id="iwp_file_comment">
			<label><?php _e( 'Comment', 'iwp' ); ?></label>
			<a href="#" class="button" id="send-comment"
			   data-docid=""
			   data-parent-comment=""
			   data-userid="<?php echo $userID; ?>"
			><?php _e( 'Send Comment', 'iwp' ); ?></a>
		</div>
		<div class="file">
			<i class="icon-document"></i>
			<div class="dfc">
				<input id="add-file" type="file" name="iwp_file" accept="<?php echo $fileFormat ?? ''; ?>"
				       size="<?php echo $size ?? '' ?>">
				<label for="add-file"><?php _e( 'Upload file', 'iwp' ); ?></label>
				<p><?php _e( 'No file selected', 'iwp' ); ?></p>
			</div>
		</div>
		<input type="hidden" name="action" value="send_doc_from"/>
		<input type="hidden" name="iwp_to_email" value="<?php echo $email ?? get_option( 'admin_email', true ) ?>"/>
		<input type="hidden" name="iwp_size" value="<?php echo $size ?? '' ?>"/>
		<input type="hidden" name="iwp_user_id" value="<?php echo $userID; ?>"/>
		<?php wp_nonce_field( 'iwp_doc_from', 'iwp_doc_from_nonce' ); ?>
		<input class="button" type="submit" value="<?php _e( 'send', 'iwp' ); ?>">
	</form>
</div>
