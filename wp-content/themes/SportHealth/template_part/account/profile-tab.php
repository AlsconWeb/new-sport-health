<?php
/**
 * Created 09.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

?>
<div class="tab-pane active" id="profil" role="tabpanel" aria-labelledby="home-tab">
	<h3 style="background:#2263A8;"><?php _e( 'Personal data', 'iwp' ); ?></h3>
	<div class="profile-block">
		<ul class="profile">
			<li><?php _e( 'Full name in Russian', 'iwp' ); ?>
				<span><?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name'] . ' ' . $userInfo['patronymic'] ?></span>
			</li>
			<li><?php _e( 'Last name, first name in English', 'iwp' ) ?>
				<span><?php echo $userInfo['fio_en'] ?? _e( 'Not indicated', 'iwp' ); ?></span></li>
			<li><?php _e( 'Date of Birth', 'iwp' ); ?>
				<span><?php echo $userInfo['dob'] ?? _e( 'Not indicated', 'iwp' ); ?></span></li>
			<li><?php _e( 'Gender', 'iwp' ); ?>
				<span><?php echo $userInfo['gender'] ?? _e( 'Not indicated', 'iwp' ); ?></span></li>
			<li><?php _e( 'Appeal (Mr., Ms. or any other)', 'iwp' ); ?>
				<span><?php echo $userInfo['appeal'] ?? _e( 'Not indicated', 'iwp' ) ?></span></li>
		</ul>
		<div class="buttons">
			<a class="button" id="edit_profile" href="#"><?php _e( 'edit', 'iwp' ); ?></a>
		</div>
	</div>
	<form name="edit_profile_from" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post"
	id="edit_profile_from" style="display: none;">
		<div class="dfr">
			<div class="input">
				<input type="text" name="iwp_userinfo[fio_ru]" id="iwp_fio_ru"
				value="<?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name'] . ' ' . $userInfo['patronymic'] ?>"
				required>
				<label><?php _e( 'Last name, first name in Russian', 'iwp' ); ?><sub>*</sub></label>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[fio_en]" id="iwp_fio_en"
				value="<?php echo $userInfo['fio_en'] ?? _e( 'Not indicated', 'iwp' ); ?>" required>
				<label><?php _e( 'Last name, first name in English', 'iwp' ); ?><sub>*</sub></label>
			</div>
			<div class="input icon-calendar">
				<label><?php _e( 'Date of Birth', 'iwp' ); ?><sub>*</sub></label>
				<input type="text" name="iwp_userinfo[dob]" id="iwp_dob"
				value="<?php echo $userInfo['dob'] ?? _e( 'Not indicated', 'iwp' ); ?>" required>
			</div>
			<div class="select">
				<label><?php _e( 'Gender', 'iwp' ); ?></label>
				<select name="iwp_userinfo[gender]" id="iwp_gender">
					<option
					value="male" <?php echo 'male' === $userInfo['gender'] ? 'selected' : '' ?>><?php _e( 'Male', 'iwp' ); ?></option>
					<option
					value="female" <?php echo 'female' === $userInfo['gender'] ? 'selected' : '' ?>><?php _e( 'Female', 'iwp' ); ?></option>
				</select>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[appeal]" id="iwp_appeal"
				value="<?php echo $userInfo['appeal'] ?? _e( 'Not indicated', 'iwp' ) ?>">
				<label><?php _e( 'Appeal (Mr., Ms. or any other)', 'iwp' ); ?></label>
			</div>
			<p><sub>*</sub><?php _e( 'required fields', 'iwp' ); ?></p>
			<div class="buttons">
				<input class="button" type="submit" value="<?php _e( 'Save', 'iwp' ); ?>">
				<a class="button cancel" id="cancel_profile_from" href="#"><?php _e( 'cancel', 'iwp' ); ?></a>
			</div>
		</div>
		<input type="hidden" name="action" value="edit_profile_from"/>
		<input type="hidden" name="iwp_user_id" value="<?php echo $userID; ?>"/>
		<input type="hidden" id="iwp_to_redirect" name="iwp_to_redirect"
		value="<?php echo get_permalink( get_option( 'iwp_account_page', true ) ); ?>"/>
		<?php wp_nonce_field( 'iwp_profile_from', 'iwp_profile_from_nonce' ); ?>
	</form>
	<h3 style="background:#72C1AD;"><?php _e( 'Contact details', 'iwp' ); ?></h3>
	<div class="profile-block contact">
		<ul class="profile">
			<li><?php _e( 'Country of Residence', 'iwp' ); ?>
				<span><?php echo $userInfo['country'] ?? _e( 'Not indicated', 'iwp' ) ?></span></li>
			<li><?php _e( 'Province', 'iwp' ); ?>
				<span><?php echo $userInfo['province'] ?? _e( 'Not indicated', 'iwp' ) ?></span></li>
			<li><?php _e( 'City', 'iwp' ); ?><span><?php echo $userInfo['city'] ?? _e( 'Not indicated', 'iwp' )
					?></span></li>
			<li><?php _e( 'Contact Number', 'iwp' ); ?>
				<span><?php echo $userInfo['phone'] ?? _e( 'Not indicated', 'iwp' ) ?></span></li>
			<li><?php _e( 'Email', 'iwp' ); ?><span><?php echo $userEmail ?></span></li>
		</ul>
		<div class="buttons">
			<a class="button" id="edit_contact" href="#"><?php _e( 'edit', 'iwp' ); ?></a>
		</div>
	</div>
	<form name="edit_contact_from" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post"
	id="edit_contact_from" style="display: none;">
		<div class="dfr">
			<div class="select">
				<label><?php _e( 'Country of Residence', 'iwp' ); ?><sub>*</sub></label>
				<select name="iwp_userinfo[country]" id="iwp_country" required>
					<?php $country = $helpers->countryList(); ?>
					<option value="0"><?php _e( "Select a you country" ); ?></option>
					<?php if ( $country ): ?>
						<?php foreach ( $country as $key => $item ): ?>
							<option
							value="<?php echo $key ?>" <?php echo $key === $userInfo['country'] ? 'selected' : '' ?>><?php echo $key; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[province]" id="iwp_province"
				value="<?php echo $userInfo['province'] ?? _e( 'Not indicated', 'iwp' ) ?>" required>
				<label><?php _e( 'Province', 'iwp' ); ?><sub>*</sub></label>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[city]" id="iwp_city"
				value="<?php echo $userInfo['city'] ?? _e( 'Not indicated', 'iwp' ) ?>" required>
				<label><?php _e( 'City', 'iwp' ); ?><sub>*</sub></label>
			</div>
			<div class="input">
				<input type="tel" name="iwp_userinfo[phone]" id="iwp_phone"
				value="<?php echo $userInfo['phone'] ?? _e( 'Not indicated', 'iwp' ) ?>"
				pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
				required>
				<label><?php _e( 'Contact number', 'iwp' ); ?><sub>*</sub></label>
				<p class="description"><?php _e( 'Format:', 'iwp' ); ?> 123-456-7890</p>
			</div>
			<div class="input">
				<label><?php _e( 'Email', 'iwp' ); ?><sub>*</sub></label>
				<input type="email" name="iwp_userinfo[email]" id="iwp_email" value="<?php echo $userEmail; ?>"
				required>
			</div>
			<p><sub>*</sub><?php _e( 'required fields', 'iwp' ); ?></p>
			<div class="buttons">
				<input class="button" type="submit" value="<?php _e( 'Save', 'iwp' ); ?>">
				<a class="button cancel" id="cancel_contact_from" href="#"><?php _e( 'cancel', 'iwp' ); ?></a>
			</div>
		</div>
		<input type="hidden" name="action" value="edit_contact_from"/>
		<input type="hidden" name="iwp_user_id" value="<?php echo $userID; ?>"/>
		<input type="hidden" id="iwp_to_redirect" name="iwp_to_redirect"
		value="<?php echo get_permalink( get_option( 'iwp_account_page', true ) ); ?>"/>
		<?php wp_nonce_field( 'iwp_contact_from', 'iwp_contact_from_nonce' ); ?>
	</form>
	<h3 style="background:#F97C55;"><?php _e( 'data for performance', 'iwp' ); ?></h3>
	<div class="profile-block performance">
		<ul class="profile">
			<li><?php _e( 'Place of work', 'iwp' ); ?>
				<span><?php echo $userInfo['work'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
			</li>
			<li><?php _e( 'Structural division of the organization', 'iwp' ); ?>
				<span><?php echo $userInfo['organization'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
			</li>
			<li><?php _e( 'Academic title Degree honorary sports title', 'iwp' ); ?>
				<span><?php echo $userInfo['degree'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
			</li>
			<li><?php _e( 'Position', 'iwp' ); ?>
				<span><?php echo $userInfo['position'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
			</li>
			<li><?php _e( 'Article title', 'iwp' ); ?>
				<span><?php echo $userInfo['article_title'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
			</li>
			<li><?php _e( 'Co-authors', 'iwp' ); ?>
				<span><?php echo $userInfo['coauthors'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
			</li>
			<li><?php _e( 'Number and title of the Congress section', 'iwp' ); ?>
				<span><?php echo $userInfo['congress_section'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
			</li>
			<li><?php _e( 'Form of participation', 'iwp' ); ?>
				<span><?php echo $userInfo['form_participation'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
			</li>
			<?php if ( 'full_time' === $userInfo['form_participation'] ): ?>
				<li><?php _e( 'Form of performance', 'iwp' ); ?>
					<span><?php echo $userInfo['form_performance'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
				</li>
				<li><?php _e( 'The need for technical means', 'iwp' ); ?>
					<span><?php echo $userInfo['technical'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
				</li>
				<li><?php _e( 'List of technical means', 'iwp' ); ?>
					<span><?php echo $userInfo['technical_list'] ?? _e( 'Not indicated', 'iwp' ) ?></span>
				</li>
			<?php endif; ?>
		</ul>
		<div class="buttons">
			<a class="button" id="edit_performance" href="#"><?php _e( 'edit', 'iwp' ); ?></a>
		</div>
	</div>
	<form name="edit_performance_from" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post"
	id="edit_performance_from" style="display: none;">
		<div class="dfr">
			<div class="input">
				<input type="text" name="iwp_userinfo[work]" id="iwp_work"
				value="<?php echo $userInfo['work'] ?? '' ?>" required>
				<label><?php _e( 'Place of work / study (in full)', 'iwp' ); ?><sub>*</sub></label>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[organization]" id="iwp_organization"
				value="<?php echo $userInfo['organization'] ?? '' ?>" required>
				<label><?php _e( 'Structural division of the organization (in full)', 'iwp' ) ?>
					<sub>*</sub></label>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[degree]" id="iwp_degree"
				value="<?php echo $userInfo['degree'] ?? '' ?>" required>
				<label><?php _e( 'Academic title / Degree / Honorary sports title', 'iwp' ); ?>
					<sub>*</sub></label>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[position]" id="iwp_position"
				value="<?php echo $userInfo['position'] ?? '' ?>" required>
				<label><?php _e( 'Position (in full)', 'iwp' ); ?><sub>*</sub></label>
			</div>
			<div class="textarea">
				<label><?php _e( 'Paper title (in full)', 'iwp' ); ?><sub>*</sub></label>
				<textarea name="iwp_userinfo[article_title]" id="iwp_article_title"
				required> <?php echo $userInfo['article_title'] ?? '' ?></textarea>
			</div>
			<div class="textarea">
				<label><?php _e( 'Coauthors separated by commas (full name in Russian and English)', 'iwp' ); ?>
					<sub>*</sub></label>
				<textarea name="iwp_userinfo[coauthors]" id="iwp_coauthors"
				required><?php echo $userInfo['coauthors'] ?? '' ?></textarea>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[congress_section]" id="iwp_congress_section"
				value="<?php echo $userInfo['congress_section'] ?? '' ?>" required>
				<label><?php _e( 'Number and title of the Congress section', 'iwp' ); ?><sub>*</sub></label>
			</div>
			<div class="select">
				<label><?php _e( 'Form of participation', 'iwp' ); ?><sub>*</sub></label>
				<select type="text" name="iwp_userinfo[form_participation]" id="iwp_form_participation" required>
					<option value="0"><?php _e( 'Choose a Form of participation', 'iwp' ); ?></option>
					<option
					value="full_time" <?php echo 'full_time' === $userInfo['form_participation'] ? 'selected' : '' ?>><?php _e(
						'Full time', 'iwp' ); ?></option>
					<option
					value="extramural" <?php echo 'extramural' === $userInfo['form_participation'] ? 'selected' : '' ?>><?php _e( 'In-absentia', 'iwp' ); ?></option>
					<option
					value="listener" <?php echo 'listener' === $userInfo['form_participation'] ? 'selected' : '' ?>><?php _e( 'Online', 'iwp' ); ?></option>
				</select>
			</div>
			<div class="select">
				<label><?php _e( 'Form of performance', 'iwp' ); ?></label>
				<select name="iwp_userinfo[form_performance]" id="iwp_form_performance">
					<option value="0"><?php _e( 'Choose a presentation format', 'iwp' ); ?></option>
					<option
					value="oral" <?php echo 'oral' === $userInfo['form_performance'] ? 'selected' : '' ?>><?php _e( 'Oral presentation', 'iwp' ); ?></option>
					<option
					value="poster" <?php echo 'poster' === $userInfo['form_performance'] ? 'selected' : '' ?>><?php _e( 'Poster presentation', 'iwp' ); ?></option>
				</select>
			</div>
			<div class="select">
				<label><?php _e( 'The need for technical means', 'iwp' ) ?></label>
				<select name="iwp_userinfo[technical]" id="iwp_technical">
					<option value="0"><?php _e( 'Select a technical', 'iwp' ); ?></option>
					<option
					value="yes" <?php echo 'yes' === $userInfo['technical'] ? 'selected' : '' ?>><?php _e( 'Yes', 'iwp' ); ?></option>
					<option
					value="no" <?php echo 'no' === $userInfo['technical'] ? 'selected' : '' ?>><?php _e( 'No', 'iwp' ); ?></option>
				</select>
			</div>
			<div class="input">
				<input type="text" name="iwp_userinfo[technical_list]" id="iwp_technical_list"
				value="<?php echo $userInfo['technical_list'] ?? '' ?>">
				<label><?php _e( 'List of technical means', 'iwp' ); ?></label>
			</div>
			<p><sub>*</sub><?php _e( 'required fields', 'iwp' ); ?></p>
			<div class="buttons">
				<input class="button" type="submit" value="<?php _e( 'Save', 'iwp' ); ?>">
				<a class="button cancel" id="cancel_performance_from" href="#"><?php _e( 'cancel', 'iwp' ); ?></a>
			</div>
		</div>
		<input type="hidden" name="action" value="edit_performance_from"/>
		<input type="hidden" name="iwp_user_id" value="<?php echo $userID; ?>"/>
		<input type="hidden" id="iwp_to_redirect" name="iwp_to_redirect"
		value="<?php echo get_permalink( get_option( 'iwp_account_page', true ) ); ?>"/>
		<?php wp_nonce_field( 'iwp_performance_from', 'iwp_performance_from_nonce' ); ?>
	</form>
</div>
