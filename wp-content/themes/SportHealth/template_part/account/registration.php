<?php
/**
 * Created 09.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

//ru
use IWP\Admin\IWPAdminHelpers;

$one_text_ru    = get_theme_mod( 'customizer_reg_fee_ru_text_one', false );
$two_text_ru    = get_theme_mod( 'customizer_reg_fee_ru_text_tow', false );
$three_text_ru  = get_theme_mod( 'customizer_reg_fee_ru_text_three', false );
$one_price_ru   = get_theme_mod( 'customizer_reg_fee_ru_price_one', false );
$two_price_ru   = get_theme_mod( 'customizer_reg_fee_ru_price_tow', false );
$three_price_ru = get_theme_mod( 'customizer_reg_fee_ru_price_three', false );

//eur
$one_text_en    = get_theme_mod( 'customizer_reg_fee_en_text_one', false );
$two_text_en    = get_theme_mod( 'customizer_reg_fee_en_text_tow', false );
$three_text_en  = get_theme_mod( 'customizer_reg_fee_en_text_three', false );
$one_price_en   = get_theme_mod( 'customizer_reg_fee_en_price_one', false );
$two_price_en   = get_theme_mod( 'customizer_reg_fee_en_price_tow', false );
$three_price_en = get_theme_mod( 'customizer_reg_fee_en_price_three', false );

$helpers = new IWPAdminHelpers();
?>

<div class="tab-pane" id="registration">
	<form id="reg_fee" name="iwp_reg_fee" method="post" enctype="multipart/form-data">
		<div class="radio-buttons dfr">
			<div class="radio-button">
				<input id="ru" type="radio" name="iwp_currency" id="iwp_currency_ru" data-currency="ru" checked="checked">
				<label for="ru"><?php _e( 'RUB', 'iwp' ); ?></label>
			</div>
			<div class="radio-button">
				<input id="eu" type="radio" name="iwp_currency" id="iwp_currency_en" data-currency="eur">
				<label for="eu"><?php _e( 'EUR', 'iwp' ); ?></label>
			</div>
		</div>
		<div class="ru">
			<div class="checkbox">
				<input type="checkbox" name="iwp_reg_fee[full-time]" id="full-time-ru"
				       value="<?php echo $one_price_ru ?? 0; ?>">
				<label for="full-time-ru"><?php echo $one_text_ru ?? __( 'Not Set', 'iwp' ); ?></label>
			</div>
			<div class="checkbox">
				<input type="checkbox" name="iwp_reg_fee[publication-one]" id="publication-one-ru"
				       value="<?php echo $two_price_ru ?? 0; ?>">
				<label for="publication-one-ru"><?php echo $two_text_ru ?? __( 'Not Set', 'iwp' ) ?></label>
			</div>
			<div class="checkbox">
				<input type="checkbox" name="iwp_reg_fee[publication-two]" id="publication-two-ru"
				       value="<?php echo $three_price_ru ?? 0; ?>">
				<label for="publication-two-ru"><?php echo $three_text_ru ?? __( 'Not Set', 'iwp' ) ?></label>
			</div>
			<p class="price"><?php _e( 'Total:', 'iwp' ); ?><span>0 руб</span></p>
		</div>
		
		<div class="en" style="display: none;">
			<div class="checkbox">
				<input type="checkbox" name="iwp_reg_fee[full-time]" id="full-time-en"
				       value="<?php echo $one_price_en ?? 0; ?>">
				<label for="full-time-en"><?php echo $one_text_en ?? __( 'Not Set', 'iwp' ); ?></label>
			</div>
			<div class="checkbox">
				<input type="checkbox" name="iwp_reg_fee[publication-one]" id="publication-one-en"
				       value="<?php echo $two_price_en ?? 0; ?>">
				<label for="publication-one-en"><?php echo $two_text_en ?? __( 'Not Set', 'iwp' ) ?></label>
			</div>
			<div class="checkbox">
				<input type="checkbox" name="iwp_reg_fee[publication-two]" id="publication-two-en"
				       value="<?php echo $three_price_en ?? 0; ?>">
				<label for="publication-two-en"><?php echo $three_text_en ?? __( 'Not Set', 'iwp' ) ?></label>
			</div>
			<p class="price"><?php _e( 'Total:', 'iwp' ); ?><span>0 eur</span></p>
		</div>
		<div class="buttons">
			<input type="hidden" name="iwp_user_id" value="<?php echo $userID; ?>"/>
			<input class="button" type="submit" id="send_requisites" value="<?php _e( 'Get requisites', 'iwp' ); ?> ">
			<?php if( $helpers->getPaymentRequestInfoByUserID( $userID ) ): ?>
				<div class="file">
					<i class="icon-document"></i>
					<div class="dfr">
						<input id="add-file_check" type="file" name="iwp_check">
						<label for="add-file_check"><?php _e( 'Upload file', 'iwp' ); ?></label>
						<a href="#" class="button" id="send_check"><?php _e( 'Send Check', 'iwp' ); ?></a>
						<p><?php _e( 'No file selected', 'iwp' ); ?></p>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</form>
</div>
