<?php
/**
 * Created 26.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\VisaForm;

/**
 * Class IWPVisaForm
 *
 * @package IWP\wpbkComponents\VisaForm
 */
class IWPVisaForm {
	/**
	 * IWPVisaForm constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_visa_form', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_visa_form', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Visa Form', 'iwp' ),
			'description'             => esc_html__( 'Add new Visa Form', 'iwp' ),
			'base'                    => 'iwp_visa_form',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'HeadLine fires', 'iwp' ),
					'param_name'  => 'head_line_firs',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'colorpicker',
					'heading'     => __( 'Head Color First', 'iwp' ),
					'param_name'  => 'head_first_color',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'HeadLine second', 'iwp' ),
					'param_name'  => 'head_line_second',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'colorpicker',
					'heading'     => __( 'Head Color Second', 'iwp' ),
					'param_name'  => 'head_second_color',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea',
					'heading'     => __( 'Title file upload', 'iwp' ),
					'param_name'  => 'title_file',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea',
					'heading'     => __( 'Description file upload', 'iwp' ),
					'param_name'  => 'description_file',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea',
					'heading'     => __( 'Title agreement', 'iwp' ),
					'param_name'  => 'title_agreement',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_raw_html',
					'heading'     => __( 'Description agreement', 'iwp' ),
					'param_name'  => 'description_agreement',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Email to Send', 'iwp' ),
					'param_name'  => 'email',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		include get_template_directory() . '/includes/wpbkComponents/template/VisaForm/template.php';
		
		return ob_get_clean();
	}
}