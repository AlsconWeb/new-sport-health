<?php
/**
 * Created 02.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\wpbkComponents\PartnersForm;

/**
 * Class IWPPartnersForm
 *
 * @package IWP\wpbkComponents\PartnersForm
 */
class IWPPartnersForm {
	/**
	 * IWPPartnersForm constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_partners_form', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_partners_form', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Partners Form', 'iwp' ),
			'description'             => esc_html__( 'Add new Partners Form', 'iwp' ),
			'base'                    => 'iwp_partners_form',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Email To Send', 'iwp' ),
					'param_name'  => 'email',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Button Text', 'iwp' ),
					'param_name'  => 'button_text',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea',
					'heading'     => __( 'Title agreement', 'iwp' ),
					'param_name'  => 'title_agreement',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_raw_html',
					'heading'     => __( 'Description agreement', 'iwp' ),
					'param_name'  => 'description_agreement',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		
		include get_template_directory() . '/includes/wpbkComponents/template/PartnersForm/template.php';
		
		return ob_get_clean();
	}
}