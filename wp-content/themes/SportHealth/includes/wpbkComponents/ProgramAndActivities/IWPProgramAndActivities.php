<?php
/**
 * Created 25.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\ProgramAndActivities;

/**
 * Class IWPProgramAndActivities
 *
 * @package IWP\wpbkComponents\ProgramAndActivities
 */
class IWPProgramAndActivities {
	/**
	 * IWPProgramAndActivities constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_program_activities', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_program_activities', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Program And Activities', 'iwp' ),
			'description'             => esc_html__( 'Add new Program And Activities', 'iwp' ),
			'base'                    => 'iwp_program_activities',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'param_group',
					'heading'     => __( 'Program', 'iwp' ),
					'param_name'  => 'program',
					'value'       => '',
					'params'      => [
						[
							'type'        => 'textfield',
							'heading'     => __( 'Head', 'iwp' ),
							'param_name'  => 'head',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
							'group'       => 'General',
						],
						[
							'type'        => 'colorpicker',
							'heading'     => __( 'Head Color', 'iwp' ),
							'param_name'  => 'head_color',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
							'group'       => 'General',
						],
						[
							'type'        => 'param_group',
							'heading'     => __( 'Activities', 'iwp' ),
							'param_name'  => 'activities',
							'value'       => '',
							'params'      => [
								[
									'type'        => 'textfield',
									'heading'     => __( 'Time', 'iwp' ),
									'param_name'  => 'time',
									'value'       => '',
									'admin_label' => false,
									'save_always' => false,
									'group'       => 'General',
								],
								[
									'type'        => 'textarea',
									'heading'     => __( 'Description', 'iwp' ),
									'param_name'  => 'description',
									'value'       => '',
									'admin_label' => false,
									'save_always' => false,
									'group'       => 'General',
								],
							],
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
					],
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		
		include get_template_directory() . '/includes/wpbkComponents/template/ProgramAndActivities/template.php';
		
		return ob_get_clean();
	}
}