<?php
/**
 * Created 22.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\organizersConference;

/**
 * Class organizersConference
 *
 * @package IWP\wpbkComponents\organizersConference
 */
class organizersConference {
	/**
	 * organizersConference constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_organizers_conference', [ $this, 'output' ] );
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_organizers_conference', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Organizers Conference', 'iwp' ),
			'description'             => esc_html__( 'Add new Organizers', 'iwp' ),
			'base'                    => 'iwp_organizers_conference',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Count output', 'iwp' ),
					'param_name'  => 'count',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Organizers Type Name', 'iwp' ),
					'param_name'  => 'type_name',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		include get_template_directory() . '/includes/wpbkComponents/template/organizersConference/template.php';
		
		return ob_get_clean();
	}
}