<?php
/**
 * Created 20.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\mainBanner;

/**
 * Class IWPMainBanner
 *
 * @package IWP\wpbkComponents\mainBanner
 */
class IWPMainBanner {
	/**
	 * IWPMainBanner constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_main_banner', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_main_banner', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Main banner', 'iwp' ),
			'description'             => esc_html__( 'Add new Main banner', 'iwp' ),
			'base'                    => 'iwp_main_banner',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Sub title', 'iwp' ),
					'param_name'  => 'subtitle',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'attach_image',
					'heading'     => __( 'Photo', 'iwp' ),
					'param_name'  => 'photo',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'attach_image',
					'heading'     => __( 'Mobile Photo', 'iwp' ),
					'param_name'  => 'mobile_photo',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'vc_link',
					'heading'     => __( 'Button', 'iwp' ),
					'param_name'  => 'button',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		include get_template_directory() . '/includes/wpbkComponents/template/MainBanner/template.php';
		
		return ob_get_clean();
	}
	
}