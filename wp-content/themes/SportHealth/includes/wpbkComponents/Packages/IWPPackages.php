<?php
/**
 * Created 02.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\wpbkComponents\Packages;

/**
 * Class IWPPackages
 *
 * @package IWP\wpbkComponents\Packages
 */
class IWPPackages {
	/**
	 * IWPPackages constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_packages', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_packages', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Partners Packages', 'iwp' ),
			'description'             => esc_html__( 'Add new Partners Packages', 'iwp' ),
			'base'                    => 'iwp_packages',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Headline', 'iwp' ),
					'param_name'  => 'headline',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'colorpicker',
					'heading'     => __( 'Headline Color', 'iwp' ),
					'param_name'  => 'headline_color',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_html',
					'heading'     => __( 'Text', 'iwp' ),
					'param_name'  => 'content',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Button Text', 'iwp' ),
					'param_name'  => 'button_text',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'dropdown',
					'heading'     => __( 'Button target', 'iwp' ),
					'param_name'  => 'button_target',
					'value'       => [
						'Select Target' => 'target',
						'Bronze'        => 'bronze',
						'Silver'        => 'silver',
						'Gold'          => 'gold',
						'General'       => 'general',
						'Info'          => 'info',
					],
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		
		include get_template_directory() . '/includes/wpbkComponents/template/Packages/template.php';
		
		return ob_get_clean();
	}
}