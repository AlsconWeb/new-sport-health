<?php
/**
 * Created 24.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\MemberCountries;

use IWP\Helpers\IWPBakeryHelpers;

/**
 * Created 24.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */
class IWPMemberCountries {
	/**
	 * IWPMemberCountries constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_member_countries', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_member_countries', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map(): array {
		$countries = new IWPBakeryHelpers();
		
		return [
			'name'                    => esc_html__( 'Member Countries', 'iwp' ),
			'description'             => esc_html__( 'Add new Member Countries', 'iwp' ),
			'base'                    => 'iwp_member_countries',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'param_group',
					'heading'     => __( 'Countries', 'iwp' ),
					'param_name'  => 'member',
					'value'       => '',
					'params'      => [
						[
							'type'       => 'dropdown',
							'value'      => $countries->countryList(),
							'heading'    => __( 'Country', 'iwp' ),
							'param_name' => 'lat_lng',
						],
					],
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		
		include get_template_directory() . '/includes/wpbkComponents/template/MemberCountries/template.php';
		
		return ob_get_clean();
	}
}