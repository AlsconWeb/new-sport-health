<?php
/**
 * Created 20.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\Slider;

/**
 * Class IWPSlider
 *
 * @package IWP\wpbkComponents\Slider
 */
class IWPSlider {
	/**
	 * IWPSlider constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_slider', [ $this, 'output' ] );
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_slider', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Slider', 'iwp' ),
			'description'             => esc_html__( 'Add new Slider', 'iwp' ),
			'base'                    => 'iwp_slider',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Counter', 'iwp' ),
					'param_name'  => 'counter',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'       => 'attach_images',
					'value'      => '',
					'heading'    => __( 'Image', 'onyx' ),
					'param_name' => 'image',
					'group'      => 'General',
				],
				[
					'type'        => 'vc_link',
					'heading'     => __( 'Button', 'iwp' ),
					'param_name'  => 'button',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		include get_template_directory() . '/includes/wpbkComponents/template/Slider/template.php';
		
		return ob_get_clean();
	}
}