<?php
/**
 * Created 03.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\wpbkComponents\ContactAndMap;

/**
 * Class IWPContactAndMap
 *
 * @package IWP\wpbkComponents\ContactAndMap
 */
class IWPContactAndMap {
	/**
	 * IWPContactAndMap constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_contact_and_map', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_contact_and_map', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Contact and Map', 'iwp' ),
			'description'             => esc_html__( 'Add new Contact and Map', 'iwp' ),
			'base'                    => 'iwp_contact_and_map',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_html',
					'heading'     => __( 'Text', 'iwp' ),
					'param_name'  => 'content',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'attach_images',
					'heading'     => __( 'Images', 'iwp' ),
					'param_name'  => 'images',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_raw_html',
					'heading'     => __( 'Map', 'iwp' ),
					'param_name'  => 'map',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		
		include get_template_directory() . '/includes/wpbkComponents/template/ContactAndMap/template.php';
		
		return ob_get_clean();
	}
}