<?php
/**
 * Created 21.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\partners;

/**
 * Class IWPPartners
 *
 * @package IWP\wpbkComponents\partners
 */
class IWPPartners {
	/**
	 * IWPPartners constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_partners_block', [ $this, 'output' ] );
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_partners_block', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Partners', 'iwp' ),
			'description'             => esc_html__( 'Add new Partners', 'iwp' ),
			'base'                    => 'iwp_partners_block',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Button text', 'iwp' ),
					'param_name'  => 'button_text',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'param_group',
					'heading'     => __( 'Partners', 'iwp' ),
					'param_name'  => 'partners',
					'value'       => '',
					'params'      => [
						[
							'type'        => 'attach_image',
							'heading'     => __( 'Set Image', 'iwp' ),
							'param_name'  => 'image',
							'value'       => '',
							'admin_label' => true,
							'save_always' => true,
						],
						[
							'type'        => 'textfield',
							'heading'     => __( 'Title', 'iwp' ),
							'param_name'  => 'title',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
						],
						[
							'type'        => 'vc_link',
							'heading'     => __( 'Button', 'iwp' ),
							'param_name'  => 'link',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
						],
					],
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		include get_template_directory() . '/includes/wpbkComponents/template/Partners/template.php';
		
		return ob_get_clean();
	}
}