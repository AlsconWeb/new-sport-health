<?php
/**
 * Created 24.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$members   = vc_param_group_parse_atts( $atts['member'] );
$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
$coordinate = [];
foreach ( $members as $key => $member ) {
	$coordinate[ $key ] = explode( ',', $member['lat_lng'] );
}

?>
<div class="map-block">
	<div class="container">
		<div class="row">
			<h2 class="title"><?php echo $atts['title'] ?></h2>
		</div>
		<div class="row">
			<div id="map"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		if ($('#map').length) {
			let map;
			let countryCoordinate = <?php print json_encode( $coordinate ) ?>;
			let markers = [];
			let image = "<?php print get_template_directory_uri() . '/assets/img/point.png' ?>";
			
			initMap();
			allMarkers();
			addMarkers();
			
			function initMap() {
				map = new google.maps.Map(document.getElementById("map"), {
					center: {
						lat: 52.823625,
						lng: 45.996631,
					},
					zoom: 2,
				});
			}
			
			function allMarkers() {
				if (countryCoordinate.length > 0) {
					$.each(countryCoordinate, function (index, value) {
						let mapLat = value[ 0 ];
						let mapLng = value[ 1 ];
						markers.push({
							position: new google.maps.LatLng(mapLat, mapLng),
							map: map,
							icon: image,
						});
					});
				}
			}
			
			function addMarkers() {
				$.each(markers, function (i, val) {
					new google.maps.Marker(val);
				});
				let bounds = new google.maps.LatLngBounds();
				for (var i = 0; i < markers.length; i++) {
					bounds.extend(markers[ i ]);
				}
				
				map.fitBounds(bounds);
			}
		}
	});

</script>
