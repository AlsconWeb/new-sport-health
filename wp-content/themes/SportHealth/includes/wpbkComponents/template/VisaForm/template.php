<?php
/**
 * Created 26.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */


use IWP\Helpers\IWPBakeryHelpers;

$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}

$helpers = new IWPBakeryHelpers();
?>

<form class="details <?php echo $css_class; ?>" method="post" action="<?php echo admin_url( 'admin-post.php' ); ?>"
      id="visa_form" enctype="multipart/form-data">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="dfr">
					<h2 class="title"><?php echo $atts['title'] ?? ''; ?></h2>
					<h3
						style="background-color:<?php echo $atts['head_first_color'] ?? ''; ?>;">
						<?php echo $atts['head_line_firs'] ?? ''; ?>
					</h3>
					<div class="input">
						<label><?php _e( 'First Name', 'iwp' ); ?></label>
						<input type="text" name="iwp_first_name" id="iwp_first_name">
					</div>
					<div class="input">
						<label><?php _e( 'Last Name', 'iwp' ); ?></label>
						<input type="text" name="iwp_last_name" id="iwp_last_name">
					</div>
					<div class="select">
						<label><?php _e( 'Gender', 'iwp' ); ?></label>
						<select name="iwp_gender" id="iwp_gender">
							<option value="male"><?php _e( 'Male', 'iwp' ); ?></option>
							<option value="female"><?php _e( 'Female', 'iwp' ); ?></option>
						</select>
					</div>
					<div class="input">
						<label><?php _e( 'Nationality', 'iwp' ); ?></label>
						<input type="text" name="iwp_nationality" id="iwp_nationality">
					</div>
					<div class="input">
						<label><?php _e( 'Passport series and number', 'iwp' ); ?></label>
						<input type="text" name="iwp_passport" id="iwp_passport">
					</div>
					<div class="input icon-calendar">
						<label><?php _e( 'Date of Birth', 'iwp' ); ?></label>
						<input type="text" name="iwp_dob" id="iwp_dob">
					</div>
					<h3 style="background-color:<?php echo $atts['head_second_color']; ?>;">
						<?php echo $atts['head_line_second'] ?? ''; ?>
					</h3>
					<div class="select">
						<label><?php _e( 'Country', 'iwp' ); ?></label>
						<select name="iwp_country" id="iwp_country">
							<?php $country = $helpers->countryList(); ?>
							<option value="0"><?php _e( "Select a you country", 'iwp' ); ?></option>
							<?php if( $country ): ?>
								<?php foreach ( $country as $key => $item ): ?>
									<option
										value="<?php echo ICL_LANGUAGE_CODE === 'en' ? $key : $item['name_ru']; ?>"><?php echo ICL_LANGUAGE_CODE === 'en' ? $key : $item['name_ru']; ?></option>
								<?php endforeach; ?>
							<?php endif; ?>
						</select>
					</div>
					<div class="input">
						<label><?php _e( 'City', 'iwp' ); ?></label>
						<input type="text" name="iwp_city" id="iwp_city">
					</div>
					<div class="input">
						<label><?php _e( 'Address', 'iwp' ); ?></label>
						<input type="text" name="iwp_address" id="iwp_address">
					</div>
					<div class="input">
						<label><?php _e( 'Postcode', 'iwp' ); ?></label>
						<input type="text" name="iwp_postcode" id="iwp_postcode">
					</div>
					<div class="input">
						<label><?php _e( 'Email', 'iwp' ); ?></label>
						<input type="email" name="iwp_email" id="iwp_email" required>
					</div>
					<div class="input">
						<label><?php _e( 'Phone', 'iwp' ); ?></label>
						<input type="tel" name="iwp_phone" id="iwp_phone" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">
						<p class="description"><?php _e( 'Format:', 'iwp' ); ?> 123-456-7890</p>
					</div>
					<div class="file">
						<h4><?php echo $atts['title_file'] ?? ''; ?></h4>
						<p><?php echo $atts['description_file'] ?? ''; ?></p><i class="icon-document"></i>
						<div class="dfc">
							<input id="add-file" type="file" name="iwp_file" accept=".jpg, .jpeg, .png, .pdf, .tiff">
							<label for="add-file"><?php _e( 'Upload file', 'iwp' ); ?></label>
							<p><?php _e( 'No file selected', 'iwp' ); ?></p>
						</div>
					</div>
					<div class="checkbox">
						<p><?php echo $atts['title_agreement'] ?? ''; ?></p>
						<input id="checkbox_agreement" type="checkbox" required>
						<label
							for="checkbox_agreement"><?php echo rawurldecode( base64_decode( $atts['description_agreement'] ) ) ?? ''; ?></label>
					</div>
					<input type="hidden" name="action" value="iwp_visa_form"/>
					<input type="hidden" name="iwp_to_email" value="<?php echo $atts['email'] ?>"/>
					<?php wp_nonce_field( 'iwp_visa_form', 'iwp_visa_form_nonce' ); ?>
					<input class="button" type="submit" id="submit_from" value="<?php _e( 'Send', 'iwp' ); ?>">
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		let paramsUrl = location.search;
		let params = new URLSearchParams(paramsUrl);
		
		let success = params.get('success');
		let error = params.get('error');
		if (success) {
			Swal.fire({
				icon: 'success',
				title: 'Success',
				text: success,
			}).then((result) => {
				if (result.isConfirmed) {
					location.href = window.location.pathname;
				}
			})
		}
		
		if (error) {
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: error,
			}).then((result) => {
				if (result.isConfirmed) {
					location.href = window.location.pathname;
				}
			})
		}
		
		$('#submit_from').click(function (e) {
			
			if ($('#checkbox_agreement:checked').length === 0) {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					html: "Вы не дали согласие на обработку персональных данным <br><br> You have not given consent to the " +
						"processing" +
						" of personal data",
				})
			}
		});
		
	});
</script>
