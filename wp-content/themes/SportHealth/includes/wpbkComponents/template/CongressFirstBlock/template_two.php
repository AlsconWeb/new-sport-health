<?php
/**
 * Created 24.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */


$dates     = vc_param_group_parse_atts( $atts['dates'] ?? '' );
$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>

<div class="first-block <?php echo $css_class; ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="desc">
					<h2 class="title"><?php the_title(); ?></h2>
					<h1 class="title"><?php echo $atts['title']; ?></h1>
					<?php if( $dates ): ?>
						<?php foreach ( $dates as $date ): ?>
							<div class="event">
								<h4><?php echo $date['date']; ?></h4>
								<p><?php echo $date['description']; ?></p>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php echo rawurldecode( base64_decode( $atts['action_block'] ) ); ?>
				</div>
			</div>
		</div>
	</div>
</div>
