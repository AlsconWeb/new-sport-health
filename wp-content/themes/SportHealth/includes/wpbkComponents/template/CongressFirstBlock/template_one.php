<?php
/**
 * Created 23.05.2021
 * Version 2.0.0
 * Last update
 * Author: Alex L
 *
 */


$organizers = vc_param_group_parse_atts( $atts['organizers'] ?? '' );
$css_class  = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>
<div class="first-block <?php echo $css_class; ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="desc">
					<h2 class="title"><?php the_title(); ?></h2>
					<h1 class="title"><?php echo $atts['title']; ?></h1>
					<?php echo rawurldecode( base64_decode( $atts['action_block'] ) ); ?>
					<?php echo $content; ?>
				</div>
			</div>
			<?php if( $organizers ): ?>
				<?php foreach ( $organizers as $organizer ): $link = vc_build_link( $organizer['link'] ?? '' ); ?>
					<div class="col">
						<div class="item">
							<a href="<?php echo ! empty( $link['url'] ) ? $link['url'] : '#'; ?>"
							   target="<?php echo $link['target']; ?>"
							   rel="<?php echo $link['rel']; ?>">
								<img src="<?php echo wp_get_attachment_url( $organizer['image'] ); ?>"
								     alt="<?php echo get_the_title( $organizer['image'] ); ?>">
							</a>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
