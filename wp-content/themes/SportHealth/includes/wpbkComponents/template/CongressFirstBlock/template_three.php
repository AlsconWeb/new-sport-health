<?php
/**
 * Created 24.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>
<div class="first-block <?php echo $css_class; ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="desc">
					<h2 class="title"><?php the_title(); ?></h2>
					<h1 class="title"><?php echo $atts['title'] ?? ''; ?></h1>
					<?php echo $content ?? ''; ?>
					<div class="contact-block">
						<?php echo rawurldecode( base64_decode( $atts['action_block'] ) ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>