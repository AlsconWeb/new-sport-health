<?php
/**
 * Created 20.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$image        = wp_get_attachment_image( $atts['photo'], 'full', false, [ 'class' => 'img-desktop' ] );
$mobile_image = wp_get_attachment_image( $atts['mobile_photo'], 'full', false, [ 'class' => 'img-mob' ] );
$link         = vc_build_link( $atts['button'] );
$css_class    = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>
<div class="banner <?php echo $css_class; ?>">
	<div class="container">
		<div class="row align-items-end">
			<div class="col-8">
				<h2 class="title"><?php echo $atts['subtitle']; ?></h2>
				<h1 class="title"><?php echo $atts['title']; ?></h1>
			</div>
			<div class="col-4">
				<a class="btn icon-arrow-right" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"
				   rel="<?php echo $link['rel']; ?>">
					<?php echo $link['title']; ?>
				</a>
			</div>
		</div>
	</div>
	<div class="img">
		<?php echo $image; ?>
		<?php echo $mobile_image; ?>
	</div>
</div>
