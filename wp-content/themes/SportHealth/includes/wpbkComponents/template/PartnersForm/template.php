<?php
/**
 * Created 02.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */


use IWP\Helpers\Form\IWPForm;

$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
$helpers = new IWPForm();
?>
<div class="form <?php echo $css_class; ?>" id="partner-form">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<form method="post" action="<?php echo admin_url( 'admin-post.php' ); ?>" id="partner_form">
					<div class="dfr">
						<div class="input">
							<input type="text" name="iwp_name" id="iwp_name" required>
							<label><?php _e( 'Full Name', 'iwp' ); ?><sub>*</sub></label>
						</div>
						<div class="input">
							<input type="tel" name="iwp_phone" id="iwp_phone" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
							<label><?php _e( 'Phone', 'iwp' ); ?><sub>*</sub></label>
							<p class="description"><?php _e( 'Format:', 'iwp' ); ?> 123-456-7890</p>
						</div>
						<div class="input">
							<input type="email" name="iwp_email" id="iwp_email" required>
							<label><?php _e( 'Email', 'iwp' ); ?><sub>*</sub></label>
						</div>
						<div class="input">
							<input type="text" name="iwp_company_name" id="iwp_company_name" required>
							<label><?php _e( 'Company', 'iwp' ); ?><sub>*</sub></label>
						</div>
						<div class="select">
							<label><?php _e( "Package's", 'iwp' ); ?></label>
							<select name="iwp_packages" id="iwp_packages">
								<?php foreach ( $helpers->getPackages() as $package ): ?>
									<?php if( 'ru' === ICL_LANGUAGE_CODE ): ?>
										<option value="<?php echo $package['param_name'] ?>"><?php echo $package['name_ru'] ?></option>
									<?php else: ?>
										<option value="<?php echo $package['param_name'] ?>"><?php echo $package['name_en'] ?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="textarea">
							<label><?php _e( 'Comment or message', 'iwp' ); ?></label>
							<textarea placeholder="<?php _e( 'Enter text', 'iwp' ); ?>" name="iwp_message"
							          id="iwp_message"></textarea>
						</div>
						<div class="checkbox">
							<p><sub>*</sub><?php echo $atts['title_agreement'] ?? ''; ?></p>
							<input id="checkbox_agreement" type="checkbox" name="iwp_agreement" value="agreement" required>
							<label
								for="checkbox_agreement"><?php echo rawurldecode( base64_decode( $atts['description_agreement'] ) ) ?? ''; ?></label>
						</div>
						<input type="hidden" name="action" value="iwp_partner_form"/>
						<input type="hidden" name="iwp_to_email" value="<?php echo $atts['email'] ?>"/>
						<?php wp_nonce_field( 'iwp_partner_form', 'iwp_partner_form_nonce' ); ?>
						<input class="button" id="submit_from" type="submit" value="<?php echo $atts['button_text'] ?>">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		if ($('.button.dfc').length) {
			$('.button.dfc').click(function (e) {
				e.preventDefault();
				let target = $(this).data('target');
				let options = $('#iwp_packages option');
				
				$.each(options, function (i, val) {
					
					if (target === $(val).val()) {
						$(val).attr('selected', 'true')
					}
				});
			});
		}
		
		let paramsUrl = location.search;
		let params = new URLSearchParams(paramsUrl);
		
		let success = params.get('success');
		let error = params.get('error');
		if (success) {
			Swal.fire({
				icon: 'success',
				title: 'Success',
				text: success,
			}).then((result) => {
				if (result.isConfirmed) {
					location.href = window.location.pathname;
				}
			})
		}
		
		if (error) {
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: error,
			}).then((result) => {
				if (result.isConfirmed) {
					location.href = window.location.pathname;
				}
			})
		}
		
		$('#submit_from').click(function (e) {
			console.log('No Check', $('#checkbox_agreement:checked'))
			if ($('#checkbox_agreement:checked').length === 0) {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					html: "Вы не дали согласие на обработку персональных данным <br><br> You have not given consent to the " +
						"processing" +
						" of personal data",
				})
			}
		});
	});
</script>
