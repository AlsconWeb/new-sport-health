<?php
/**
 * Created 03.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
$image_ids = explode( ',', $atts['images'] );
?>
<div class="container <?php echo $css_class; ?>">
	<?php if( $atts['title'] ): ?>
		<div class="row">
			<div class="col-12">
				<h2 class="title"><?php echo $atts['title']; ?></h2>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-6">
			<?php echo $content ?? ''; ?>
			<div class="row">
				<?php foreach ( $image_ids as $image_id ): ?>
					<div class="col-6"><?php echo wp_get_attachment_image( $image_id, 'full' ) ?></div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="col-6">
			<div id="map"><?php echo rawurldecode( base64_decode( $atts['map'] ) ); ?></div>
		</div>
	</div>
</div>