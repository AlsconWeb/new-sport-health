<?php
/**
 * Created 20.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$organizers = vc_param_group_parse_atts( $atts['organizers'] );
$css_class  = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>

<div class="organizers <?php echo $css_class ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="title"><?php echo $atts['title'] ?></h2>
				<?php echo $content; ?>
			</div>
			<?php foreach ( $organizers as $organizer ): ?>
				<div class="col">
					<div class="item">
						<img src="<?php echo wp_get_attachment_url( $organizer['image'] ); ?>" alt="<?php echo get_the_title(
							$organizer['image'] ) ?>">
						<p><?php echo $organizer['description'] ?></p>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
