<?php
/**
 * Created 24.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>

<div class="info <?php echo $css_class; ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="title"><?php echo $atts['title'] ?></h2>
				<?php echo $content; ?>
			</div>
		</div>
	</div>
</div>
