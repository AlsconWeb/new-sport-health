<?php
/**
 * Created 03.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
$image_ids = explode( ',', $atts['images'] ) ?? false;
?>

<div class="row">
	<div class="col-12">
		<h2 class="title"><?php echo $atts['title'] ?? "" ?></h2>
		<h3
			style="background-color:<?php echo $atts['headline_color'] ?? '#2263A8'; ?>;"><?php echo $atts['headline'] ?? "" ?></h3>
	</div>
	<div class="col-4">
		<?php echo $content; ?>
	</div>
	<?php if( $image_ids ): ?>
		<?php foreach ( $image_ids as $image_id ): ?>
			<div class="col-4">
				<?php echo wp_get_attachment_image( $image_id, 'full' ); ?>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

</div>
