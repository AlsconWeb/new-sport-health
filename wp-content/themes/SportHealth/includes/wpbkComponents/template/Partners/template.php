<?php
/**
 * Created 21.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$partners  = vc_param_group_parse_atts( $atts['partners'] );
$css_class = '';
if ( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
$i           = 0;
$partnerJSON = [];
foreach ( $partners as $key => $partner ) {
	$partnerJSON[ $key ]['image_url'] = wp_get_attachment_url( $partner['image'] );
	$partnerJSON[ $key ]['image_alt'] = get_the_title( $partner['image'] );
	$partnerJSON[ $key ]['title']     = $partner['title'];
	$partnerJSON[ $key ]['link']      = vc_build_link( $partner['link'] ?? '' );
}

?>
<div class="partners">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="title"><?php echo $atts['title']; ?></h2>
			</div>
			<div class="wrapper-items dfr row-cols-4">
				<?php foreach ( $partners as $partner ):
					if ( 4 === $i ) {
						break;
					}
					$link = vc_build_link( $partner['link'] ?? '' );
					?>
					<div class="col">
						<div class="item">
							<a href="<?php echo $link['url'] ?? '#'; ?>"
							target="<?php echo $link['target']; ?>"
							rel="<?php echo $link['rel']; ?>">
								<img src="<?php echo wp_get_attachment_url( $partner['image'] ); ?>"
								alt="<?php echo get_the_title( $partner['image'] ); ?>">
								<h4><?php echo $partner['title'] ?></h4>
							</a>
						</div>
					</div>
					<?php $i ++; endforeach; ?>
			</div>
			<div class="col-12"><a class="button" id="partner-more" href="#"><?php echo $atts['button_text'] ?></a></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	let partners = <?php print json_encode( $partnerJSON ); ?>;
	
	let i = 0;
	jQuery(document).ready(function ($) {
		partners.splice(0, 4);
		$('#partner-more').click(function (e) {
			e.preventDefault();
			let html = '';
			$.each(partners, function (index, value) {
				html = `<div class="col"><div class="item"><a href="${value.link.url}" target="${value.link.target}"
				rel="${value.link.rel}"
				><img src="${value.image_url}" alt="${value.image_alt}"><h4>${value.title}</h4></a></div></div>`;
				$('.wrapper-items').append(html);
			});
			$(this).hide();
		});
	});
</script>
