<?php
/**
 * Created 28.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
$tel_one = get_theme_mod( 'customizer_contact_info_tel' );
$tel_two = get_theme_mod( 'customizer_contact_info_tel_two' );
$email   = get_theme_mod( 'customizer_contact_info_email' );

$tel_one_link = preg_replace( "([\/, , (, ), -])", '', $tel_one );
$tel_two_link = preg_replace( "([\/, , (, ), -])", '', $tel_two );
?>
<section>
	<div class="first-block map-block <?php echo $css_class; ?>">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="title"><?php the_title(); ?></h2>
					<h1 class="title"><?php echo $atts['title'] ?></h1>
					<div id="map"><?php print rawurldecode( base64_decode( $atts['map'] ) ) ?></div>
					<ul class="contacts">
						<li class="icon-phone">
							<a href="tel:<?php echo $tel_one_link ?>"><?php echo $tel_one; ?></a>
							<a href="tel:<?php echo $tel_two_link ?>"><?php echo $tel_two; ?></a>
						</li>
						<li class="icon-email">
							<a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
						</li>
						<li class="icon-marker">
							<?php echo get_theme_mod( 'customizer_contact_info_address' ); ?>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
