<?php
/**
 * Created 02.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

$css_class = '';
if ( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>
<div class="container <?php echo $css_class; ?>">
	<?php if ( $atts['title'] ): ?>
		<div class="row">
			<div class="col-12">
				<h2 class="title"><?php echo $atts['title']; ?></h2>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-12">
			<div class="item">
				<h3
				style="background-color:<?php echo $atts['headline_color'] ?? '#0BA2E0' ?>;"><?php echo $atts['headline'] ?></h3>
				<?php echo wpautop( $content ) ?? '' ?>
				<a class="button dfc" href="#partner-form"
				data-target="<?php echo $atts['button_target'] ?? 'none'; ?>"><?php echo $atts['button_text'] ?? ''; ?></a>
			</div>
		</div>
	</div>
</div>

