<?php
/**
 * Created 22.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$arg = [
	'post_type'      => 'organizers',
	'posts_per_page' => $atts['count'],
	'tax_query'      => [
		[
			'taxonomy' => 'organizers_conference',
			'field'    => 'name',
			'terms'    => $atts['type_name'],
		],
	],
	'orderby'        => 'date',
	'order'          => 'ASC',
];

$query      = new WP_Query( $arg );
$itertor    = 1;
$colorClass = '';
$css_class  = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>
<div class="congress <?php echo $css_class ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="title"><?php echo $atts['title'] ?? ''; ?></h2>
			</div>
		</div>
		<?php if( $query->have_posts() ): ?>
			<?php while ( $query->have_posts() ):
				$query->the_post();
				$id = get_the_ID();
				switch ( $itertor ) {
					case 1 :
						$colorClass = 'green';
						break;
					case 2 :
						$colorClass = 'blue';
						break;
					case 3 :
						$colorClass = 'orange';
						break;
					default:
						$colorClass = 'green';
				}
				if( $itertor % 2 ):
					?>
					<div class="row align-items-center">
						<div class="col-5 <?php echo $colorClass; ?>">
							<div class="img">
								<?php if( has_post_thumbnail( $id ) ): ?>
									<?php the_post_thumbnail( 'organization' ); ?>
								<?php else: ?>
									<img src="https://via.placeholder.com/540x450" alt="Not Image">
								<?php endif; ?>
							</div>
						</div>
						<div class="col-7">
							<h3><?php the_title(); ?></h3>
							<p><?php the_excerpt(); ?></p>
							<a class="btn icon-arrow-right"
							   href="<?php the_permalink(); ?>"><?php _e( 'Read More', 'iwp' ); ?></a>
						</div>
					</div>
				<?php else: ?>
					<div class="row align-items-center">
						<div class="col-7">
							<h3><?php the_title(); ?></h3>
							<p><?php the_excerpt(); ?></p>
							<a class="btn icon-arrow-right"
							   href="<?php the_permalink(); ?>"><?php _e( 'Read More', 'iwp' ); ?></a>
						</div>
						<div class="col-5 <?php echo $colorClass; ?>">
							<div class="img">
								<?php if( has_post_thumbnail( $id ) ): ?>
									<?php the_post_thumbnail( 'organization' ); ?>
								<?php else: ?>
									<img src="https://via.placeholder.com/540x450" alt="Not Image">
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php
				endif;
				$itertor ++;
			endwhile;
			wp_reset_postdata(); ?>
		<?php endif; ?>
	</div>
</div>
