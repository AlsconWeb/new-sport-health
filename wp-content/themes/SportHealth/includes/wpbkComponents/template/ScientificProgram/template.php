<?php
/**
 * Created 25.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$programs  = vc_param_group_parse_atts( $atts['program'] );
$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>

<div class="program <?php echo $css_class ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="title"><?php echo $atts['title'] ?></h2>
				<?php echo $content ?? '' ?>
				<?php if( $programs ): ?>
					<?php foreach ( $programs as $program ): ?>
						<h3 style="background-color:<?php echo $program['head_color'] ?? '' ?>;">
							<?php echo $program['head'] ?? '' ?>
						</h3>
						<?php $sections = vc_param_group_parse_atts( $program['sections'] ?? '' ); ?>
						<?php if( $sections ): ?>
							<ul>
								<?php foreach ( $sections as $section ): ?>
									<li>
										<?php echo $section['section_name'] ?>
										<p><?php echo $section['description']; ?></p>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
