<?php
/**
 * Created 20.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$images    = explode( ',', $atts['image'] );
$link      = vc_build_link( $atts['button'] );
$css_class = '';
if ( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>
<div class="slider-block">
	<div class="container-fluid">
		<div class="row row-cols-auto align-items-center justify-content-end">
			<div class="col">
				<div class="description">
					<h3 class="title">
			  <?php echo $atts['counter']; ?><span><?php echo $atts['title'] ?></span>
					</h3>
					<div class="slider-arrow">
						<i class="icon-left"></i>
						<i class="icon-right"></i>
					</div>
					<a class="btn icon-arrow-right"
					   href="<?php echo $link['url']; ?>"
					   target="<?php echo $link['target']; ?>"
					   rel="<?php echo $link['rel']; ?>">
			  <?php echo $link['title']; ?>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="photo-gallery">
			<?php foreach ( $images as $image ): ?>
							<a href="<?php echo wp_get_attachment_url( $image ); ?>" data-fancybox="gallery">
								<img src="<?php echo wp_get_attachment_url( $image ); ?>" alt="<?php echo get_the_title( $image ) ?>">
							</a>
			<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
