<?php
/**
 * Created 21.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$patronages = vc_param_group_parse_atts( $atts['patronage'] );
$css_class  = '';
if ( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>

<div class="patronage <?php echo $css_class; ?>">
	<div class="container">
		<div class="row">
		<?php if ( $atts['title'] ): ?>
					<div class="col-12">
						<h2 class="title"><?php echo $atts['title']; ?></h2>
					</div>
		<?php endif; ?>
		<?php foreach ( $patronages as $patronages ): $link = vc_build_link( $patronages['link'] ); ?>
					<div class="col">
						<div class="item">
							<a href="<?php echo $link['url']; ?>"
							   target="<?php echo $link['target']; ?>"
							   rel="<?php echo $link['rel']; ?>">
								<img src="<?php echo wp_get_attachment_url( $patronages['image'] ); ?>"
								     alt="<?php echo get_the_title( $patronages['image'] ); ?>">
							</a>
						</div>
					</div>
		<?php endforeach; ?>
		</div>
	</div>
</div>
