<?php
/**
 * Created 03.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\wpbkComponents\ListingHeadline;

/**
 * Class IWPListingHeadline
 *
 * @package IWP\wpbkComponents\ListingHeadline
 */
class IWPListingHeadline {
	/**
	 * IWPListingHeadline constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_listing_headline', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_listing_headline', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Listing Headline', 'iwp' ),
			'description'             => esc_html__( 'Add new Listing Headline', 'iwp' ),
			'base'                    => 'iwp_listing_headline',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Headline', 'iwp' ),
					'param_name'  => 'headline',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'colorpicker',
					'heading'     => __( 'Headline Color', 'iwp' ),
					'param_name'  => 'headline_color',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_html',
					'heading'     => __( 'Text', 'iwp' ),
					'param_name'  => 'content',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		
		include get_template_directory() . '/includes/wpbkComponents/template/ListingHeadline/template.php';
		
		return ob_get_clean();
	}
}