<?php
/**
 * Created 25.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\ScientificProgram;

/**
 * Class IWPScientificProgram
 *
 * @package IWP\wpbkComponents\ScientificProgram
 */
class IWPScientificProgram {
	/**
	 * IWPScientificProgram constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_scientific_program', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_scientific_program', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Scientific Program', 'iwp' ),
			'description'             => esc_html__( 'Add new Scientific Program', 'iwp' ),
			'base'                    => 'iwp_scientific_program',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_html',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'content',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'param_group',
					'heading'     => __( 'Program', 'iwp' ),
					'param_name'  => 'program',
					'value'       => '',
					'params'      => [
						[
							'type'        => 'textfield',
							'heading'     => __( 'Head', 'iwp' ),
							'param_name'  => 'head',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
							'group'       => 'General',
						],
						[
							'type'        => 'colorpicker',
							'heading'     => __( 'Head Color', 'iwp' ),
							'param_name'  => 'head_color',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
							'group'       => 'General',
						],
						[
							'type'        => 'param_group',
							'heading'     => __( 'Sections', 'iwp' ),
							'param_name'  => 'sections',
							'value'       => '',
							'params'      => [
								[
									'type'        => 'textfield',
									'heading'     => __( 'Section Name', 'iwp' ),
									'param_name'  => 'section_name',
									'value'       => '',
									'admin_label' => false,
									'save_always' => false,
									'group'       => 'General',
								],
								[
									'type'        => 'textarea',
									'heading'     => __( 'Description', 'iwp' ),
									'param_name'  => 'description',
									'value'       => '',
									'admin_label' => false,
									'save_always' => false,
									'group'       => 'General',
								],
							],
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
					],
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		
		include get_template_directory() . '/includes/wpbkComponents/template/ScientificProgram/template.php';
		
		return ob_get_clean();
	}
}