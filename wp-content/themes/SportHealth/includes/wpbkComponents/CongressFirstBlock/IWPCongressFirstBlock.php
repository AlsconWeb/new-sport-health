<?php
/**
 * Created 23.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\wpbkComponents\CongressFirstBlock;

/**
 * Class IWPCongresFirstBlock
 *
 * @package IWP\wpbkComponents\CongressFirstBlock
 */
class IWPCongressFirstBlock {
	/**
	 * IWPCongressFirstBlock constructor.
	 */
	public function __construct() {
		add_shortcode( 'iwp_congress_first_block', [ $this, 'output' ] );
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'iwp_congress_first_block', [ $this, 'map' ] );
		}
	}
	
	/**
	 * Map function
	 *
	 * @return array
	 */
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Congress First Block', 'iwp' ),
			'description'             => esc_html__( 'Add new Congress First Block', 'iwp' ),
			'base'                    => 'iwp_congress_first_block',
			'category'                => __( 'IWP', 'iwp' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'dropdown',
					'heading'     => __( 'Style', 'iwp' ),
					'param_name'  => 'style',
					'value'       => [
						'Select Style'           => 'one',
						'Style Info And Images'  => 'one_style',
						'Style Date'             => 'two',
						'Style Info And Contact' => 'three',
						'Style Four'             => 'four',
					],
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'iwp' ),
					'param_name'  => 'title',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_raw_html',
					'heading'     => __( 'Action block', 'iwp' ),
					'param_name'  => 'action_block',
					'value'       => '',
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'dependency'  => [ 'element' => 'style', 'value' => [ 'one_style', 'two', 'three' ] ],
				],
				[
					'type'        => 'textarea_html',
					'heading'     => __( 'Text', 'iwp' ),
					'param_name'  => 'content',
					'value'       => '',
					'admin_label' => false,
					'save_always' => false,
					'group'       => 'General',
					'dependency'  => [ 'element' => 'style', 'value' => [ 'one_style', 'three' ] ],
				],
				[
					'type'        => 'param_group',
					'heading'     => __( 'Organizers', 'iwp' ),
					'param_name'  => 'organizers',
					'value'       => '',
					'dependency'  => [ 'element' => 'style', 'value' => 'one_style' ],
					'params'      => [
						[
							'type'        => 'attach_image',
							'heading'     => __( 'Set Image', 'iwp' ),
							'param_name'  => 'image',
							'value'       => '',
							'admin_label' => true,
							'save_always' => true,
							'group'       => 'General',
						],
						[
							'type'        => 'vc_link',
							'heading'     => __( 'URL', 'iwp' ),
							'param_name'  => 'link',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
							'group'       => 'General',
						],
					],
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'param_group',
					'heading'     => __( 'Dates', 'iwp' ),
					'param_name'  => 'dates',
					'value'       => '',
					'dependency'  => [ 'element' => 'style', 'value' => 'two' ],
					'params'      => [
						[
							'type'        => 'textfield',
							'heading'     => __( 'Date', 'iwp' ),
							'param_name'  => 'date',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
							'group'       => 'General',
						],
						[
							'type'        => 'textarea',
							'heading'     => __( 'Description', 'iwp' ),
							'param_name'  => 'description',
							'value'       => '',
							'admin_label' => false,
							'save_always' => false,
							'group'       => 'General',
						],
					
					],
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	/**
	 * Output template
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public static function output( $atts, $content = null ) {
		ob_start();
		
		switch ( $atts['style'] ) {
			case 'one_style':
				include get_template_directory() . '/includes/wpbkComponents/template/CongressFirstBlock/template_one.php';
				break;
			case 'two':
				include get_template_directory() . '/includes/wpbkComponents/template/CongressFirstBlock/template_two.php';
				break;
			case 'three':
				include get_template_directory() . '/includes/wpbkComponents/template/CongressFirstBlock/template_three.php';
				break;
			case 'four':
				include get_template_directory() . '/includes/wpbkComponents/template/CongressFirstBlock/template_four.php';
				break;
			default:
				include get_template_directory() . '/includes/wpbkComponents/template/CongressFirstBlock/template_one.php';
		}
		
		return ob_get_clean();
	}
}