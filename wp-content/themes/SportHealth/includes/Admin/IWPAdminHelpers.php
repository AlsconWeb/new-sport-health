<?php
/**
 * Created 16.06.2021
 * Version 1.0.1
 * Last update 01.08.21
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\Admin;

/**
 * Class IWPAdminHelpers
 *
 * @package IWP\Admin
 */
class IWPAdminHelpers {
	/**
	 * Connect to DB.
	 *
	 * @var \QM_DB|\wpdb
	 */
	private $db;
	/**
	 * Status
	 *
	 * @var string[]
	 */
	private $status = [
		'pending',
		'completed',
		'rejects',
	];
	/**
	 * Params Key.
	 *
	 * @var string[]
	 */
	private $paramsKey = [
		'organization',
		'user_registered',
		'country',
		'form_participation',
	];
	/**
	 * Libs to generate EXEL file
	 *
	 * @var \SimpleXLSXGen
	 */
	private $exel;

	/**
	 * IWPAdminHelpers constructor.
	 */
	public function __construct () {
		global $wpdb;

		$this->db   = $wpdb;
		$this->exel = new \SimpleXLSXGen();
	}

	/**
	 * Get Payment Request table
	 *
	 * @param int      $countOutput Number of lines to display
	 * @param int|null $pages       Page Count
	 *
	 * @return array
	 */
	public function getPaymentRequestInfo ( int $countOutput, int $pages = null ): array {
		if ( null === $pages || $pages === 1 ) {
			$sql = "SELECT * FROM `{$this->db->prefix}iwp_payment` ORDER BY `id` DESC LIMIT " . $countOutput;
		}

		if ( $pages && 1 !== $pages ) {
			$offsetCount = ( $pages * $countOutput ) - $countOutput;
			$sql         = "SELECT * FROM `{$this->db->prefix}iwp_payment` ORDER BY `id` DESC LIMIT " . $countOutput . " OFFSET " .
										 $offsetCount;
		}

		$result = $this->db->get_results ( $sql );
		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}

	/**
	 * Get Count page
	 *
	 * @param int $countOutput
	 *
	 * @return false|float
	 */
	public function getPaymentRequestCountPage ( int $countOutput ) {
		$sql    = "SELECT COUNT(`id`) FROM `{$this->db->prefix}iwp_payment`  WHERE 1";
		$result = (int) $this->db->get_results ( $sql, ARRAY_N )[0][0];


		return ceil ( $result / $countOutput );
	}

	/**
	 * Get Count page
	 *
	 * @param int $countOutput
	 *
	 * @return false|float
	 */
	public function getReportsDocCountPage ( int $countOutput ) {
		$sql    = "SELECT COUNT(`id`) FROM `{$this->db->prefix}iwp_doc_list`  WHERE 1";
		$result = (int) $this->db->get_results ( $sql, ARRAY_N )[0][0];


		return ceil ( $result / $countOutput );
	}

	/**
	 * Get Count page
	 *
	 * @param int $countOutput
	 *
	 * @return false|float
	 */
	public function getUserListCountPage ( int $countOutput ) {
		$sql    = "SELECT COUNT(`user_id`) FROM `{$this->db->prefix}usermeta`  WHERE  `meta_value` LIKE '%\"conference\\_participant\"%'";
		$result = (int) $this->db->get_results ( $sql, ARRAY_N )[0][0];


		return ceil ( $result / $countOutput );
	}

	/**
	 * Get Payment Info by User ID
	 *
	 * @param int $userID User ID
	 *
	 * @return array
	 */
	public function getPaymentRequestInfoByUserID ( int $userID ): array {
		$sql    = "SELECT * FROM `{$this->db->prefix}iwp_payment` WHERE `userID` = " . $userID;
		$result = $this->db->get_results ( $sql );
		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}

	/**
	 * Change Payment Status
	 *
	 * @param int    $userID User ID
	 * @param string $status Status pending|completed|rejects
	 *
	 * @return bool
	 */
	public function changeStatus ( int $userID, string $status ): bool {

		if ( ! in_array ( $status, $this->status ) ) {
			return false;
		}

		$result = $this->db->update ( $this->db->prefix . 'iwp_payment', [
			'status' => $status,
		], [ 'userID' => $userID, ] );

		if ( false !== $result ) {
			return true;
		}

		return false;
	}

	/**
	 * Delete Payment Request in DB
	 *
	 * @param int $userID User ID
	 *
	 * @return bool
	 */
	public function deletePaymentsRequest ( int $userID ): bool {
		$result = $this->db->delete ( $this->db->prefix . 'iwp_payment', [ 'userID' => $userID ] );

		if ( $result ) {
			return true;
		}

		return false;
	}

	/**
	 * Delete file by attachment ID;
	 *
	 * @param $file_id
	 *
	 * @return bool[]|string[]
	 */
	public function removeFile ( $file_id ): array {
		$delete = wp_delete_attachment ( $file_id, true );
		if ( false === $delete ) {
			return [ 'error' => "File has not been deleted" ];
		}

		return [ 'success' => true ];
	}

	/**
	 * Get Reports Docs
	 *
	 * @param int      $countOutput Count outputs
	 * @param int|null $pages       Page Num
	 *
	 * @return array|object
	 */
	public function getReportDocs ( int $countOutput, int $pages = null ) {
		if ( null === $pages || $pages === 1 ) {
			$sql = "SELECT * FROM `{$this->db->prefix}iwp_doc_list` ORDER BY `date` DESC LIMIT " . $countOutput;
		}

		if ( $pages && 1 !== $pages ) {
			$offsetCount = ( $pages * $countOutput ) - $countOutput;
			$sql         = "SELECT * FROM `{$this->db->prefix}iwp_doc_list` ORDER BY `date` DESC LIMIT " . $countOutput . " OFFSET " .
										 $offsetCount;
		}

		$result = $this->db->get_results ( $sql );
		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}

	/**
	 * Get Comment by Doc ID
	 *
	 * @param int $dcoID
	 *
	 * @return array|object
	 */
	public function getCommentByDocID ( int $dcoID ) {
		$sql    = "SELECT * FROM `{$this->db->prefix}iwp_doc_comment` WHERE `docID` = " . $dcoID . " and `parentComment` = 0";
		$result = $this->db->get_results ( $sql );

		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}

	/**
	 * Add Replay Comment
	 *
	 * @param int    $commentID   Parent Comment
	 * @param string $commentText Text
	 * @param int    $docID       Doc ID
	 *
	 * @return bool
	 */
	public function addRepayComment ( int $commentID, string $commentText, int $docID ): bool {

		$result = $this->db->insert ( $this->db->prefix . 'iwp_doc_comment', [
			'userID'        => 1,
			'docID'         => $docID,
			'comment'       => $commentText,
			'parentComment' => $commentID,
			'readStatus'    => 0,
		], [ '%d', '%d', '%s', '%d', '%d' ] );

		if ( ! $result ) {
			return false;
		}

		return true;
	}

	/**
	 * Get Comment Child
	 *
	 * @param int $commentID Comment ID
	 *
	 * @return array|object
	 */
	public function getChildComment ( int $commentID ) {
		$sql    = "SELECT * FROM `{$this->db->prefix}iwp_doc_comment` WHERE `parentComment` = " . $commentID;
		$result = $this->db->get_results ( $sql );

		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}

	/**
	 * Send User info Email
	 *
	 * @param string $action      Action
	 * @param int    $userID      User ID
	 * @param string $messageText Text message output in email
	 *
	 * @return bool|mixed|void
	 */
	public function sendEmail ( string $action, int $userID, string $messageText ) {
		$mail   = false;
		$header = [
			'From: Международный Научный Конгресс Спорт, Человек, Здоровье: <info@sport-health.ru>',
			'reply-to:' . get_option ( 'admin_email' ),
			'content-type:text/html',
		];
		switch ( $action ) {
			case 'replay_comment':
				$responseMessage = __ ( 'You got a response to your file comment', 'iwp' );
				ob_start ();
				include_once get_template_directory () . '/includes/emailTemplate/info.php';
				$userEmail = get_user_by ( 'id', $userID )->user_email;
				$message   = ob_get_clean ();
				$mail      = wp_mail ( $userEmail, $responseMessage, $message, $header );
				break;
			case 'change_status':
				$responseMessage = __ ( 'The status of the document has been changed', 'iwp' );
				ob_start ();
				include_once get_template_directory () . '/includes/emailTemplate/info.php';
				$userEmail = get_user_by ( 'id', $userID )->user_email;
				$message   = ob_get_clean ();
				$mail      = wp_mail ( $userEmail, $responseMessage, $message, $header );
				break;
			case 'delete':
				$responseMessage = __ ( 'Document has been deleted', 'iwp' );
				ob_start ();
				include_once get_template_directory () . '/includes/emailTemplate/info.php';
				$userEmail = get_user_by ( 'id', $userID )->user_email;
				$message   = ob_get_clean ();
				$mail      = wp_mail ( $userEmail, $responseMessage, $message, $header );
				break;
		}

		return $mail;
	}

	/**
	 * Changing the status of a document
	 *
	 * @param int    $docID  Attachment ID
	 * @param string $status Status
	 *
	 * @return bool
	 */
	public function changeStatusReportDoc ( int $docID, string $status ): bool {
		$response = $this->db->update ( $this->db->prefix . 'iwp_doc_list', [ 'status' => $status ], [ 'docID' => $docID ] );

		if ( false === $response ) {
			return false;
		}

		return true;
	}

	/**
	 * Remove Doc in DB
	 *
	 * @param int $docID Attachment ID
	 *
	 * @return bool
	 */
	public function removeReportDoc ( int $docID ): bool {
		$response = $this->db->delete ( $this->db->prefix . 'iwp_doc_list', [ 'docID' => $docID ] );

		if ( $response ) {
			return true;
		}

		return false;
	}

	/**
	 * User List of Conference
	 *
	 * @param int        $countOutput Count Output User
	 * @param int|null   $pages       Page Number
	 * @param array|null $filter      Filter params
	 *
	 * @return array
	 */
	public function getUserList ( int $countOutput, int $pages = null, array $filters = null ): array {

		$userLists = [];
		if ( null === $filters ) {
			$users = get_users ( [
				'role__in' => 'conference_participant',
				'number'   => $countOutput,
				'paged'    => $pages ?? 1,
				'fields'   => 'all',
			] );
		}
		if ( ! empty( $filters ) ) {
			$mataQuery             = [];
			$mataQuery['relation'] = 'AND';
			foreach ( $filters as $key => $filter ) {
				if ( 'user_registered' !== $key ) {
					$mataQuery[] = [ 'key' => $key, 'value' => $filter ];
				}
			}


			$users = get_users ( [
				'role__in'   => 'conference_participant',
				'number'     => $countOutput,
				'paged'      => $pages ?? 1,
				'fields'     => 'all',
				'meta_query' => $mataQuery,
				'orderby'    => 'registered',
				'order'      => $filters['user_registered'],
			] );
		}


		foreach ( $users as $user ) {
			$sql = "SELECT `meta_key`,`meta_value` FROM `{$this->db->prefix}usermeta` WHERE `user_id` = " . $user->ID;

			$results = $this->db->get_results ( $sql );

			foreach ( $results as $result ) {
				$userLists[ $user->ID ][ $result->meta_key ] = $result->meta_value;
			}
		}

		return $userLists;
	}

	/**
	 * Get Payment Status By User ID
	 *
	 * @param int $userID User ID
	 *
	 * @return array|object|null
	 */
	public function getPaymentStatusByUserID ( int $userID ) {
		$sql = "SELECT `status` FROM `{$this->db->prefix}iwp_payment` WHERE `userID` = " . $userID;

		return $this->db->get_results ( $sql )[0]->status;
	}

	/**
	 * User Statistic Count by Date
	 * Valid values day | month | year
	 *
	 * @param String $time day | month | year
	 *
	 * @return mixed
	 */
	public function userStatisticFromData ( string $time ) {
		$start = '';
		$end   = '';

		if ( 'day' === $time ) {
			$start = current_time ( 'Y-m-d' );
			$end   = strtotime ( $start );
		}

		if ( 'month' === $time ) {
			$start = date ( 'Y-m-01' );
			$end   = date ( 'Y-m-t' );
		}

		if ( 'year' === $time ) {
			$start = date ( 'Y-m-d', strtotime ( 'first day of january this year' ) );
			$end   = date ( 'Y-m-d', strtotime ( 'last day of december this year' ) );
		}

		if ( 'day' === $time ) {
			$sql = "SELECT COUNT( `ID` ) FROM `{$this->db->prefix}users` INNER JOIN `{$this->db->prefix}usermeta` ON( "
						 . $this->db->prefix . "users . ID = " . $this->db->prefix . "usermeta . user_id ) WHERE `user_registered` LIKE '%{$start}%' and ( ( ( sph_usermeta . meta_key = 'sph_capabilities' and " . $this->db->prefix . "usermeta . meta_value LIKE '%\"conference\\_participant\"%' ) ) )";
		} else {
			$sql = "SELECT COUNT( `ID` ) FROM `{$this->db->prefix}users` INNER JOIN `{$this->db->prefix}usermeta` ON( "
						 . $this->db->prefix . "users . ID = " . $this->db->prefix . "usermeta . user_id ) WHERE `user_registered` BETWEEN DATE '{$start}' and DATE '{$end}' and ( ( ( sph_usermeta . meta_key = 'sph_capabilities' and " . $this->db->prefix . "usermeta . meta_value LIKE '%\"conference\\_participant\"%' ) ) )";
		}

		return $this->db->get_results ( $sql, ARRAY_N )[0][0];
	}

	/**
	 * Will return unique organizations that were registered for the event
	 *
	 * @return array|object|null
	 */
	public function getUniqueOrganization () {
		$sql = "SELECT DISTINCT `meta_value` FROM `{$this->db->prefix}usermeta` WHERE `meta_key` = 'organization'";

		return $this->db->get_results ( $sql, OBJECT );
	}

	/**
	 * Gets Parse Params
	 *
	 * @param array $params $_GET Array
	 *
	 * @return array|null
	 */
	public function getParamsParse ( array $params ): ?array {
		$paramsArray = [];
		foreach ( $params as $key => $param ) {
			if ( in_array ( $key, $this->paramsKey ) ) {
				$paramsArray[ $key ] = $param;
			}
		}

		if ( empty( $paramsArray ) ) {
			return null;
		}

		return $paramsArray;
	}

	/**
	 * Generate User Info XLSX file.
	 *
	 * @return bool
	 */
	public function userListGenerateExel (): bool {
		$data = [
			[
				'User ID',
				'ФИО',
				'ФИО EN',
				'Дата Рождения',
				'Пол',
				'Страна',
				'Область',
				'Город',
				'Телфон',
				'Email',
				'Место работы / учебы',
				'Организация',
				'Звание',
				'Должность',
				'Название Статьи',
				'Соавторы',
				'Номер конграсса',
				'Форма Участия',
				'Форма выступления',
				'Список тех. Средств',
			],
		];

		$usersInfo = $this->getUserList ( - 1 );
		foreach ( $usersInfo as $key => $user ) {
			$userData = [
				$key,
				$user['last_name'] . ' ' . $user['first_name'] . ' ' . $user['patronymic'],
				$user['fio_en'] ?? '-',
				$user['dob'] ?? '-',
				'male' === $user['gender'] ? 'Мужской' : 'Женский',
				$user['country'] ?? '-',
				$user['province'] ?? '-',
				$user['city'] ?? '-',
				$user['phone'] ?? '-',
				$user['nickname'] ?? '-',
				$user['work'] ?? '-',
				$user['organization'] ?? '-',
				$user['degree'] ?? '-',
				$user['position'] ?? '-',
				$user['article_title'] ?? '-',
				$user['coauthors'] ?? '-',
				$user['congress_section'] ?? '-',
				$user['form_participation'] ?? '-',
				$user['form_performance'] ?? '-',
				$user['technical_list'] ?? '-',
			];

			$data[] = $userData;
		}

		$baseDir = wp_upload_dir ()['basedir'] . '/export/';
		$xlsx    = $this->exel->fromArray ( $data )->saveAs (
			$baseDir . 'UserInfo-' . current_time ( 'Y-m-d' ) .
			'.xlsx'
		);

		return $xlsx;
	}

	/**
	 * Generate Payments Doc XLSX file.
	 *
	 * @return bool
	 */
	public function paymentDocGenerateExel () {
		$data = [
			[
				'User ID',
				'ФИО',
				'Статус',
				'Сылка на документ',
			],
		];

		$listDocs = $this->getPaymentRequestInfo ( 100000 );
		foreach ( $listDocs as $doc ) {
			$userHelpers = new \IWP\Helpers\User\IWPUserHelpers( $doc->userID );
			$fileUrl     = wp_get_attachment_url ( $doc->file, 'full' );
			$userInfo    = $userHelpers->getUserInfo ();
			$data[]      = [
				$doc->id,
				$userInfo['first_name'] . ' ' . $userInfo['last_name'],
				$doc->status,
				$fileUrl,
			];
		}
		$baseDir = wp_upload_dir ()['basedir'] . '/export/';
		$xlsx    = $this->exel->fromArray ( $data )->saveAs (
			$baseDir . 'PaymentDoc-' . current_time ( 'Y-m-d' ) .
			'.xlsx'
		);

		return $xlsx;
	}

	public function reportsDocGenerateExel (): bool {
		$data = [
			[
				'User ID',
				'ФИО',
				'Статус',
				'Сылка на документ',
			],
		];

		$listDocs = $this->getReportDocs ( 100000 );
		foreach ( $listDocs as $doc ) {
			$userHelpers = new \IWP\Helpers\User\IWPUserHelpers( $doc->userID );
			$fileUrl     = wp_get_attachment_url ( $doc->docID, 'full' );
			$userInfo    = $userHelpers->getUserInfo ();
			$data[]      = [
				$doc->id,
				$userInfo['first_name'] . ' ' . $userInfo['last_name'],
				$doc->status,
				$fileUrl,
			];
		}
		$baseDir = wp_upload_dir ()['basedir'] . '/export/';
		$xlsx    = $this->exel->fromArray ( $data )->saveAs (
			$baseDir . 'ReportsDoc-' . current_time ( 'Y-m-d' ) .
			'.xlsx'
		);

		return $xlsx;
	}
}