<?php
/**
 * Created 16.06.2021
 * Version 1.0.1
 * Last update 01.08.21
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\Admin;

/**
 * Class IWPAdminInit
 *
 * @package IWP\Admin
 */
class IWPAdminInit {
	protected $helpers;

	/**
	 * IWPAdminInit constructor.
	 */
	public function __construct() {

		$this->helpers = new IWPAdminHelpers();

		add_action( 'admin_menu', [ $this, 'registerPaymentDocPage' ] );
		add_action( 'admin_menu', [ $this, 'registerDocReports' ] );
		add_action( 'admin_menu', [ $this, 'registerUserConference' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'addScript' ] );

		add_action( 'wp_ajax_action_payment_doc', [ $this, 'actionPaymentDoc' ] );
		add_action( 'wp_ajax_nopriv_action_payment_doc', [ $this, 'actionPaymentDoc' ] );

		add_action( 'wp_ajax_send_replay_comment', [ $this, 'replayComment' ] );
		add_action( 'wp_ajax_nopriv_send_replay_comment', [ $this, 'replayComment' ] );

		add_action( 'wp_ajax_change_status_doc_report', [ $this, 'actionReportDoc' ] );
		add_action( 'wp_ajax_nopriv_change_status_doc_report', [ $this, 'actionReportDoc' ] );

		add_action( 'wp_ajax_update_user_data', [ $this, 'updateUserInfo' ] );
		add_action( 'wp_ajax_nopriv_update_user_data', [ $this, 'updateUserInfo' ] );

		add_action( 'admin_post_nopriv_iwp_filter_user', [ $this, 'filterUserList' ] );
		add_action( 'admin_post_iwp_filter_user', [ $this, 'filterUserList' ] );

		add_action( 'wp_ajax_generate_exel_user_list', [ $this, 'generateExelUserList' ] );

		add_action( 'wp_ajax_generate_exel_doc_payment', [ $this, 'generateExelDocPayment' ] );

		add_action( 'wp_ajax_generate_exel_reports_doc', [ $this, 'generateExelDocReports' ] );
	}

	/**
	 * Register Payments Docs Page
	 */
	public function registerPaymentDocPage(): void {
		add_menu_page( __( 'Payment Docs for User', 'iwp' ), __( 'Payment Docs', 'iwp' ), 'manage_options', 'iwp-payment-doc', [
			$this,
			'addPaymentDocPage',
		], 'dashicons-pdf', 10 );
	}

	/**
	 * Output Payment Docs Page
	 */
	public function addPaymentDocPage(): void {
		ob_start();
		include_once get_template_directory() . '/template_part/admin/paymentDoc.php';
		echo ob_get_clean();
	}

	/**
	 * Register Page User List of Conference
	 */
	public function registerUserConference(): void {
		add_menu_page( __( 'User List of Conference', 'iwp' ), __( 'User List', 'iwp' ), 'manage_options', 'iwp-user-list', [
			$this,
			'addUserListConferencePage',
		], 'dashicons-buddicons-buddypress-logo', 10 );
	}

	/**
	 * Output User List of Conference Page
	 */
	public function addUserListConferencePage(): void {
		ob_start();
		include_once get_template_directory() . '/template_part/admin/userList.php';
		echo ob_get_clean();
	}

	/**
	 * Register Reports Doc page
	 */
	public function registerDocReports(): void {
		add_menu_page( __( 'Report or Presentation', 'iwp' ), __( 'Report Docs', 'iwp' ), 'manage_options', 'iwp-report-doc', [
			$this,
			'addReportDocPage',
		], 'dashicons-media-interactive', 10 );
	}

	/**
	 * Output Reports Doc page
	 */
	public function addReportDocPage(): void {
		ob_start();
		include_once get_template_directory() . '/template_part/admin/reportDoc.php';
		echo ob_get_clean();
	}

	/**
	 * Add Script and Style
	 *
	 * @param $hook
	 */
	public function addScript( $hook ): void {
		if ( 'toplevel_page_iwp-payment-doc' === $hook || 'toplevel_page_iwp-report-doc' === $hook || 'toplevel_page_iwp-user-list' === $hook ) {
			wp_enqueue_style( 'bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css', [], '5.0.1',
				'' );

			wp_enqueue_script( 'iwp-admin', get_template_directory_uri() . '/assets/js/admin.js', [
				'jquery',
				'sweetalert2',
			], '' );

			wp_localize_script( 'iwp-admin', 'iwp', [
				'ajaxURL' => admin_url( 'admin-ajax.php' ),
			] );

			wp_enqueue_script( 'sweetalert2', 'https://cdn.jsdelivr.net/npm/sweetalert2@11.0.12/dist/sweetalert2.all.min.js', [ 'jquery' ], '', true );
		}
	}

	/**
	 * Action Handler Payment Doc Ajax
	 */
	public function actionPaymentDoc(): void {
		$userID = (int) $_POST['userID'];
		$action = $_POST['userAction'];

		if ( empty( $userID ) ) {
			wp_send_json_error( [ 'message' => __( 'No User ID', 'iwp' ) ] );
		}

		if ( empty( $action ) ) {
			wp_send_json_error( [ 'message' => __( 'No action', 'iwp' ) ] );
		}

		$response        = false;
		$responseMessage = '';

		switch ( $action ) {
			case'pending':
				$response        = $this->helpers->changeStatus( $userID, $action );
				$responseMessage = __( 'Status changed to pending', 'iwp' );
				$mail            = true;
				break;
			case 'completed':
				$response        = $this->helpers->changeStatus( $userID, $action );
				$responseMessage = __( 'Status changed to completed', 'iwp' );
				$header          = [
					'From: Международный Научный Конгресс Спорт, Человек, Здоровье: <info@sport-health.ru>',
					'reply-to:' . get_option( 'admin_email' ),
					'content-type:text/html',
				];
				ob_start();
				include_once get_template_directory() . '/includes/emailTemplate/statusChange.php';
				$userEmail = get_user_by( 'id', $userID )->user_email;
				$message   = ob_get_clean();
				$mail      = wp_mail( $userEmail, $responseMessage, $message,
					$header );
				break;
			case 'rejects':
				$response        = $this->helpers->changeStatus( $userID, $action );
				$responseMessage = __( 'Status changed to rejects', 'iwp' );
				$header          = [
					'From: Международный Научный Конгресс Спорт, Человек, Здоровье: <info@sport-health.ru>',
					'reply-to:' . get_option( 'admin_email' ),
					'content-type:text/html',
				];
				ob_start();
				include_once get_template_directory() . '/includes/emailTemplate/statusChange.php';
				$userEmail = get_user_by( 'id', $userID )->user_email;
				$message   = ob_get_clean();
				$mail      = wp_mail( $userEmail, $responseMessage, $message, $header );
				break;
			case 'delete':
				$response        = $this->helpers->deletePaymentsRequest( $userID );
				$fileID          = $this->helpers->getPaymentRequestInfoByUserID( $userID )[0]->file;
				$removeFile      = $this->helpers->removeFile( $fileID );
				$responseMessage = __( 'Document has been deleted', 'iwp' );
				$mail            = true;
				break;
		}

		if ( $response && $mail ) {
			wp_send_json_success( [ 'message' => $responseMessage ] );
		}

		wp_send_json_error( [ 'message' => __( "An error occurred please try again later", 'iwp' ) ] );
	}

	/**
	 * Repay Comment Ajax
	 */
	public function replayComment(): void {
		$parentComment = (int) $_POST['commentID'];
		$commentText   = sanitize_text_field( $_POST['comment'] );
		$docID         = (int) $_POST['docID'];
		$userID        = (int) $_POST['userID'];

		if ( empty( $parentComment ) ) {
			wp_send_json_error( [ 'message' => __( 'Comment ID is Empty', 'iwp' ) ] );
		}

		if ( empty( trim( $commentText ) ) ) {
			wp_send_json_error( [ 'message' => __( 'Comment is Empty', 'iwp' ) ] );
		}

		if ( empty( $docID ) ) {
			wp_send_json_error( [ 'message' => __( 'Doc ID is empty', 'iwp' ) ] );
		}

		$response = $this->helpers->addRepayComment( $parentComment, $commentText, $docID );

		if ( ! $response ) {
			wp_send_json_error( [ 'message' => __( 'No comment has been added', 'iwp' ) ] );
		}

		$mail = $this->helpers->sendEmail( 'replay_comment', $userID, $commentText );

		wp_send_json_success( [ 'message' => __( 'Comment add', 'iwp' ), 'comment' => $response, 'mail_send' => $mail ] );
	}

	/**
	 * Change Status Reports Doc Ajax
	 */
	public function actionReportDoc(): void {
		$docID  = $_POST['docID'];
		$status = $_POST['status'];
		$userID = $_POST['userID'];

		if ( empty( $docID ) ) {
			wp_send_json_error( [ 'message' => __( 'Doc ID is Empty', 'iwp' ) ] );
		}

		if ( empty( $status ) ) {
			wp_send_json_error( [ 'message' => __( 'Action is Empty', 'iwp' ) ] );
		}

		if ( empty( $userID ) ) {
			wp_send_json_error( [ 'message' => __( 'User ID is Empty', 'iwp' ) ] );
		}

		$mail          = false;
		$file          = false;
		$docListRemove = false;

		switch ( $status ) {
			case'publish':
				$message      = __( "Your article has been accepted for publication", 'iwp' );
				$statusChange = $this->helpers->changeStatusReportDoc( $docID, 'publish' );
				$mail         = $this->helpers->sendEmail( 'change_status', $userID, $message );
				break;
			case'draft':
				$message      = __( "Document status changed 'under review'", 'iwp' );
				$statusChange = $this->helpers->changeStatusReportDoc( $docID, 'draft' );
				$mail         = $this->helpers->sendEmail( 'change_status', $userID, $message );
				break;
			case'rejects':
				$message      = __( "The status of the document has been changed to declined", 'iwp' );
				$statusChange = $this->helpers->changeStatusReportDoc( $docID, 'rejects' );
				$mail         = $this->helpers->sendEmail( 'change_status', $userID, $message );
				break;
			case'delete':
				$message       = __( "Your file has been deleted", 'iwp' );
				$file          = $this->helpers->removeFile( $docID );
				$docListRemove = $this->helpers->removeReportDoc( $docID );
				$mail          = $this->helpers->sendEmail( 'change_status', $userID, $message );
				break;
		}

		if ( $mail ) {
			wp_send_json_success( [
				'message'          => __( 'Status updated successfully on: ', 'iwp ' ) . $status,
				'status_change'    => $statusChange,
				'file_remove'      => $file,
				'doc_remove_in_db' => $docListRemove,

			] );
		}

		wp_send_json_error( [ 'message' => 'An error occurred, please try again later', 'iwp' ] );

	}

	/**
	 * Update User Info Ajax
	 */
	public function updateUserInfo(): void {

		$userID = $_POST['userID'];
		unset( $_POST['action'], $_POST['userID'] );
		$userInfo = $_POST;

		if ( empty( $userID ) ) {
			wp_send_json_error( [ 'message' => __( 'User ID is Empty', 'iwp' ) ] );
		}

		if ( empty( $userInfo ) ) {
			wp_send_json_error( [ 'message' => __( 'User Info is Empty', 'iwp' ) ] );
		}

		foreach ( $userInfo as $key => $item ) {
			update_user_meta( $userID, $key, $item );
		}

		wp_send_json_success( [ 'message' => __( 'Update User Information', 'iwp' ) ] );
	}

	/**
	 * Filter Handler User List Page
	 */
	public function filterUserList(): void {

		if ( empty( $_POST ) || ! wp_verify_nonce( $_POST['iwp_filter_user_nonce'], 'iwp_filter_user_form' ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '&error="nonce_error"', 301 );
			exit;
		}

		$request     = $_POST['iwp_filter'];
		$filterParam = [];

		foreach ( $request as $key => $item ) {
			if ( '0' !== $item ) {
				$filterParam[ $key ] = $item;
			}
		}

		wp_redirect( $_REQUEST['_wp_http_referer'] . '&' . http_build_query( $filterParam, '&', '&' ) );
	}

	/**
	 * Ajax Handler Generate User List XLSX File.
	 */
	public function generateExelUserList(): void {
		$generateFile = $this->helpers->userListGenerateExel();
		if ( $generateFile ) {
			$baseUrl = wp_upload_dir()['baseurl'] . '/export/';
			$url     = $baseUrl . 'UserInfo-' . current_time( 'Y-m-d' ) . '.xlsx';
			wp_send_json_success( [ 'url' => $url, ] );
		}

		wp_send_json_error( [ 'file_generate' => $generateFile ] );
	}

	/**
	 * Ajax Handler Generate payment doc XLSX File.
	 */
	public function generateExelDocPayment(): void {
		$generateFile = $this->helpers->paymentDocGenerateExel();

		if ( $generateFile ) {
			$baseUrl = wp_upload_dir()['baseurl'] . '/export/';
			$url     = $baseUrl . 'PaymentDoc-' . current_time( 'Y-m-d' ) . '.xlsx';
			wp_send_json_success( [ 'url' => $url, ] );
		}

		wp_send_json_error( [ 'file_generate' => $generateFile ] );
	}

	/**
	 * Ajax Handler Generate reports doc XLSX File.
	 */
	public function generateExelDocReports(): void {
		$generateFile = $this->helpers->reportsDocGenerateExel();

		if ( $generateFile ) {
			$baseUrl = wp_upload_dir()['baseurl'] . '/export/';
			$url     = $baseUrl . 'ReportsDoc-' . current_time( 'Y-m-d' ) . '.xlsx';
			wp_send_json_success( [ 'url' => $url, ] );
		}

		wp_send_json_error( [ 'file_generate' => $generateFile ] );
	}
}