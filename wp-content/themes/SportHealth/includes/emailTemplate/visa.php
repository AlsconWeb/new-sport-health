<?php
/**
 * Created 03.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="x-apple-disable-message-reformatting">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">
	<title></title>
	<style>
      table,
      td,
      div,
      h1,
      p {
          font-family: 'Inter', sans-serif;
      }
	</style>
</head>

<body style="margin:0;padding:0;">
<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
	<tr>
		<td align="center" style="padding:0;">
			<table role="presentation"
			       style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
				<tr style="background:#0DA2E0; padding:15px 0px;">
					<td align="left" width="90px" style="padding: 15px 40px;">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-white.svg" alt="" width="90px"
						     style="margin-right: 8px; height:auto;display:inline-block; vertical-align: middle;"/>
						<h1
							style="font-family: 'Inter', sans-serif; color:#ffffff; font-size: 11px; line-height: 1.3;font-weight: 600; display: inline-block; vertical-align: middle;">
							X МЕЖДУНАРОДНЫЙ НАУЧНЫЙ КОНГРЕСС <br> "СПОРТ, ЧЕЛОВЕК, ЗДОРОВЬЕ" <br> X INTERNATIONAL SCIENTIFIC CONGRESS
							<br>"SPORT, PEOPLE AND HEALTH"</h1>
					</td>
				</tr>
				<tr>
					<td style="padding:80px 40px 60px;">
						<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
							<tr>
								<td style="padding: 0;">
									<h2
										style="color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 18px; line-height: 1.5;">
										Контакты</h2>
									<table>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px;color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Имя:
											</td>
											<td
												style="padding-bottom:8px;padding-left:15px;  color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<?php echo $dataArray['iwp_first_name']; ?>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px;color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Фамилия:
											</td>
											<td
												style="padding-bottom:8px;padding-left:15px;  color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<?php echo $dataArray['iwp_last_name']; ?>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Пол:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<?php echo $dataArray['iwp_gender']; ?>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Национальность:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<?php echo $dataArray['iwp_nationality']; ?>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Паспорт серия, номер:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<?php echo $dataArray['iwp_passport']; ?>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Дата рождения:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<?php echo $dataArray['iwp_dob']; ?>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Страна:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<p><?php echo $dataArray['iwp_country'] ?></p>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Город:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<p><?php echo $dataArray['iwp_city'] ?></p>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Адресс:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<p><?php echo $dataArray['iwp_address'] ?></p>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Почтовый индекс:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<p><?php echo $dataArray['iwp_postcode'] ?></p>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Email:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<p><?php echo $dataArray['iwp_email'] ?></p>
											</td>
										</tr>
										<tr style="vertical-align: top;">
											<td
												style="min-width: 140px; padding-bottom:8px; color:#323033; font-family: 'Inter', sans-serif; font-weight: 600; font-size: 14px; line-height: 1.5;">
												Телефон:
											</td>
											<td
												style="padding-bottom:8px; padding-left:15px; color:#323033; font-family: 'Inter', sans-serif; font-size: 14px; line-height: 1.5;">
												<p><?php echo $dataArray['iwp_phone'] ?></p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr style="background:#0DA2E0;">
					<td style="padding:15px 40px;">
						<h3
							style="font-family: 'Inter', sans-serif; color:#ffffff; font-weight: 600; font-size: 11px; line-height: 1.3; text-transform: uppercase; margin: 0;">
							2021 <br>Санкт-Петербург, Россия <br> Saint Petersburg, Russia</h3>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>

</html>
