<?php
/**
 * Created 17.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

use IWP\Helpers\User\IWPUserHelpers;

$userHelpers = new IWPUserHelpers( $userID );
$userInfo    = $userHelpers->getUserInfo();
?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="x-apple-disable-message-reformatting">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">
	<title></title>
	<style>
      table,
      td,
      div,
      h1,
      p {
          font-family: 'Inter', sans-serif;
      }
	</style>
</head>

<body style="margin:0;padding:0;">
<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
	<tr>
		<td align="center" style="padding:0;">
			<table role="presentation"
			       style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
				<tr style="background:#0DA2E0; padding:15px 0px;">
					<td align="left" width="90px" style="padding: 15px 40px;">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-white.svg" alt="" width="90px"
						     style="margin-right: 8px; height:auto;display:inline-block; vertical-align: middle;"/>
						<h1
							style="font-family: 'Inter', sans-serif; color:#ffffff; font-size: 11px; line-height: 1.3;font-weight: 600; display: inline-block; vertical-align: middle;">
							X МЕЖДУНАРОДНЫЙ НАУЧНЫЙ КОНГРЕСС <br> "СПОРТ, ЧЕЛОВЕК, ЗДОРОВЬЕ" <br> X INTERNATIONAL SCIENTIFIC CONGRESS
							<br>"SPORT, PEOPLE AND HEALTH"</h1>
					</td>
				</tr>
				<tr>
					<td style="padding:80px 40px 60px;">
						<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
							<tr>
								<td style="padding: 0;color:#153643;">
									<p
										style="font-family: 'Inter', sans-serif; line-height: 1.5; color: #323033;font-size: 14px;"><?php echo $responseMessage; ?></p>
									<p
										style="font-family: 'Inter', sans-serif; line-height: 1.5; color: #323033;font-size: 14px;"><?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name'] ?>
									<hr style="border-color: transparent; border-top-color:#A5A8B5; margin-top: 30px;">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr style="background:#0DA2E0;">
					<td style="padding:15px 40px;">
						<h3
							style="font-family: 'Inter', sans-serif; color:#ffffff; font-weight: 600; font-size: 11px; line-height: 1.3; text-transform: uppercase; margin: 0;">
							2021 <br>Санкт-Петербург, Россия <br> Saint Petersburg, Russia</h3>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>

</html>
