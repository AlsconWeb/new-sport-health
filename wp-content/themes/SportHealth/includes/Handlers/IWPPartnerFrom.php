<?php
/**
 * Created 03.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\Handlers;

use IWP\Helpers\Form\IWPForm;

/**
 * Class IWPPartnerFrom
 *
 * @package IWP\Handlers
 */
class IWPPartnerFrom {
	protected $helpers;
	
	public function __construct() {
		$this->helpers = new IWPForm();
		
		add_action( 'admin_post_nopriv_iwp_partner_form', [ $this, 'partnerFrom' ] );
		add_action( 'admin_post_iwp_partner_form', [ $this, 'partnerFrom' ] );
	}
	
	public function partnerFrom() {
		if( empty( $_POST ) || ! wp_verify_nonce( $_POST['iwp_partner_form_nonce'], 'iwp_partner_form' ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="nonce_error"', 301 );
			exit;
		}
		
		$request   = $_POST;
		$dataArray = $this->helpers->gatPostDataArray( $request );
		if( empty( $dataArray['iwp_email'] ) || ! is_email( $dataArray['iwp_email'] ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Invalid Email"', 301 );
			exit;
		}
		
		if( empty( $dataArray['iwp_name'] ) || strlen( $dataArray['iwp_name'] ) < 5 ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Full name cannot be less than 5 characters"', 301 );
			exit;
		}
		
		if( empty( $dataArray['iwp_company_name'] ) || strlen( $dataArray['iwp_company_name'] ) < 3 ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Company name cannot be less than 3 characters"', 301 );
			exit;
		}
		
		ob_start();
		
		include_once get_template_directory() . '/includes/emailTemplate/partner.php';
		$header = [ 'From: Email с формы Стать партнером ', 'content-type: text/html', ];
		
		$message = ob_get_clean();
		
		$mail = wp_mail( $dataArray['iwp_to_email'] ?? get_option( 'admin_email' ), 'Email с формы Стать партнером ' .
		                                                                            get_bloginfo( 'url' ),
			$message, $header );
		
		if( $mail ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?success="Email was sent successfully"', 301 );
			exit;
		}
		
		wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Email was not send, try again in a few minutes"', 301 );
		exit;
	}
}