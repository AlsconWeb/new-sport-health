<?php
/**
 * Created 26.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\Handlers;

use IWP\Helpers\Form\IWPForm;

/**
 * Class IWPVisaForm
 *
 * @package IWP\Handlers
 */
class IWPVisaForm {
	
	protected $helpers;
	
	/**
	 * IWPVisaForm constructor.
	 */
	public function __construct() {
		$this->helpers = new IWPForm();
		
		add_action( 'admin_post_nopriv_iwp_visa_form', [ $this, 'visaFrom' ] );
		add_action( 'admin_post_iwp_visa_form', [ $this, 'visaFrom' ] );
	}
	
	/**
	 *  Handler Visa Form
	 */
	public function visaFrom(): void {
		
		if( empty( $_POST ) || ! wp_verify_nonce( $_POST['iwp_visa_form_nonce'], 'iwp_visa_form' ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="nonce_error"', 301 );
			exit;
		}
		$request    = $_POST;
		$dataArray  = $this->helpers->gatPostDataArray( $request );
		$attachment = '';
		if( empty( $dataArray['iwp_email'] ) || ! is_email( $dataArray['iwp_email'] ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Invalid Email"', 301 );
			exit;
		}
		
		$file = &$_FILES['iwp_avatar'];
		if( ! empty( $file ) ) {
			$fileData   = $this->helpers->uploadFile( $file );
			$attachment = $fileData['patch'];
		}
		
		if( ! empty( $file ) && isset( $fileData['error'] ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Invalid File format or Size"', 301 );
			exit;
		}
		
		ob_start();
		
		include_once get_template_directory() . '/includes/emailTemplate/visa.php';
		
		$message = ob_get_clean();
		
		$header = [ 'From: Email Оформление визы ', 'content-type: text/html', ];
		
		
		$mail = wp_mail( $dataArray['iwp_to_email'] ?? get_option( 'admin_email' ), 'Email Оформление визы с сайта  ' .
		                                                                            get_bloginfo( 'url' ), $message, $header,
			$attachment );
		
		if( $mail ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?success="Email was sent successfully"', 301 );
			exit;
		}
		
		wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Email was not send, try again in a few minutes"', 301 );
		exit;
	}
	
	
}