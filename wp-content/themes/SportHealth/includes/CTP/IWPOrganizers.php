<?php
/**
 * Created 22.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\CTP;

/**
 * Class IWPOrganizers
 *
 * @package IWP\CTP
 */
class IWPOrganizers {
	/**
	 * IWPOrganizers constructor.
	 */
	public function __construct() {
		$this->createTaxonomy();
		$this->registerPostTypes();
	}
	
	/**
	 * Register Post Types
	 */
	public function registerPostTypes() {
		register_post_type( 'organizers', [
			'label'         => null,
			'labels'        => [
				'name'               => __( 'Organizers', 'iwp' ),
				'singular_name'      => __( 'Organizer', 'iwp' ),
				'add_new'            => __( 'Add Organizers', 'iwp' ),
				'add_new_item'       => __( 'Add Organizers', 'iwp' ),
				'edit_item'          => __( 'Edit Organizers', 'iwp' ),
				'new_item'           => __( 'New Organizers', 'iwp' ),
				'view_item'          => __( 'View Organizers', 'iwp' ),
				'search_items'       => __( 'Search Organizers', 'iwp' ),
				'not_found'          => __( 'Not found', 'iwp' ),
				'not_found_in_trash' => 'Not found in trash',
				'parent_item_colon'  => '',
				'menu_name'          => __( 'Organizers', 'iwp' ),
			],
			'description'   => '',
			'public'        => true,
			'menu_position' => 10,
			'menu_icon'     => 'dashicons-admin-users',
			'hierarchical'  => false,
			'supports'      => [ 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions', ],
			'taxonomies'    => [],
			'has_archive'   => false,
			'rewrite'       => true,
			'query_var'     => true,
		] );
	}
	
	
	public function createTaxonomy() {
		register_taxonomy( 'organizers_conference', [ 'organizers' ], [
			'label'             => __( 'Organizers Conference', 'iwp' ),
			'labels'            => [
				'name'              => __( 'Organizers type', 'iwp' ),
				'singular_name'     => __( 'Organizers type', 'iwp' ),
				'search_items'      => __( 'Search Organizers', 'iwp' ),
				'all_items'         => __( 'All Organizers', 'iwp' ),
				'view_item '        => __( 'View Organizers', 'iwp' ),
				'parent_item'       => __( 'Parent Organizers', 'iwp' ),
				'parent_item_colon' => __( 'Parent Organizers:', 'iwp' ),
				'edit_item'         => __( 'Edit Organizers', 'iwp' ),
				'update_item'       => __( 'Update Organizers', 'iwp' ),
				'add_new_item'      => __( 'Add New Organizers', 'iwp' ),
				'new_item_name'     => __( 'New Organizers Name', 'iwp' ),
				'menu_name'         => __( 'Organizers type', 'iwp' ),
			],
			'description'       => '',
			'public'            => true,
			'hierarchical'      => false,
			'rewrite'           => true,
			'capabilities'      => [],
			'show_admin_column' => true,
		] );
	}
}