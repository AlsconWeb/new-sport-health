<?php
/**
 * Created 22.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\CTP;

/**
 * Class IWPArchiveCongress
 *
 * @package IWP\CTP
 */
class IWPArchiveCongress {
	public function __construct() {
		$this->registerPostTypes();
	}
	
	/**
	 * Register Post Types
	 */
	public function registerPostTypes() {
		register_post_type( 'archive-congress', [
			'label'         => null,
			'labels'        => [
				'name'               => __( 'Archive Congress', 'iwp' ),
				'singular_name'      => __( 'Archive Congress', 'iwp' ),
				'add_new'            => __( 'Add', 'iwp' ),
				'add_new_item'       => __( 'Add', 'iwp' ),
				'edit_item'          => __( 'Edit', 'iwp' ),
				'new_item'           => __( 'New', 'iwp' ),
				'view_item'          => __( 'View', 'iwp' ),
				'search_items'       => __( 'Search', 'iwp' ),
				'not_found'          => __( 'Not found', 'iwp' ),
				'not_found_in_trash' => 'Not found in trash',
				'parent_item_colon'  => '',
				'menu_name'          => __( 'Archive Congress', 'iwp' ),
			],
			'description'   => '',
			'public'        => true,
			'menu_position' => 20,
			'menu_icon'     => 'dashicons-media-archive',
			'hierarchical'  => true,
			'supports'      => [ 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions', ],
			'has_archive'   => false,
			'rewrite'       => true,
			'query_var'     => true,
		] );
		
		
	}
}