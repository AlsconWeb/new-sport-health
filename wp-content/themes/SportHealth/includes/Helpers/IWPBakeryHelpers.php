<?php
/**
 * Created 24.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\Helpers;


class IWPBakeryHelpers {
	
	/**
	 * IWPBakeryHelpers constructor.
	 */
	public function __construct() {
	
	}
	
	/**
	 * Load Country JSON file
	 *
	 * @return false|mixed
	 */
	private function loadCountryJSON() {
		$json_data = json_decode( file_get_contents( get_template_directory() . '/includes/Helpers/countries/countries.json' ) );
		if( $json_data ) {
			return $json_data;
		}
		
		return false;
	}
	
	/**
	 * countyList() Generate array to dropdown list.
	 *
	 * @return array
	 */
	public function countryList(): array {
		$county     = $this->loadCountryJSON();
		$countyList = [];
		if( $county ) {
			foreach ( $county as $key => $item ) {
				$countyList[ $item->name ] = [ 'cord' => implode( ',', $item->latlng ) ];
				$countyList[ $item->name ] = [ "name_ru" => $item->name_ru ];
			}
			
			return $countyList;
		}
		
		return [];
	}
	
}