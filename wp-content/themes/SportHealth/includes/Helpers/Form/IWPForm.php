<?php
/**
 * Created 26.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\Helpers\Form;

/**
 * Class IWPForm
 *
 * @package IWP\Helpers\Form
 */
class IWPForm {
	
	private $packages = [
		[
			'name_en'    => 'BRONZE SPONSORSHIP PACKAGE',
			'name_ru'    => 'БРОНЗОВЫЙ СПОНСОР',
			'cost_en'    => 1200,
			'cost_ru'    => 100000,
			'param_name' => 'bronze',
		],
		[
			'name_en'    => 'BRONZE SPONSORSHIP PACKAGE',
			'name_ru'    => 'СЕРЕБРЯНЫЙ СПОНСОР',
			'cost_en'    => 2950,
			'cost_ru'    => 250000,
			'param_name' => 'silver',
		],
		[
			'name_en'    => 'GOLDEN SPONSORSHIP PACKAGE',
			'name_ru'    => 'ЗОЛОТОЙ СПОНСОР',
			'cost_en'    => 3500,
			'cost_ru'    => 250000,
			'param_name' => 'gold',
		],
		[
			'name_en'    => 'GENERAL PARTNERSHIP PACKAGE',
			'name_ru'    => 'ГЕНЕРАЛЬНЫЙ ПАРТНЕР',
			'cost_en'    => 6500,
			'cost_ru'    => 550000,
			'param_name' => 'general',
		],
		[
			'name_en'    => 'INFORMATION PARTNER PACKAGE',
			'name_ru'    => 'ИНФОРМАЦИОННЫЙ ПАРТНЕР',
			'cost_en'    => 1200,
			'cost_ru'    => 100000,
			'param_name' => 'info',
		],
	];
	
	/**
	 * IWPForm constructor.
	 */
	public function __construct() {
	
	}
	
	/**
	 * Create Array POST DATA Field Value
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	public function gatPostDataArray( array $data ): array {
		$metaBoxData = [];
		foreach ( $data as $key => $value ) {
			if( ! empty( $value ) && preg_match( '/iwp_*/', $key ) ) {
				$metaBoxData[ $key ] = sanitize_text_field( $value );
			}
		}
		
		return $metaBoxData;
	}
	
	/**
	 * Upload File from Form
	 *
	 * @param      $fileUpload
	 * @param null $size
	 *
	 * @return array|mixed|string
	 */
	public function uploadFile( $fileUpload, $size = null ) {
		
		$uploadedfile = $fileUpload;
		
		if( ! $size ) {
			$size = 2097152;
		}
		
		if( ! wp_check_filetype( $uploadedfile['name'] )['ext'] ) {
			return $file['error'] = "Mime Type Disallow";
		}
		
		if( $size < $uploadedfile['size'] ) {
			return $file['error'] = "Too large size, file no more than 2MB";
		}
		
		if( ! function_exists( 'wp_handle_upload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
		
		$file = wp_handle_upload( $uploadedfile, [ 'test_form' => false ] );
		if( $file && empty( $file['error'] ) ) {
			
			$attachment = [
				'guid'           => $file['url'],
				'post_mime_type' => $uploadedfile['type'],
				'post_title'     => $uploadedfile['name'],
				'post_content'   => '',
				'post_status'    => 'inherit',
			];
			
			$attach_id = wp_insert_attachment( $attachment, $file['file'], 0 );
			
			
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file['file'] );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			
			return [
				'ID'       => $attach_id,
				'url'      => $file['url'],
				'patch'    => $file['file'],
				'fileName' => $uploadedfile['name'],
			];
		}
		
		return $file['error'];
	}
	
	/**
	 * Delete file by attachment ID;
	 *
	 * @param $file_id
	 *
	 * @return bool[]|string[]
	 */
	public function removeFile( $file_id ): array {
		$delete = wp_delete_attachment( $file_id, true );
		if( false === $delete ) {
			return [ 'error' => "File has not been deleted" ];
		}
		
		return [ 'success' => true ];
	}
	
	/**
	 * Get type Packages
	 *
	 * @return array[]
	 */
	public function getPackages(): array {
		return $this->packages;
	}
	
	/**
	 * Get Package by param mane
	 *
	 * @param string $package_param_name
	 *
	 * @return array|mixed
	 */
	public function getPricePackage( string $package_param_name ) {
		foreach ( $this->packages as $package ) {
			if( in_array( $package_param_name, $package, true ) ) {
				return $package;
			}
		}
		
		return [];
	}
	
	/**
	 * Processes and clears the value of user information fields during registration
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	public function getUserDataByForm( array $data ): array {
		$resultData = [];
		foreach ( $data as $key => $item ) {
			if( ! empty( $item ) ) {
				$resultData[ $key ] = sanitize_text_field( $item );
			}
		}
		
		return $resultData;
	}
}