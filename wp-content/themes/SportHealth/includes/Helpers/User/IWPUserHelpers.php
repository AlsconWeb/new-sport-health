<?php
/**
 * Created 08.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\Helpers\User;

/**
 * Class IWPUserHelpers
 *
 * @package IWP\Helpers\User
 */
class IWPUserHelpers {
	protected $userID;
	protected $userInfoField = [
		'fio_en',
		'patronymic',
		'dob',
		'gender',
		'appeal',
		'country',
		'province',
		'city',
		'phone',
		'work',
		'organization',
		'degree',
		'position',
		'article_title',
		'coauthors',
		'congress_section',
		'form_participation',
		'form_performance',
		'technical',
		'technical_list',
		'first_name',
		'last_name',
	];

	/**
	 * IWPUserHelpers constructor.
	 */
	public function __construct( $userID ) {
		$this->userID = $userID;
	}

	/**
	 * Update User Meta
	 *
	 * @param array $metaData
	 *
	 * @return bool
	 */
	public function updateUserMetaInfo( array $metaData ): bool {
		if ( empty( $metaData ) ) {
			return false;
		}

		foreach ( $metaData as $key => $value ) {

			if ( 'fio_ru' === $key ) {
				$fullNameRu = explode( ' ', $value );
				update_user_meta( $this->userID, 'first_name', $fullNameRu[1] );
				update_user_meta( $this->userID, 'last_name', $fullNameRu[0] );
				update_user_meta( $this->userID, 'patronymic', $fullNameRu[2] );
			}

			update_user_meta( $this->userID, $key, $value );
		}

		return true;
	}

	/**
	 * Get User Information
	 *
	 * @return array
	 */
	public function getUserInfo(): array {
		$userInfo = [];

		foreach ( $this->userInfoField as $item ) {
			$meta = get_user_meta( $this->userID, $item, true );
			if ( $meta ) {
				$userInfo[ $item ] = $meta;
			}
		}

		return $userInfo;
	}
}