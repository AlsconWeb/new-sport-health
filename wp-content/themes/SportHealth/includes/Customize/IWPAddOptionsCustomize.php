<?php
/**
 * Created 22.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace IWP\Customize;

use WP_Customize_Image_Control;

/**
 * Class IWPAddOptionsCustomize
 *
 * @package IWP\Customize
 */
class IWPAddOptionsCustomize {
	/**
	 * IWPAddOptionsCustomize constructor.
	 */
	
	private $customize;
	
	public function __construct() {
		global $wp_customize;
		$this->customize = $wp_customize;
		$this->addSection();
		$this->addLogoSettings();
		$this->addFooterCallToAction();
		$this->addContactInfoFooter();
		$this->addSettingsFormSendDoc();
		$this->addSettingsRegFee();
	}
	
	public function addSection(): void {
		
		// Add Section
		$this->customize->add_section( 'general', [
			'title'    => __( 'General', 'iwp' ),
			'priority' => 70,
		] );
		
		$this->customize->add_section( 'contact_info', [
			'title'    => __( 'Contact Info', 'iwp' ),
			'priority' => 80,
		] );
		
		$this->customize->add_section( 'profile_form', [
			'title'    => __( 'Profile Form Send Doc', 'iwp' ),
			'priority' => 90,
		] );
		
		$this->customize->add_section( 'registration_fee_form', [
			'title'    => __( 'Registration fee settings', 'iwp' ),
			'priority' => 100,
		] );
	}
	
	/**
	 * Add Logo
	 */
	public function addLogoSettings(): void {
		
		$this->customize->add_setting( 'customizer_logo', [
			'transport' => 'refresh',
			'height'    => 325,
		] );
		$this->customize->add_setting( 'customizer_logo_white', [
			'transport' => 'refresh',
			'height'    => 325,
		] );
		
		$this->customize->add_control( new WP_Customize_Image_Control( $this->customize, 'customizer_logo', [
			'label'    => __( 'Logo', 'name-theme' ),
			'section'  => 'general',
			'settings' => 'customizer_logo',
		] ) );
		$this->customize->add_control( new WP_Customize_Image_Control( $this->customize, 'customizer_logo_white', [
			'label'    => __( 'Logo White', 'name-theme' ),
			'section'  => 'general',
			'settings' => 'customizer_logo_white',
		] ) );
	}
	
	/**
	 * Add Footer Call To Action
	 */
	public function addFooterCallToAction(): void {
		
		$this->customize->add_setting( 'customizer_text_cta', [
			'default'           => '',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => 'refresh',
		] );
		
		$this->customize->add_setting( 'customizer_text_cta_button', [
			'default'           => '',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => 'refresh',
		] );
		
		$this->customize->add_setting( 'customizer_text_cta_button_url', [
			'default'           => '',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_text_cta', [
			'section' => 'general',
			'label'   => __( 'Text Call to Action Footer', 'iwp' ),
			'type'    => 'textarea',
		] );
		$this->customize->add_control( 'customizer_text_cta_button', [
			'section' => 'general',
			'label'   => __( 'Text Button', 'iwp' ),
			'type'    => 'text',
		] );
		$this->customize->add_control( 'customizer_text_cta_button_url', [
			'section' => 'general',
			'label'   => __( 'Url Button', 'iwp' ),
			'type'    => 'url',
		] );
		
	}
	
	/**
	 * Add Contact Info by Footer
	 */
	public function addContactInfoFooter(): void {
		$this->customize->add_setting( 'customizer_contact_info_address', [
			'default'           => '',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => 'refresh',
		] );
		$this->customize->add_setting( 'customizer_contact_info_tel', [
			'default'           => '',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => 'refresh',
		] );
		$this->customize->add_setting( 'customizer_contact_info_tel_two', [
			'default'           => '',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => 'refresh',
		] );
		$this->customize->add_setting( 'customizer_contact_info_email', [
			'default'           => '',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => 'refresh',
		] );
		$this->customize->add_setting( 'customizer_contact_info_copyright', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_contact_info_address', [
			'section' => 'contact_info',
			'label'   => __( 'Address', 'iwp' ),
			'type'    => 'textarea',
		] );
		$this->customize->add_control( 'customizer_contact_info_tel', [
			'section' => 'contact_info',
			'label'   => __( 'Phone one', 'iwp' ),
			'type'    => 'text',
		] );
		$this->customize->add_control( 'customizer_contact_info_tel_two', [
			'section' => 'contact_info',
			'label'   => __( 'Phone two', 'iwp' ),
			'type'    => 'text',
		] );
		
		$this->customize->add_control( 'customizer_contact_info_email', [
			'section' => 'contact_info',
			'label'   => __( 'Email', 'iwp' ),
			'type'    => 'text',
		] );
		
		$this->customize->add_control( 'customizer_contact_info_copyright', [
			'section' => 'contact_info',
			'label'   => __( 'Copyright', 'iwp' ),
			'type'    => 'textarea',
		] );
	}
	
	/**
	 * Add From settings in profile page
	 */
	public function addSettingsFormSendDoc() {
		
		$this->customize->add_setting( 'customizer_profile_form_headline', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_profile_form_headline', [
			'section' => 'profile_form',
			'label'   => __( 'HeadLine text', 'iwp' ),
			'type'    => 'textarea',
		] );
		
		$this->customize->add_setting( 'customizer_profile_form_file_size', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_profile_form_file_size', [
			'section'     => 'profile_form',
			'label'       => __( 'File Size', 'iwp' ),
			'description' => 'size in MB',
			'type'        => 'number',
		] );
		
		$this->customize->add_setting( 'customizer_profile_form_email_to', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_profile_form_email_to', [
			'section'     => 'profile_form',
			'label'       => __( 'Email', 'iwp' ),
			'description' => 'Where the documents will be sent',
			'type'        => 'email',
		] );
		
		$this->customize->add_setting( 'customizer_profile_form_formats', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_profile_form_formats', [
			'section'     => 'profile_form',
			'label'       => __( 'Formats', 'iwp' ),
			'description' => 'Specify the formats available for download, separated by commas (example: .doc, .docx, .jpg)',
			'type'        => 'textarea',
		] );
	}
	
	public function addSettingsRegFee(): void {
		$this->customize->add_setting( 'customizer_reg_fee_ru_text_one', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_ru_text_one', [
			'section' => 'registration_fee_form',
			'label'   => __( 'RU Full time participant Text', 'iwp' ),
			'type'    => 'textarea',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_ru_price_one', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_ru_price_one', [
			'section' => 'registration_fee_form',
			'label'   => __( 'RU Full time participant price', 'iwp' ),
			'type'    => 'text',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_ru_text_two', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_ru_text_two', [
			'section' => 'registration_fee_form',
			'label'   => __( 'RU Article publication text', 'iwp' ),
			'type'    => 'textarea',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_ru_price_two', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_ru_price_two', [
			'section' => 'registration_fee_form',
			'label'   => __( 'RU Article publication price', 'iwp' ),
			'type'    => 'text',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_ru_text_three', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_ru_text_three', [
			'section' => 'registration_fee_form',
			'label'   => __( 'RU Publishing two articles text', 'iwp' ),
			'type'    => 'textarea',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_ru_price_three', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_ru_price_three', [
			'section' => 'registration_fee_form',
			'label'   => __( 'RU Publishing two articles price', 'iwp' ),
			'type'    => 'text',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_en_text_one', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_en_text_one', [
			'section' => 'registration_fee_form',
			'label'   => __( 'EN Full time participant Text', 'iwp' ),
			'type'    => 'textarea',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_en_price_one', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_en_price_one', [
			'section' => 'registration_fee_form',
			'label'   => __( 'EN Full time participant price', 'iwp' ),
			'type'    => 'text',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_en_text_two', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_en_text_two', [
			'section' => 'registration_fee_form',
			'label'   => __( 'EN Article publication text', 'iwp' ),
			'type'    => 'textarea',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_en_price_two', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_en_price_two', [
			'section' => 'registration_fee_form',
			'label'   => __( 'EN Article publication price', 'iwp' ),
			'type'    => 'text',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_en_text_three', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_en_text_three', [
			'section' => 'registration_fee_form',
			'label'   => __( 'EN Publishing two articles text', 'iwp' ),
			'type'    => 'textarea',
		] );
		
		$this->customize->add_setting( 'customizer_reg_fee_en_price_three', [
			'default'   => '',
			'transport' => 'refresh',
		] );
		
		$this->customize->add_control( 'customizer_reg_fee_en_price_three', [
			'section' => 'registration_fee_form',
			'label'   => __( 'EN Publishing two articles price', 'iwp' ),
			'type'    => 'text',
		] );
	}
}