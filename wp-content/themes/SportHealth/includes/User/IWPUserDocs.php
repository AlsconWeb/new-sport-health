<?php
/**
 * Created 09.06.2021
 * Version 1.0.1
 * Last update 30.06.21
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\User;

use IWP\Helpers\Form\IWPForm;

/**
 * Class IWPUserDocs
 *
 * @package IWP\User
 */
class IWPUserDocs {
	
	protected $db;
	protected $helpers;
	
	/**
	 * IWPUserDocs constructor.
	 */
	public function __construct() {
		
		global $wpdb;
		$this->db      = $wpdb;
		$this->helpers = new IWPForm();
		
		add_action( 'after_switch_theme', [ $this, 'addDocListTable' ] );
		add_action( 'after_switch_theme', [ $this, 'addDocCommentTable' ] );
		add_action( 'after_switch_theme', [ $this, 'addPaymentRequestTable' ] );
		
		add_action( 'admin_post_nopriv_send_doc_from', [ $this, 'handlerDocSend' ] );
		add_action( 'admin_post_send_doc_from', [ $this, 'handlerDocSend' ] );
		
		add_action( 'wp_ajax_remove_doc_file', [ $this, 'removeFile' ] );
		add_action( 'wp_ajax_nopriv_remove_doc_file', [ $this, 'removeFile' ] );
		
		add_action( 'wp_ajax_send_comment', [ $this, 'addCommentToFile' ] );
		add_action( 'wp_ajax_nopriv_send_comment', [ $this, 'addCommentToFile' ] );
		
		add_action( 'wp_ajax_send_requisites', [ $this, 'getRequisites' ] );
		add_action( 'wp_ajax_nopriv_send_requisites', [ $this, 'getRequisites' ] );
		
		add_action( 'wp_ajax_send_check', [ $this, 'sendCheck' ] );
		add_action( 'wp_ajax_nopriv_send_check', [ $this, 'sendCheck' ] );
	}
	
	/**
	 * Create Table Doc List
	 */
	public function addDocListTable(): void {
		
		$docListTable = $this->db->get_results( "SHOW TABLES LIKE '{$this->db->prefix}iwp_doc_list'" );
		
		if( ! $docListTable ) {
			$sql = "CREATE TABLE `{$this->db->prefix}iwp_doc_list` ( `id` BIGINT NOT NULL AUTO_INCREMENT, `userID` BIGINT NULL , `docID` BIGINT NULL , `status` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL , `date` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`));";
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
		
	}
	
	/**
	 * Create Table Doc Comment
	 */
	public function addDocCommentTable(): void {
		$docCommentTable = $this->db->get_results( "SHOW TABLES LIKE '{$this->db->prefix}iwp_doc_comment'" );
		
		if( ! $docCommentTable ) {
			$sql = "CREATE TABLE `{$this->db->prefix}iwp_doc_comment` ( `id` BIGINT NOT NULL AUTO_INCREMENT, `userID` BIGINT NULL , `docID` BIGINT NULL , `comment` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `parentComment` BIGINT NULL , `readStatus` BOOLEAN NOT NULL DEFAULT FALSE , `date` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`));";
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
	}
	
	/**
	 * Create Table iwp_payment
	 */
	public function addPaymentRequestTable(): void {
		$paymentRequestTable = $this->db->get_results( "SHOW TABLES LIKE '{$this->db->prefix}iwp_payment'" );
		
		if( ! $paymentRequestTable ) {
			$sql = "CREATE TABLE `{$this->db->prefix}iwp_payment` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `userID` BIGINT NULL , `payment_request` BOOLEAN NULL , `status` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL , `file` BIGINT NULL , PRIMARY KEY (`id`));";
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
	}
	
	/**
	 * Insert doc to iwp_doc_list table
	 *
	 * @param int $docID  ID Attachment
	 * @param int $userID ID User
	 *
	 * @return bool
	 */
	protected function addToDB( int $docID, int $userID ): bool {
		
		$result = $this->db->insert( "{$this->db->prefix}iwp_doc_list",
			[ 'userID' => $userID, 'docID' => $docID, 'status' => 'draft' ],
			[ '%d', '%d', '%s' ] );
		
		if( ! $result ) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Get User Docs
	 *
	 * @param int $userID User ID
	 *
	 * @return array|object|null
	 */
	public function getUserDocs( int $userID ) {
		$sql = "SELECT * FROM `{$this->db->prefix}iwp_doc_list` WHERE `userID`=" . $userID;
		
		return $this->db->get_results( $sql );
	}
	
	/**
	 * Removes a file from iwp_doc_list table
	 *
	 * @param int $docID attachment ID
	 *
	 * @return bool
	 */
	protected function deleteDocInBD( int $docID ): bool {
		$result = $this->db->delete( $this->db->prefix . 'iwp_doc_list', [ 'docID' => $docID ] );
		if( $result ) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Add a comment from iwp_doc_comment table
	 *
	 * @param int    $userID  User ID
	 * @param int    $docID   Attachment ID
	 * @param string $comment Comment
	 * @param int    $parent  ID Parent comment
	 *
	 * @return bool
	 */
	protected function addCommentInDB( int $userID, int $docID, string $comment, int $parent ): bool {
		
		$result = $this->db->insert( $this->db->prefix . 'iwp_doc_comment', [
			'userID'        => $userID,
			'docID'         => $docID,
			'comment'       => $comment,
			'parentComment' => $parent,
			'readStatus'    => 0,
		], [ '%d', '%d', '%s', '%d', '%d' ] );
		
		if( ! $result ) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * New comments counter
	 *
	 * @param int $userID User ID
	 * @param int $docID  Attachment ID
	 *
	 * @return integer
	 */
	public function getCountNewComment( int $userID, int $docID ): int {
		$sql = "SELECT COUNT(readStatus) FROM `{$this->db->prefix }iwp_doc_comment` WHERE `userID` = $userID AND `docID` = $docID AND `readStatus` = 0";
		
		$result = $this->db->get_results( $sql, ARRAY_A )[0]["COUNT(readStatus)"] ?? 0;
		
		return (int) $result;
	}
	
	
	/**
	 * Get Comments for Doc
	 *
	 * @param int $docID Attachment ID
	 *
	 * @return array|object|null
	 */
	public function getCommentsForDoc( int $docID ) {
		$sql    = "SELECT * FROM `{$this->db->prefix}iwp_doc_comment` WHERE `docID` =" . $docID . " AND `parentComment`= 0";
		$result = $this->db->get_results( $sql );
		
		if( ! empty( $result ) ) {
			return $result;
		}
		
		return [];
	}
	
	/**
	 * Get Comment Child
	 *
	 * @param int $commentID Comment ID
	 *
	 * @return array|object
	 */
	public function getChildComment( int $commentID ) {
		$sql    = "SELECT * FROM `{$this->db->prefix}iwp_doc_comment` WHERE `parentComment` =" . $commentID;
		$result = $this->db->get_results( $sql );
		
		if( ! empty( $result ) ) {
			return $result;
		}
		
		return [];
	}
	
	/**
	 * Add payment request
	 *
	 * @param int  $userID         User ID
	 * @param bool $paymentRequest Successfully sent details to email
	 * @param bool $status         Did the admin check the payment receipt or not
	 * @param int  $attachmentID   Attachment ID payment receipts
	 *
	 * @return bool
	 */
	public function addPaymentRequest( int $userID, bool $paymentRequest, string $status, int $attachmentID = null ):
	bool {
		$result = $this->db->insert( $this->db->prefix . 'iwp_payment', [
			'userID'          => $userID,
			'payment_request' => $paymentRequest,
			'status'          => $status,
			'file'            => $attachmentID,
		], [ '%d', '%d', '%s', '%d' ] );
		
		if( ! $result ) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Add Attachment ID Check
	 *
	 * @param int $userID User ID
	 * @param int $docID  Attachment ID
	 *
	 * @return bool
	 */
	protected function addCheckFromUser( int $userID, int $docID ): bool {
		$result = $this->db->update( $this->db->prefix . 'iwp_payment', [
			'file' => $docID,
		], [ 'userID' => $userID, ] );
		
		if( ! $result ) {
			return false;
		}
		
		return true;
	}
	
	
	/**
	 *  Handler Doc Send
	 */
	public function handlerDocSend(): void {
		if( empty( $_POST ) || ! wp_verify_nonce( $_POST['iwp_doc_from_nonce'], 'iwp_doc_from' ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="nonce_error"', 301 );
			exit;
		}
		
		$file   = &$_FILES['iwp_file'];
		$userID = (int) $_POST['iwp_user_id'];
		$mailTo = $_POST['iwp_to_email'] ?? get_option( 'admin_email' );
		$size   = $_POST['iwp_size'];
		
		if( ! empty( $file ) ) {
			$fileData   = $this->helpers->uploadFile( $file, $size );
			$attachment = $fileData['patch'];
		}
		
		if( ! empty( $file ) && isset( $fileData['error'] ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Invalid File format or Size"', 301 );
			exit;
		}
		
		$setFileData = $this->addToDB( $fileData['ID'], $userID );
		
		$email = $this->sendEmail( 'doc', $userID, $mailTo, $attachment, $fileData['url'], true );
		
		if( $setFileData && $email ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?success="Send file to moderation"', 301 );
			exit;
		}
		
	}
	
	
	/**
	 * Send Email
	 *
	 * @param string            $type        Type email
	 * @param int               $userID      User ID
	 * @param string|null       $emailTo     Where the letter will be sent default admin email
	 * @param file              $attachment  Attachment file
	 * @param string            $link        Url doc
	 * @param bool              $sendUser    Send User email default false
	 * @param string|null|array $messageText Text comment
	 *
	 * @return bool
	 *
	 * @TODO переделать на массви параметров
	 */
	protected function sendEmail(
		string $type, int $userID, string $emailTo = null, $attachment = null, $link = null, bool $sendUser =
	false, $messageText = null
	): bool {
		switch ( $type ) {
			case 'doc':
				$header = [ 'Form: <info@sport-health.ru>', 'content-type: text/html', ];
				if( empty( $emailTo ) ) {
					$emailTo = get_option( 'admin_email' );
				}
				if( $sendUser ) {
					ob_start();
					include_once get_template_directory() . '/includes/emailTemplate/docSend.php';
					$message   = ob_get_clean();
					$userEmail = get_user_by( 'id', $userID )->user_email;
					$mailUser  = wp_mail( $userEmail, 'Статья на рассмотрении', $message,
						$header );
				}
				
				ob_start();
				include_once get_template_directory() . '/includes/emailTemplate/docToAdmin.php';
				$message   = ob_get_clean();
				$mailAdmin = wp_mail( $emailTo, 'Статья на рассмотрение с сайта ' . get_bloginfo( 'url' ), $message,
					$header,
					$attachment );
				
				break;
			case 'docSendComment':
				$header = [ 'Form: <info@sport-health.ru>', 'content-type: text/html', ];
				if( empty( $emailTo ) ) {
					$emailTo = get_option( 'admin_email' );
				}
				if( $sendUser ) {
					ob_start();
					include_once get_template_directory() . '/includes/emailTemplate/docSendComment.php';
					$message   = ob_get_clean();
					$userEmail = get_user_by( 'id', $userID )->user_email;
					$mailUser  = wp_mail( $userEmail, 'Был добавлен новый коментарий ', $message,
						$header );
				}
				ob_start();
				include_once get_template_directory() . '/includes/emailTemplate/docSendComment.php';
				$message   = ob_get_clean();
				$mailAdmin = wp_mail( $emailTo, 'Был добавлен новый коментарий ' . get_bloginfo( 'url' ), $message,
					$header,
					$attachment );
				break;
			case 'paymentRequest':
				$header = [ 'Form: <info@sport-health.ru>', 'content-type: text/html', ];
				if( empty( $emailTo ) ) {
					$emailTo = get_option( 'admin_email' );
				}
				if( $sendUser ) {
					ob_start();
					include_once get_template_directory() . '/includes/emailTemplate/paymentRequest.php';
					$message   = ob_get_clean();
					$userEmail = get_user_by( 'id', $userID )->user_email;
					$mailUser  = wp_mail( $userEmail, 'Реквизиты для оплаты ', $message,
						$header );
				}
				ob_start();
				include_once get_template_directory() . '/includes/emailTemplate/paymentToAdmin.php';
				$userEmail = get_user_by( 'id', $userID )->user_email;
				$message   = ob_get_clean();
				$mailAdmin = wp_mail( $emailTo, 'Реквизиты для оплаты были отправлены  ' . $userEmail, $message,
					$header,
					$attachment );
				break;
			case 'sendCheck':
				$header = [ 'Form: <info@sport-health.ru>', 'content-type: text/html', ];
				if( empty( $emailTo ) ) {
					$emailTo = get_option( 'admin_email' );
				}
				ob_start();
				include_once get_template_directory() . '/includes/emailTemplate/checkToAdmin.php';
				$userEmail = get_user_by( 'id', $userID )->user_email;
				$message   = ob_get_clean();
				$mailAdmin = wp_mail( $emailTo, 'Подтверждение платежа  ' . $userEmail, $message,
					$header,
					$attachment );
				break;
		}
		
		if( $mailAdmin ) {
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Get Doc Status from user
	 *
	 * @param string $status Status File in DB
	 *
	 * @return string
	 */
	public function getDocStatus( string $status ): string {
		switch ( $status ) {
			case 'draft':
				$statusText = __( 'Moderation', 'iwp' );
				break;
			case 'publish':
				$statusText = __( 'Accepted', 'iwp' );
				break;
			case 'revision':
				$statusText = __( 'Requires revision', 'iwp' );
				break;
			case 'rejected':
				$statusText = __( 'Rejected', 'iwp' );
				break;
			default:
				$statusText = __( 'Moderation', 'iwp' );
		}
		
		return $statusText;
	}
	
	/**
	 * Remove Doc File
	 */
	public function removeFile(): void {
		$fileID = $_POST['docID'];
		
		if( empty( $fileID ) ) {
			wp_send_json_error( [ 'message' => 'Doc ID is empty' ] );
		}
		
		$result = $this->helpers->removeFile( $fileID );
		if( ! empty( $result['error'] ) ) {
			wp_send_json_error( [ 'message' => $result['error'] ] );
		}
		
		if( $this->deleteDocInBD( $fileID ) ) {
			wp_send_json_success( [ 'message' => 'file been deleted' ] );
		}
	}
	
	/**
	 * Add comment to doc file
	 */
	public function addCommentToFile(): void {
		$userID  = (int) $_POST['userID'];
		$docID   = (int) $_POST['docID'];
		$comment = sanitize_text_field( $_POST['comment'] );
		$parent  = (int) $_POST['parent'];
		
		if( trim( $comment ) === '' ) {
			wp_send_json_error( [ 'message' => __( 'Fill in the comment field for the document', 'iwp' ) ] );
		}
		
		if( empty( $userID ) ) {
			wp_send_json_error( [ 'message' => __( 'User ID is empty', 'iwp' ) ] );
		}
		
		if( empty( $docID ) ) {
			wp_send_json_error( [ 'message' => __( 'Doc ID is empty', 'iwp' ) ] );
		}
		
		$result = $this->addCommentInDB( $userID, $docID, $comment, $parent );
		
		if( ! $result ) {
			wp_send_json_error( [ 'message' => __( 'An error occurred comment was not added', 'iwp' ) ] );
		}
		$email = $this->sendEmail( 'docSendComment', $userID, '', $docID, null, false, $comment );
		wp_send_json_success( [ 'message' => __( 'Comment added successfully', 'iwp' ), 'email' => $email ] );
	}
	
	/**
	 * Send Request Payment
	 */
	public function getRequisites(): void {
		$userID  = (int) $_POST['userID'];
		$options = $_POST['options'];
		
		if( empty( $userID ) ) {
			wp_send_json_error( [ 'message' => __( 'User ID is empty', 'iwp' ) ] );
		}
		
		$total = $this->getTotalPrice( $options );
		
		$email = $this->sendEmail( 'paymentRequest', $userID, '', null, null, true, $total );
		
		if( ! $email ) {
			wp_send_json_error( [ 'message' => __( 'Requisites were not sent', 'iwp' ) ] );
		}
		
		$result = $this->addPaymentRequest( $userID, $email, 'pending' );
		
		if( ! $result ) {
			wp_send_json_error( [ 'message' => __( 'Error please try again', 'iwp' ) ] );
		}
		
		wp_send_json_success( [ 'message' => __( 'Requisites have been sent', 'iwp' ), 'email' => $email ] );
	}
	
	/**
	 * Total Price and Currency
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	protected function getTotalPrice( array $data ): array {
		$total = 0;
		foreach ( $data as $val ) {
			$total += (int) $val['cost'];
		}
		
		$currency = 'ru' === explode( '-', $data[0]['name'] )[2] ? 'rub' : 'eur';
		
		return [ 'total' => $total, 'currency' => $currency ];
	}
	
	/**
	 * Send Check
	 */
	public function sendCheck(): void {
		$file   = &$_FILES['file'];
		$userID = (int) $_POST['userID'];
		
		if( empty( $userID ) ) {
			wp_send_json_error( [ 'message' => __( 'User ID empty', 'iwp' ) ] );
		}
		
		if( ! empty( $file ) ) {
			$fileData   = $this->helpers->uploadFile( $file );
			$attachment = $fileData['patch'];
		}
		
		if( ! empty( $file ) && isset( $fileData['error'] ) ) {
			wp_send_json_error( [ 'message' => __( 'Invalid File format or Size', 'iwp' ) ] );
		}
		
		$result = $this->addCheckFromUser( $userID, $fileData['ID'] );
		
		if( ! $result ) {
			wp_send_json_error( [ 'message' => __( 'Error please try again', 'iwp' ) ] );
		}
		
		$email = $this->sendEmail( 'sendCheck', $userID, '', $attachment, $fileData['url'], false );
		
		if( ! $email ) {
			wp_send_json_error( [ 'message' => __( 'File not send', 'iwp' ) ] );
		}
		
		wp_send_json_success( [ 'message' => __( 'The file was sent successfully', 'iwp' ), 'email' => $email ] );
	}
}