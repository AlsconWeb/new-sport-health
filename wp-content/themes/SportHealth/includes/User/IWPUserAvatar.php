<?php
/**
 * Created 07.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\User;

use IWP\Helpers\Form\IWPForm;

/**
 * Class IWPUserAvatar
 *
 * @package IWP\User
 */
class IWPUserAvatar {
	
	protected $helpers;
	
	/**
	 * IWPUserAvatar constructor.
	 */
	public function __construct() {
		add_action( 'wp_ajax_upload_avatar', [ $this, 'uploadAvatar' ] );
		add_action( 'wp_ajax_nopriv_upload_avatar', [ $this, 'uploadAvatar' ] );
		
		add_action( 'wp_ajax_remove_avatar', [ $this, 'removeAvatar' ] );
		add_action( 'wp_ajax_nopriv_remove_avatar', [ $this, 'removeAvatar' ] );
		
		$this->helpers = new IWPForm();
	}
	
	/**
	 * Ajax Handler Upload Avatar
	 */
	public function uploadAvatar(): void {
		$file   = &$_FILES['file'];
		$userID = (int) $_POST['userID'];
		
		if( empty( $userID ) ) {
			wp_send_json_error( [ 'message' => 'User ID is Empty' ] );
		}
		
		$uploadResult = $this->helpers->uploadFile( $file );
		
		if( ! empty( $uploadResult['error'] ) ) {
			wp_send_json_error( [ 'message' => $uploadResult['error'] ] );
		}
		
		$avatarURL = wp_get_attachment_image_url( $uploadResult['ID'], 'avatar' );
		
		update_user_meta( $userID, 'iwp_user_avatar', [ 'url' => $avatarURL, 'ID' => $uploadResult['ID'] ] );
		wp_send_json_success( [
			'urlAvatar' => $avatarURL,
			'fileName'  => $uploadResult['fileName'],
		] );
	}
	
	/**
	 * Remove Avatar User
	 */
	public function removeAvatar(): void {
		$userID       = $_POST['userID'];
		$attachmentID = $_POST['attachmentID'];
		if( empty( $userID ) ) {
			wp_send_json_error( [ 'message' => 'User ID is Empty' ] );
		}
		
		if( empty( $attachmentID ) ) {
			wp_send_json_error( [ 'message' => 'Attachment ID is Empty' ] );
		}
		
		$result = $this->helpers->removeFile( $attachmentID );
		
		if( ! empty( $result['error'] ) ) {
			wp_send_json_error( [ 'message' => $result['error'] ] );
		}
		
		delete_user_meta( $userID, 'iwp_user_avatar' );
		wp_send_json_success();
	}
}