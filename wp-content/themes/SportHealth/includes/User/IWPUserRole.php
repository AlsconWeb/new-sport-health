<?php
/**
 * Created 06.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\User;

/**
 * Class IWPUserRole
 *
 * @package IWP\User
 */
class IWPUserRole {
	protected $capabilities = [ 'read' => true ];
	protected $regCapabilities;
	
	/**
	 * IWPUserRole constructor.
	 */
	public function __construct() {
		add_action( 'switch_theme', [ $this, 'deactivateTheme' ] );
		add_action( 'after_switch_theme', [ $this, 'activateTheme' ] );
		
		$this->regCapabilities = get_option( 'users_can_register' );
		
		
		add_action( 'after_switch_theme', [ $this, 'createRegistrationPage' ] );
		add_action( 'after_switch_theme', [ $this, 'createLoginPage' ] );
		add_action( 'after_switch_theme', [ $this, 'createLostPasswordPage' ] );
		add_action( 'after_switch_theme', [ $this, 'createAccountPage' ] );
	}
	
	/**
	 * Remove Role after switch themes
	 */
	public function deactivateTheme(): void {
		remove_role( 'conference_participant' );
		update_option( 'default_role', 'subscriber' );
	}
	
	/**
	 * Register Role Conference Participant after Activation themes
	 */
	public function activateTheme(): void {
		add_role( 'conference_participant', __( 'Conference Participant' ), $this->capabilities );
		update_option( 'default_role', 'conference_participant' );
	}
	
	/**
	 * Create Registration Page
	 */
	public function createRegistrationPage(): void {
		
		if( $this->regCapabilities && ! get_page_by_title( 'Регистрация' ) ) {
			
			$page_id = wp_insert_post( wp_slash( [
				'post_status' => 'publish',
				'post_type'   => 'page',
				'post_title'  => 'Регистрация',
				'post_author' => 1,
				'post_parent' => 0,
				'menu_order'  => 0,
				'meta_input'  => [ '_wp_page_template' => 'register.php' ],
			] ) );
			
			update_option( 'iwp_register_page', $page_id, 'on' );
		}
	}
	
	/**
	 * Create Login Page
	 */
	public function createLoginPage(): void {
		if( $this->regCapabilities && ! get_page_by_title( 'Войти' ) ) {
			
			$page_id = wp_insert_post( wp_slash( [
				'post_status' => 'publish',
				'post_type'   => 'page',
				'post_title'  => 'Войти',
				'post_author' => 1,
				'post_parent' => 0,
				'menu_order'  => 0,
				'meta_input'  => [ '_wp_page_template' => 'login.php' ],
			] ) );
			update_option( 'iwp_login_page', $page_id, 'on' );
		}
	}
	
	/**
	 * Create Forget Password Page
	 */
	public function createLostPasswordPage(): void {
		if( $this->regCapabilities && ! get_page_by_title( 'Востоновить пароль' ) ) {
			
			$page_id = wp_insert_post( wp_slash( [
				'post_status' => 'publish',
				'post_type'   => 'page',
				'post_title'  => 'Востоновить пароль',
				'post_author' => 1,
				'post_parent' => 0,
				'menu_order'  => 0,
				'meta_input'  => [ '_wp_page_template' => 'lostPassword.php' ],
			] ) );
			update_option( 'iwp_forget_password_page', $page_id, 'on' );
		}
	}
	
	/**
	 * Create Account Page
	 */
	public function createAccountPage(): void {
		if( $this->regCapabilities && ! get_page_by_title( 'Личный кабинет' ) ) {
			
			$page_id = wp_insert_post( wp_slash( [
				'post_status' => 'publish',
				'post_type'   => 'page',
				'post_title'  => 'Личный кабинет',
				'post_author' => 1,
				'post_parent' => 0,
				'menu_order'  => 0,
				'meta_input'  => [ '_wp_page_template' => 'account.php' ],
			] ) );
			update_option( 'iwp_account_page', $page_id, 'on' );
		}
	}
}