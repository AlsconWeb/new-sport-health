<?php
/**
 * Created 06.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\User;

use IWP\Helpers\Form\IWPForm;
use IWP\Helpers\User\IWPUserHelpers;

/**
 * Class IWPUserRegistrations
 *
 * @package IWP\User
 */
class IWPUserRegistrations {
	protected $helpers;
	
	/**
	 * IWPUserRegistrations constructor.
	 */
	public function __construct() {
		$this->helpers = new IWPForm();
		
		add_action( 'admin_post_nopriv_iwp_register_form', [ $this, 'registrationFrom' ] );
		add_action( 'admin_post_iwp_register_form', [ $this, 'registrationFrom' ] );
	}
	
	/**
	 * Registration Handler
	 */
	public function registrationFrom(): void {
		if( empty( $_POST ) || ! wp_verify_nonce( $_POST['iwp_register_form_nonce'], 'iwp_register_form' ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="nonce_error"', 301 );
			exit;
		}
		
		$request   = $_POST;
		$dataArray = $this->helpers->gatPostDataArray( $request );
		if( empty( $dataArray['iwp_email'] ) || ! is_email( $dataArray['iwp_email'] ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Invalid Email"', 301 );
			exit;
		}
		
		
		if( $dataArray['iwp_password'] !== $dataArray['iwp_confirm']
		    || empty( $dataArray['iwp_password'] )
		    || strlen( $dataArray['iwp_password'] ) < 5 ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Password does not match or is less than 5 characters long"',
				301 );
			exit;
		}
		
		$userdata = [
			'user_login' => $dataArray['iwp_email'],
			'user_pass'  => $dataArray['iwp_password'],
			'user_email' => $dataArray['iwp_email'],
			'first_name' => $dataArray['iwp_first_name'],
			'last_name'  => $dataArray['iwp_last_name'],
		];
		
		$user_id = wp_insert_user( $userdata );
		
		if( ! is_wp_error( $user_id ) ) {
			$userInfo = $this->helpers->getUserDataByForm( $request['iwp_userinfo'] );
			$userMeta = new IWPUserHelpers( $user_id );
			$userMeta->updateUserMetaInfo( $userInfo );
			
			wp_set_current_user( $user_id );
			wp_set_auth_cookie( $user_id );
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?success="Registration completed successfully"', 301 );
			exit;
		}
		
		wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="' . $user_id->get_error_message() . '"',
			301 );
		exit;
		
	}
}