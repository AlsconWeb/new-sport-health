<?php
/**
 * Created 08.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

namespace IWP\User;

use IWP\Helpers\Form\IWPForm;
use IWP\Helpers\User\IWPUserHelpers;

/**
 * Class IWPUserProfileFrom
 *
 * @package IWP\User
 */
class IWPUserProfileFrom {
	
	protected $helpers;
	
	/**
	 * IWPUserProfileFrom constructor.
	 */
	public function __construct() {
		$this->helpers = new IWPForm();
		
		add_action( 'admin_post_nopriv_edit_profile_from', [ $this, 'editProfileFrom' ] );
		add_action( 'admin_post_edit_profile_from', [ $this, 'editProfileFrom' ] );
		
		add_action( 'admin_post_nopriv_edit_contact_from', [ $this, 'editContactFrom' ] );
		add_action( 'admin_post_edit_contact_from', [ $this, 'editContactFrom' ] );
		
		add_action( 'admin_post_nopriv_edit_performance_from', [ $this, 'editPerformanceFrom' ] );
		add_action( 'admin_post_edit_performance_from', [ $this, 'editPerformanceFrom' ] );
	}
	
	/**
	 * Edit Profile data
	 */
	public function editProfileFrom(): void {
		
		if( empty( $_POST ) || ! wp_verify_nonce( $_POST['iwp_profile_from_nonce'], 'iwp_profile_from' ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="nonce_error"', 301 );
			exit;
		}
		
		$request = $_POST['iwp_userinfo'];
		$userID  = $_POST['iwp_user_id'];
		
		
		$userInfo = $this->helpers->getUserDataByForm( $request );
		$userMeta = new IWPUserHelpers( $userID );
		$result   = $userMeta->updateUserMetaInfo( $userInfo );
		
		if( $result ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?success="Date is Save"', 301 );
			exit;
		}
		
		wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Date not Save"',
			301 );
		exit;
	}
	
	/**
	 * Edit Contact Info User
	 */
	public function editContactFrom(): void {
		if( empty( $_POST ) || ! wp_verify_nonce( $_POST['iwp_contact_from_nonce'], 'iwp_contact_from' ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="nonce_error"', 301 );
			exit;
		}
		
		$request = $_POST['iwp_userinfo'];
		$userID  = $_POST['iwp_user_id'];
		
		if( empty( $request['email'] ) || get_user_by( 'email', $request['email'] ) || ! is_email( $request['email'] ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Email empty or not valid"', 301 );
		}
		
		$userInfo = $this->helpers->getUserDataByForm( $request );
		$userMeta = new IWPUserHelpers( $userID );
		$result   = $userMeta->updateUserMetaInfo( $userInfo );
		
		$updateUser = wp_update_user( [
			'ID'         => $userID,
			'user_email' => $request['email'],
			'user_login' => $request['email'],
		] );
		
		if( is_wp_error( $updateUser ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="' . $updateUser->get_error_message() . '"', 301 );
			exit;
		}
		
		wp_redirect( $_REQUEST['_wp_http_referer'] . '?success="Date is Save"', 301 );
		exit;
	}
	
	/**
	 * Edit Performance From
	 */
	public function editPerformanceFrom(): void {
		if( empty( $_POST ) || ! wp_verify_nonce( $_POST['iwp_performance_from_nonce'], 'iwp_performance_from' ) ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="nonce_error"', 301 );
			exit;
		}
		
		$request  = $_POST['iwp_userinfo'];
		$userID   = $_POST['iwp_user_id'];
		$userInfo = $this->helpers->getUserDataByForm( $request );
		$userMeta = new IWPUserHelpers( $userID );
		$result   = $userMeta->updateUserMetaInfo( $userInfo );
		
		if( $result ) {
			wp_redirect( $_REQUEST['_wp_http_referer'] . '?success="Date is Save"', 301 );
			exit;
		}
		
		wp_redirect( $_REQUEST['_wp_http_referer'] . '?error="Date not Save"',
			301 );
		exit;
	}
	
}