<?php
/**
 * Created 22.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

get_header(); ?>
	<section>
		<div class="archive">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2><?php _e( 'Archive', 'iwp' ); ?></h2>
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
				<?php if( have_posts() ): ?>
					<?php while ( have_posts() ):the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php get_footer() ?>