<?php
/**
 * Created 07.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * Template Name: Account Page
 */

use IWP\Helpers\IWPBakeryHelpers;
use IWP\Helpers\User\IWPUserHelpers;

if( ! is_user_logged_in() ) {
	wp_redirect( get_permalink( get_option( 'iwp_login_page', true ) ), 301 );
}

get_header();

$userID      = wp_get_current_user()->ID;
$userAvatar  = get_user_meta( $userID, 'iwp_user_avatar', true );
$userHelpers = new IWPUserHelpers( $userID );
$userEmail   = get_userdata( $userID )->user_email;

$userInfo = $userHelpers->getUserInfo();
$helpers  = new IWPBakeryHelpers();
?>
<section>
	<div class="personal-cabinet">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 class="title"><?php the_title(); ?></h1>
					<div class="dfr">
						<div class="user-photo">
							<?php if( ! empty( $userAvatar['url'] ) ): ?>
								<div class="img">
									<a href="#" id="remove-avatar"
									   data-userid="<?php echo $userID; ?>"
									   data-avatarid="<?php echo $userAvatar['ID']; ?>">X</a>
									<img src="<?php echo $userAvatar['url'] ?>" alt="User Avatar">
								</div>
							<?php else: ?>
								<input id="add-photo" type="file" name="iwp_avatar" accept=".jpg, .jpeg, .png"
								       data-userid="<?php echo $userID; ?>">
								<label class="icon-photo"
								       for="add-photo">
									<span><?php _e( 'Upload a photo', 'iwp' ); ?></span>
									<?php _e( 'No file selected', 'iwp' ); ?>
								</label>
							<?php endif; ?>
						</div>
						<div class="user-desc">
							<h3><?php echo $userInfo['first_name'] . ' ' . $userInfo['last_name'] ?></h3>
							<a class="icon-lock" href="#"><?php _e( 'Change Password', 'iwp' ); ?></a>
							<a class="icon-logout"
							   href="<?php echo wp_logout_url() ?>&redirect_to='/'"><?php _e( 'Logout', 'iwp' ); ?></a>
						</div>
					</div>
					<ul class=" nav-tabs">
						<li class="active"><a href="#profil" data-toggle="tab"><?php _e( 'profile', 'iwp' ); ?></a></li>
						<li><a href="#list-doc" data-toggle="tab"><?php _e( 'list of documents', 'iwp' ); ?></a></li>
						<li><a href="#registration" data-toggle="tab"><?php _e( 'registration fee', 'iwp' ); ?></a></li>
					</ul>
					<div class="tab-content">
						<?php include_once __DIR__ . '/template_part/account/profile-tab.php'; ?>
						<?php include_once __DIR__ . '/template_part/account/list-doc.php'; ?>
						<?php include_once __DIR__ . '/template_part/account/registration.php'; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
