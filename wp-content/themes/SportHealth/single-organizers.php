<?php
/**
 * Created 15.06.2021
 * Version 1.0.1
 * Last update 01.07.21
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

get_header();
?>
<section>
	<div class="inner">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="title"><?php _e( 'Greeting speech', 'iwp' ); ?></h2>
					<div class="dfr">
						<?php the_post_thumbnail( 'full' ); ?>
						<div class="desc">
							<h1><?php the_title(); ?></h1>
							<p><?php the_excerpt(); ?></p>
						</div>
					</div>
					<div class="content">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
