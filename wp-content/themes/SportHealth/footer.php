<?php
/**
 * Created 20.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$tel_one = get_theme_mod( 'customizer_contact_info_tel' );
$tel_two = get_theme_mod( 'customizer_contact_info_tel_two' );
$email   = get_theme_mod( 'customizer_contact_info_email' );

$tel_one_link = preg_replace( "([\/, , (, ), -])", '', $tel_one );
$tel_two_link = preg_replace( "([\/, , (, ), -])", '', $tel_two );

?>
<?php if( ! is_page( [ 110, 589 ] ) ): ?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="title"><?php echo get_theme_mod( 'customizer_text_cta' ); ?></h2>
					<a class="btn icon-arrow-right"
					   href="<?php echo get_theme_mod( 'customizer_text_cta_button_url' ); ?>">
						<?php echo get_theme_mod( 'customizer_text_cta_button' ); ?>
					</a>
				</div>
				<div class="col-2">
					<a class="logo" href="<?php bloginfo( 'url' ); ?>">
						<img src="<?php echo get_theme_mod( 'customizer_logo_white' ); ?>" alt="Logo">
					</a>
				</div>
				<div class="col-10">
					<?php wp_nav_menu( [
						'theme_location' => 'footer_menu',
						'container'      => '',
						'menu_class'     => 'menu dfr justify-content-between',
						'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					] ); ?>
					<ul class="contacts">
						<li class="icon-marker">
							<?php echo get_theme_mod( 'customizer_contact_info_address' ); ?>
						</li>
						<li class="icon-phone">
							<a href="tel:<?php echo $tel_one_link ?>"><?php echo $tel_one; ?></a>
							<a href="tel:<?php echo $tel_two_link ?>"><?php echo $tel_two; ?></a>
						</li>
						<li class="icon-email">
							<a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
						</li>
					</ul>
					<p class="copyright">Copyright © <?php echo current_time( 'Y' );
						echo get_theme_mod( 'customizer_contact_info_copyright' ); ?> </p>
				</div>
			</div>
		</div>
	</footer>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>
