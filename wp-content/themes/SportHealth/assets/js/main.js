jQuery(document).ready(function ($) {
	$('ul[style*="color"] li').each(function(){
		var textEl = $(this).text();
		$(this).wrapInner('<p></p>')
	});
	/**
	 * Header language switcher
	 */
	if ($('#language').length) {
		$('#language').change(function (e) {
			if ($(this).prop('checked')) {
				$('.language').data('active_lang', 'en');
				location.href = $('.language').data('url_en');
			} else {
				$('.language').data('active_lang', 'ru');
				location.href = $('.language').data('url_ru');
			}
		});
	}
	
	/**
	 * Register From
	 */
	if ($('form.register').length || $('#edit_profile').length || $('#register_form').length) {
		let paramsUrl = location.search;
		let params = new URLSearchParams(paramsUrl);
		
		let success = params.get('success');
		let error = params.get('error');
		console.log(error, params)
		if (success) {
			Swal.fire({
				icon: 'success',
				title: 'Success',
				text: success,
			}).then((result) => {
				if (result.isConfirmed) {
					location.href = $('#iwp_to_redirect').val();
				}
			})
		}
		
		if (error) {
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: error,
			}).then((result) => {
				if (result.isConfirmed) {
					location.href = $('#iwp_to_redirect').val();
				}
			})
		}
		
		$('#iwp_form_performance').parent().hide();
		$('#iwp_technical').parent().hide();
		$('#iwp_technical_list').parent().hide();
		
		$('#iwp_form_participation').change(function (e) {
			if ($(this).val() !== 'full_time') {
				$('#iwp_form_performance').parent().hide();
				$('#iwp_technical').parent().hide();
				$('#iwp_technical_list').parent().hide();
			} else {
				$('#iwp_form_performance').parent().show();
				$('#iwp_technical').parent().show();
				$('#iwp_technical_list').parent().show();
			}
		});
		
	}
	
	/**
	 * Ajax Upload Avatar
	 */
	if ($('#add-photo').length) {
		$('#add-photo').change(function (e) {
			let formData = new FormData();
			formData.append('file', $(this)[ 0 ].files[ 0 ]);
			formData.append('action', 'upload_avatar');
			formData.append('userID', $(this).data('userid'));
			
			$.ajax({
				type: 'POST',
				url: iwp.ajaxURL,
				data: formData,
				contentType: false,
				async: true,
				cache: false,
				processData: false,
				timeout: 60000,
				success: function (data) {
					if (data.success) {
						$('#add-photo').hide();
						$('label.icon-photo').hide();
						$('.user-photo').append(`<img src='${data.data.urlAvatar}'>`);
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: data.data.message,
						});
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr)
				},
			});
		});
	}
	
	/**
	 * Remove Avatar
	 */
	if ($('#remove-avatar').length) {
		$('#remove-avatar').click(function (e) {
			e.preventDefault();
			
			let data = {
				action: 'remove_avatar',
				userID: $(this).data('userid'),
				attachmentID: $(this).data('avatarid')
			}
			
			$.ajax({
				type: 'POST',
				url: iwp.ajaxURL,
				data: data,
				success: function (data) {
					if (data.success) {
						location.reload();
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: data.data.message,
						});
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log('error...', xhr);
					//error logging
				},
			});
		});
	}
	
	
	/**
	 * Account Page
	 */
	if ($('#edit_profile').length) {
		//profile section
		$('#edit_profile').click(function (e) {
			e.preventDefault();
			$('#edit_profile_from').show();
			$('.profile-block').hide();
			
		});
		
		$('#cancel_profile_from').click(function (e) {
			e.preventDefault();
			$('#edit_profile_from').hide();
			$('.profile-block').show();
		});
	}
	//contact section
	if ($('#edit_contact').length) {
		$('#edit_contact').click(function (e) {
			e.preventDefault();
			$('#edit_contact_from').show();
			$('.contact').hide();
		});
		
		$('#cancel_contact_from').click(function (e) {
			e.preventDefault();
			$('#edit_contact_from').hide();
			$('.contact').show();
		});
	}
	//performance section
	if ($('#edit_performance').length) {
		$('#edit_performance').click(function (e) {
			e.preventDefault();
			$('#edit_performance_from').show();
			$('.performance').hide();
		});
		
		$('#cancel_performance_from').click(function (e) {
			e.preventDefault();
			$('#edit_performance_from').hide();
			$('.performance').show();
		});
	}
	//output file name
	if ($('#add-file, #add-file_check').length) {
		$('#add-file, #add-file_check').change(function (e) {
			$(this).parent().find('p').text($(this)[ 0 ].files[ 0 ].name)
		});
	}
	//delete file
	if ($('.icon-delete').length) {
		$('.icon-delete').click(function (e) {
			e.preventDefault();
			
			let data = {
				action: 'remove_doc_file',
				docID: $(this).data('docid')
				
			}
			let elDelete = $(this).parent().parent().parent().parent();
			
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
			
			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete file',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.isConfirmed) {
					$.ajax({
						type: 'POST',
						url: iwp.ajaxURL,
						data: data,
						success: function (data) {
							elDelete.remove();
							swalWithBootstrapButtons.fire(
								'Deleted!',
								'Your file has been deleted.',
								'success'
							)
							
						},
						error: function (xhr, ajaxOptions, thrownError) {
							console.log('error...', xhr);
							//error logging
						},
					});
				}
				
			});
		});
	}
	
	//add comment
	if ($('.icon-edit').length) {
		
		$('.icon-edit').click(function (e) {
			e.preventDefault();
			$('#comment_field').show();
			$('#send-comment').data('docid', $(this).data('docid'))
		});
		
		$('#send-comment').click(function (e) {
			e.preventDefault();
			
			let data = {
				action: 'send_comment',
				docID: $(this).data('docid'),
				userID: $(this).data('userid'),
				comment: $('#iwp_file_comment').val(),
				parent: $(this).data('parent-comment') ? $(this).data('parent-comment') : 0
			}
			
			$('#comment_field').hide();
			$.ajax({
				type: 'POST',
				url: iwp.ajaxURL,
				data: data,
				success: function (data) {
					if (data.success) {
						Swal.fire({
							icon: 'success',
							title: 'Success',
							text: data.data.message,
						})
						$('#iwp_file_comment').val('')
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Error',
							text: data.data.message,
						})
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log('error...', xhr);
					//error logging
				},
			});
			
		})
	}
	
	//registration fee
	if ($('#reg_fee').length) {
		$('input[name=iwp_currency]').change(function (e) {
			if ('eur' === $(this).data('currency')) {
				$('.ru').hide();
				$('.en').show();
				$('.ru input[type=checkbox]:checked').each(function (i) {
					$(this).prop('checked', false);
				});
				$('.ru .price span').text(0 + ' руб')
			} else {
				$('.ru').show();
				$('.en').hide();
				$('.en input[type=checkbox]:checked').each(function (i) {
					$(this).prop('checked', false);
				});
				$('.en .price span').text(0 + ' eur')
			}
		});
		
		
		$('.ru input[type=checkbox]').change(function (e) {
			let total = 0
			$('.ru input[type=checkbox]:checked').each(function (i) {
				total = total + Number($(this).val())
			});
			$('.ru .price span').text(total + ' руб')
		});
		
		$('.en input[type=checkbox]').change(function (e) {
			let total = 0
			$('.en input[type=checkbox]:checked').each(function (i) {
				total = total + Number($(this).val())
			});
			$('.en .price span').text(total + ' eur')
		})
		
		
		$('#send_requisites').click(function (e) {
			e.preventDefault();
			let el = $('[type=checkbox]:checked');
			let value = [];
			$.each(el, function (i, val) {
				if ('on' !== $(this).val()) {
					value.push({cost: $(this).val(), name: $(this).attr('id')});
				}
			});
			
			let data = {
				action: 'send_requisites',
				options: value,
				userID: $('[name=iwp_user_id]').val(),
			}
			
			if (value.length === 0) {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: "Choose one of the items above",
				});
			} else {
				$.ajax({
					type: 'POST',
					url: iwp.ajaxURL,
					data: data,
					success: function (data) {
						if (data.success) {
							Swal.fire({
								icon: 'success',
								title: 'Success',
								text: data.data.message,
							}).then((result) => {
								if (result.isConfirmed) {
									location.reload();
								}
							});
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: data.data.message,
							});
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log('error...', xhr);
						//error logging
					},
				});
			}
		});
		
		$('#send_check').click(function (e) {
			e.preventDefault();
			let formData = new FormData();
			
			formData.append('file', $('input[name=iwp_check]')[ 0 ].files[ 0 ]);
			formData.append('action', 'send_check');
			formData.append('userID', $('[name=iwp_user_id]').val());
			
			$.ajax({
				type: 'POST',
				url: iwp.ajaxURL,
				data: formData,
				contentType: false,
				async: true,
				cache: false,
				processData: false,
				timeout: 60000,
				success: function (data) {
					if (data.success) {
						Swal.fire({
							icon: 'success',
							title: 'Success',
							text: data.data.message,
						}).then((result) => {
							if (result.isConfirmed) {
								location.reload();
							}
						});
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: data.data.message,
						});
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log('error...', xhr);
					//error logging
				},
			});
		});
	}
	
	$('.repay-comment').click(function (e) {
		e.preventDefault();
		
		$(this).parent().find('.replay-comment-input').show();
	});
	
	$('.iwp_replay_comment_btn').click(function (e) {
		e.preventDefault();
		let data = {
			action: 'send_comment',
			docID: $(this).data('docid'),
			userID: $(this).data('userid'),
			comment: $('[name=iwp_replay_comment]').val(),
			parent: $(this).data('comment'),
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (data) {
				if (data.success) {
					Swal.fire({
						icon: 'success',
						title: 'Success',
						text: data.data.message,
					})
					$('[name=iwp_replay_comment]').val('')
					$(this).parent().find('.replay-comment-input').hide();
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Error',
						text: data.data.message,
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
		 
	})
	
});