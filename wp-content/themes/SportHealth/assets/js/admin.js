jQuery(document).ready(($) => {
	/**
	 * Payment Docs Request page
	 */
	$('.action-btn').click(function (e) {
		e.preventDefault();
		
		let data = {
			action: 'action_payment_doc',
			userID: $(this).data('userid'),
			userAction: $(this).parent().parent().find('.form-select').val()
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (data) {
				if (data.success) {
					Swal.fire({
						icon: 'success',
						title: 'Success',
						text: data.data.message,
					}).then((result) => {
						if (result.isConfirmed) {
							location.reload();
						}
					});
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: data.data.message,
					});
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
	});
	
	/**
	 * Report Docs Page
	 */
	let flag = false;
	$('#report-doc tr').click(function (e) {
		if ($(this).data('doc_comment') !== 0) {
			if (!flag) {
				let id = $(this).data('doc');
				let slector = '[data-doc-id=' + id + ']';
				$(slector).show();
				flag = !flag;
			} else {
				flag = !flag;
				let id = $(this).data('doc');
				let slector = '[data-doc-id=' + id + ']';
				$(slector).hide();
			}
		}
	});
	
	$('.repay-comment').click(function (e) {
		e.preventDefault();
		$(this).parent().find('.replay-comment-input').show();
	})
	
	$('.iwp_replay_comment_btn').click(function (e) {
		e.preventDefault();
		let data = {
			action: 'send_replay_comment',
			commentID: $(this).data('comment'),
			comment: $(this).parent().find('.form-control').val(),
			docID: $(this).parent().parent().parent().parent().parent().data('doc-id'),
			userID: $(this).parent().parent().parent().parent().parent().data('userid')
		}
		let el = $(this).parent().parent().parent().parent().parent();
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (data) {
				if (data.success) {
					el.hide();
					Swal.fire({
						icon: 'success',
						title: 'Success',
						text: data.data.message,
					}).then((result) => {
						if (result.isConfirmed) {
							location.reload();
						}
					});
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: data.data.message,
					});
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			}
		});
	})
	
	$('.action-report-btn').click(function (e) {
		e.preventDefault();
		
		let data = {
			action: 'change_status_doc_report',
			docID: $(this).data('docid'),
			status: $(this).parent().parent().find('.form-select').val(),
			userID: $(this).data('userid'),
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (data) {
				if (data.success) {
					Swal.fire({
						icon: 'success',
						title: 'Success',
						text: data.data.message,
					}).then((result) => {
						if (result.isConfirmed) {
							location.reload();
						}
					});
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: data.data.message,
					});
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
	});
	
	/**
	 * User List of Conference Page
	 */
	let pageFlag = false;
	$('.edit-btn').click(function (e) {
		if ($(this).data('userid') !== 0) {
			if (!pageFlag) {
				let id = $(this).data('userid');
				let slector = '[data-uid=' + id + ']';
				$(slector).show();
				pageFlag = !pageFlag;
			} else {
				pageFlag = !pageFlag;
				let id = $(this).data('userid');
				let slector = '[data-uid=' + id + ']';
				$(slector).hide();
			}
		}
	});
	
	$('.save-user-info').click(function (e) {
		e.preventDefault();
		
		let data = {
			action: 'update_user_data',
			userID: $(this).parent().parent().parent().parent().data('uid'),
			fio_ru: $(this).parent().parent().parent().find('[id^=iwp_fio_ru-]').val(),
			fio_en: $(this).parent().parent().parent().find('[id^=iwp_fio_en-]').val(),
			dob: $(this).parent().parent().parent().find('[id^=iwp_dob-]').val(),
			gender: $(this).parent().parent().parent().find('[id^=iwp_gender-]').val(),
			appeal: $(this).parent().parent().parent().find('[id^=iwp_appeal-]').val(),
			country: $(this).parent().parent().parent().find('[id^=iwp_country-]').val(),
			province: $(this).parent().parent().parent().find('[id^=iwp_province-]').val(),
			city: $(this).parent().parent().parent().find('[id^=iwp_city-]').val(),
			phone: $(this).parent().parent().parent().find('[id^=iwp_phone-]').val(),
			email: $(this).parent().parent().parent().find('[id^=iwp_email-]').val(),
			work: $(this).parent().parent().parent().find('[id^=iwp_work-]').val(),
			organization: $(this).parent().parent().parent().find('[id^=iwp_organization-]').val(),
			degree: $(this).parent().parent().parent().find('[id^=iwp_degree-]').val(),
			position: $(this).parent().parent().parent().find('[id^=iwp_position-]').val(),
			article_title: $(this).parent().parent().parent().find('[id^=iwp_article_title-]').val(),
			coauthors: $(this).parent().parent().parent().find('[id^=iwp_coauthors-]').val(),
			congress_section: $(this).parent().parent().parent().find('[id^=iwp_congress_section-]').val(),
			form_participation: $(this).parent().parent().parent().find('[id^=iwp_form_participation-]').val(),
			form_performance: $(this).parent().parent().parent().find('[id^=iwp_form_performance-]').val(),
			technical: $(this).parent().parent().parent().find('[id^=iwp_technical-]').val(),
			technical_list: $(this).parent().parent().parent().find('[id^=iwp_technical_list-]').val(),
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (data) {
				if (data.success) {
					el.hide();
					Swal.fire({
						icon: 'success',
						title: 'Success',
						text: data.data.message,
					}).then((result) => {
						if (result.isConfirmed) {
							location.reload();
						}
					});
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: data.data.message,
					});
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
		
	});
	
	if ($('.clean').length) {
		$('.clean').click(function (e) {
			e.preventDefault();
			location.href = '/wp-admin/admin.php?page=iwp-user-list';
		});
	}
	
	/**
	 * Generate User List XLSX doc.
	 */
	$('.generate-exel-user-list').click(function (e) {
		e.preventDefault();
		let data = {
			action: 'generate_exel_user_list',
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (res) {
				// do something with ajax data
				console.log(res)
				if (res.success) {
					Swal.fire({
						title: '<strong>Download File</strong>',
						icon: 'info',
						html:
							'You can Download File, ' +
							`<a href="${res.data.url}">links</a> `,
						showCloseButton: true,
						focusConfirm: false,
						confirmButtonText:
							'<i class="fa fa-thumbs-up"></i> Great!',
						confirmButtonAriaLabel: 'Thumbs up, great!',
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			}
		});
	});
	
	/**
	 * Generate Payment Doc XLSX doc.
	 */
	$('.generate-exel-payment-doc').click(function (e) {
		e.preventDefault();
		let data = {
			action: 'generate_exel_doc_payment',
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (res) {
				// do something with ajax data
				console.log(res)
				if (res.success) {
					Swal.fire({
						title: '<strong>Download File</strong>',
						icon: 'info',
						html:
							'You can Download File, ' +
							`<a href="${res.data.url}">links</a> `,
						showCloseButton: true,
						focusConfirm: false,
						confirmButtonText:
							'<i class="fa fa-thumbs-up"></i> Great!',
						confirmButtonAriaLabel: 'Thumbs up, great!',
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			}
		});
	});
	
	$('.generate-exel-reports-doc').click(function (e) {
		e.preventDefault();
		let data = {
			action: 'generate_exel_reports_doc',
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (res) {
				// do something with ajax data
				console.log(res)
				if (res.success) {
					Swal.fire({
						title: '<strong>Download File</strong>',
						icon: 'info',
						html:
							'You can Download File, ' +
							`<a href="${res.data.url}">links</a> `,
						showCloseButton: true,
						focusConfirm: false,
						confirmButtonText:
							'<i class="fa fa-thumbs-up"></i> Great!',
						confirmButtonAriaLabel: 'Thumbs up, great!',
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			}
		});
	});
	
});