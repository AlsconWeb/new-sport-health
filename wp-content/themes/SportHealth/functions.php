<?php
/**
 * Created 20.05.2021
 * Version 1.0.1
 * Last update 30.06.21
 * Author: Alex L
 *
 */


use IWP\Admin\IWPAdminInit;
use IWP\CTP\IWPArchiveCongress;
use IWP\CTP\IWPOrganizers;
use IWP\Customize\IWPAddOptionsCustomize;
use IWP\Handlers\IWPPartnerFrom;
use IWP\User\IWPUserAvatar;
use IWP\User\IWPUserDocs;
use IWP\User\IWPUserProfileFrom;
use IWP\User\IWPUserRegistrations;
use IWP\User\IWPUserRole;
use IWP\wpbkComponents\CongressFirstBlock\IWPCongressFirstBlock;
use IWP\wpbkComponents\ContactAndMap\IWPContactAndMap;
use IWP\wpbkComponents\ContactInfo\IWPContactInfo;
use IWP\wpbkComponents\ExhibitionStands\IWPExhibitionStands;
use IWP\wpbkComponents\InfoBlock\IWPInfoBlock;
use IWP\wpbkComponents\ListingHeadline\IWPListingHeadline;
use IWP\wpbkComponents\mainBanner\IWPMainBanner;
use IWP\wpbkComponents\MemberCountries\IWPMemberCountries;
use IWP\wpbkComponents\organizersBlock\IWPOrganizersBlock;
use IWP\wpbkComponents\organizersConference\organizersConference;
use IWP\wpbkComponents\Packages\IWPPackages;
use IWP\wpbkComponents\partners\IWPPartners;
use IWP\wpbkComponents\PartnersForm\IWPPartnersForm;
use IWP\wpbkComponents\patronageBlock\IWPPatronage;
use IWP\wpbkComponents\ProgramAndActivities\IWPProgramAndActivities;
use IWP\wpbkComponents\ScientificProgram\IWPScientificProgram;
use IWP\wpbkComponents\Slider\IWPSlider;
use IWP\wpbkComponents\VisaForm\IWPVisaForm;


/**
 * Add Composer Autoload PSR-4
 */
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Get control version file
 *
 * @param $src
 *
 * @return false|int
 */
function version( $src ) {
	return filemtime( get_stylesheet_directory() . $src );
}

/**
 * Add Script and Style
 */
add_action( 'wp_enqueue_scripts', 'addScriptsThemes' );
function addScriptsThemes() {
	wp_enqueue_script( 'datepicker', get_stylesheet_directory_uri() . '/assets/js/bootstrap-datepicker.min.js', [ 'jquery' ], version( '/assets/js/bootstrap-datepicker.min.js' ), true );
	wp_enqueue_script( 'build', get_stylesheet_directory_uri() . '/assets/js/build.js', [ 'jquery' ], version( '/assets/js/build.js' ), true );
	wp_enqueue_script( 'bootstrap-datepicker.ru.min', get_stylesheet_directory_uri() . '/assets/locales/bootstrap-datepicker.ru.min.js', [ 'jquery' ], version( '/assets/locales/bootstrap-datepicker.ru.min.js' ), true );
	wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/assets/js/main.js', [
		'build',
		'sweetalert2',
	], version( '/assets/js/main.js' ), true );
	if( ! is_front_page() ) {
		wp_enqueue_script( 'gmap', '//maps.googleapis.com/maps/api/js?key=AIzaSyDoFR4O2S0D5Uebjtxa9hCQ3bx2oiIhZz4&amp', [ 'jquery' ], '', true );
	}
	wp_enqueue_script( 'sweetalert2', 'https://cdn.jsdelivr.net/npm/sweetalert2@11.0.12/dist/sweetalert2.all.min.js', [ 'jquery' ], '', true );
	wp_enqueue_script( 'fancybox', get_stylesheet_directory_uri() . '/assets/js/jquery.fancybox.min.js', [ 'jquery' ],
		version( '/assets/js/jquery.fancybox.min.js' ), true );
	
	wp_localize_script( 'main', 'iwp', [
		'ajaxURL' => admin_url( 'admin-ajax.php' ),
	] );
	
	wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/assets/css/main.css', '', version( '/assets/css/main.css' ) );
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css', '', version( '/style.css' ) );
	wp_enqueue_style( 'fancybox', get_stylesheet_directory_uri() . '/assets/css/jquery.fancybox.min.css', '', version( '/assets/css/jquery.fancybox.min.css' ) );
	wp_enqueue_style( 'datepicker', get_stylesheet_directory_uri() . '/assets/css/bootstrap-datepicker.min.css', '', version( '/assets/css/bootstrap-datepicker.min.css' ) );
	wp_enqueue_style( 'standalone.min.css', get_stylesheet_directory_uri() . '/assets/css/bootstrap-datepicker3.standalone.min.css', '', version( '/assets/css/bootstrap-datepicker3.standalone.min.css' ) );
}

/**
 * Init Themes
 */
add_action( 'init', 'initThemes' );
function initThemes() {
	// register menu
	register_nav_menus( [
		'top_header_menu' => __( 'Top Header Menu', 'iwp' ),
		'footer_menu'     => __( 'Footer Menu', 'iwp' ),
	] );
	
	// remove empty P elements
	remove_filter( 'the_excerpt', 'wpautop' );
	
	//media size
	add_image_size( 'organization', 540, 450, false );
	add_image_size( 'gallery', 356, 221, false );
	add_image_size( 'avatar', 230, 230, false );
	
	//CTP
	$organizers        = new IWPOrganizers();
	$archiveConference = new IWPArchiveCongress();
	
	//form Handlers
	$visaForm     = new IWP\Handlers\IWPVisaForm();
	$partnerFrom  = new IWPPartnerFrom();
	$userRegister = new IWPUserRegistrations();
	
	//User
	$role         = new IWPUserRole();
	$uploadAvatar = new IWPUserAvatar();
	$profileFrom  = new IWPUserProfileFrom();
	$userDoc      = new IWPUserDocs();
	
	//Admin panel
	$adminInit = new IWPAdminInit();
	
	// hide admin bar if is no admin
	if( ! current_user_can( 'manage_options' ) ) {
		add_filter( 'show_admin_bar', '__return_false' );
	}
	
	// redirect to account page if is no admin
	if( is_admin() && ! current_user_can( 'manage_options' ) && ! wp_doing_ajax() ) {
		wp_redirect( get_permalink( get_option( 'iwp_account_page', true ) ), 301 );
	}
}


/**
 * Add Support SVG
 *
 * @param $file_types
 *
 * @return array
 */
add_filter( 'upload_mimes', 'addSupportSVG' );
function addSupportSVG( $file_types ): array {
	$new_filetypes        = [];
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types           = array_merge( $file_types, $new_filetypes );
	
	return $file_types;
}

/**
 * Init WP Bakery Components
 */
add_action( 'vc_before_init', 'jwaAddComponents' );
function jwaAddComponents() {
	$mainBanner           = new IWPMainBanner();
	$slider               = new IWPSlider();
	$organizers           = new IWPOrganizersBlock();
	$patronage            = new IWPPatronage();
	$partners             = new IWPPartners();
	$organizersConference = new organizersConference();
	$congressFirstBlock   = new IWPCongressFirstBlock();
	$infoBlock            = new IWPInfoBlock();
	$membersCountry       = new IWPMemberCountries();
	$programAndActivities = new IWPProgramAndActivities();
	$scientificProgram    = new IWPScientificProgram();
	$visaForm             = new IWPVisaForm();
	$contactInfo          = new IWPContactInfo();
	$packages             = new IWPPackages();
	$partnersForm         = new IWPPartnersForm();
	$listingHeadline      = new IWPListingHeadline();
	$contactAndMap        = new IWPContactAndMap();
	$exhibitionStands     = new IWPExhibitionStands();
}

/**
 * Add to Customize
 *
 * @param $wp_customize
 */
add_action( 'customize_register', 'addCustomize' );
function addCustomize( $wp_customize ) {
	$customize = new IWPAddOptionsCustomize();
}

/**
 * Add Russian Localization
 */
add_action( 'after_setup_theme', 'addTextDomain' );
function addTextDomain() {
	load_theme_textdomain( 'iwp', get_template_directory() . '/languages' );
}

/**
 * Add Custom Switcher WPML
 */
if( function_exists( 'icl_get_languages' ) ) {
	function getLanguagesSwitcher() {
		$languages = icl_get_languages( 'skip_missing=0&orderby=code' );
		if( ! empty( $languages ) ) {
			ob_start();
			$activeLang = [];
			foreach ( $languages as $language ) {
				if( $language['active'] ) {
					$activeLang = $language;
				}
			}
			
			?>
			
			<div class="language <?php echo 'ru' === ICL_LANGUAGE_CODE ? '' : 'checked' ?>"
			     data-before="Ru"
			     data-after="En"
			     data-active_lang="<?php echo $activeLang['code']; ?>"
			     data-url_en="<?php echo $languages['en']['url']; ?>"
			     data-url_ru="<?php echo $languages['ru']['url']; ?>"
			>
				<input id="language" type="checkbox" <?php echo 'ru' === ICL_LANGUAGE_CODE ? '' : 'checked' ?>>
				<label for="language"> </label>
			</div>
			<?php
			return ob_get_clean();
		}
	}
}

/**
 * Hide Default register from
 */
add_action( 'init', 'redirectDefaultRegisterPage' );
function redirectDefaultRegisterPage() {
	$login_page  = home_url( '/' );
	$page_viewed = basename( $_SERVER['REQUEST_URI'] );
	
	if( $page_viewed == "wp-login.php?action=register" && $_SERVER['REQUEST_METHOD'] == 'GET' ) {
		wp_redirect( $login_page );
		exit;
	}
}

/**
 * Redirect login fail
 */
add_action( 'wp_login_failed', 'login_failed' );
function login_failed() {
	$url        = 'ru' === ICL_LANGUAGE_CODE ? '/vojti/' : '/en/login/';
	$login_page = home_url( $url );
	wp_redirect( $login_page . '?error=Login+Fail' );
	exit;
}

