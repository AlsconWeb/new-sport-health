<?php
/**
 * Created 06.06.2021
 * Version 1.0.1
 * Last update 01.07.21
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * Template Name: Login page
 */


get_header();
?>
<section>
	<?php if ( have_posts() ): ?>
		<?php while ( have_posts() ):the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		<form class="details login" name="loginform" method="post" action="<?php bloginfo( 'url' ); ?>/wp-login.php"
		id="register_form">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="dfr">
							<h2 class="title"><?php echo _e( 'Login', 'iwp' ) ?></h2>
							<div class="input">
								<label><?php _e( 'Email', 'iwp' ); ?></label>
								<input type="text" name="log" id="iwp_email">
							</div>
							<div class="input">
								<label><?php _e( 'Password', 'iwp' ); ?></label>
								<input type="password" name="pwd" id="iwp_password">
							</div>
							<input type="hidden" name="action" value="iwp_login_form"/>
							<?php wp_nonce_field( 'iwp_login_form', 'iwp_login_form_nonce' ); ?>
							<input class="button" type="submit" id="submit_from" value="<?php _e( 'Login', 'iwp' ); ?>">
							<input type="hidden" name="redirect_to" id="iwp_to_redirect"
							value="<?php echo get_permalink( get_option( 'iwp_account_page', true ) ); ?>">
							<div class="dfr">
								<a
								href="<?php echo 'ru' === ICL_LANGUAGE_CODE ? get_permalink( get_option( 'iwp_forget_password_page',
								true ) ) : get_permalink( get_option( 'iwp_forget_password_page_en', true ) ); ?>"><?php _e( 'Forgot my password', 'iwp' );
									?></a>
								| <a
								href="<?php echo 'ru' === ICL_LANGUAGE_CODE ? get_permalink( get_option( 'iwp_register_page', true
								) ) : get_permalink( get_option( 'iwp_register_page_en', true ) ); ?>">
									<?php _e( 'Create Account', 'iwp' );
									?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php endif; ?>
</section>
<?php get_footer(); ?>
