<?php
/**
 * Created 06.06.2021
 * Version 1.0.1
 * Last update 01.07.2021
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * Template Name: Register Form
 */

use IWP\Helpers\IWPBakeryHelpers;

get_header();

$helpers = new IWPBakeryHelpers();
?>
<section>
	<?php if ( have_posts() ): ?>
		<?php while ( have_posts() ):the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		<form class="details register" method="post" action="<?php echo admin_url( 'admin-post.php' ); ?>"
		id="register_form">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="dfr">
							<h2 class="title"><?php echo _e( 'Registration', 'iwp' ) ?></h2>
							<div class="dfr">
								<div class="input">
									<label><?php _e( 'First Name', 'iwp' ); ?><sub>*</sub></label>
									<input type="text" name="iwp_first_name" id="iwp_first_name">
								</div>
								<div class="input">
									<label><?php _e( 'Last Name', 'iwp' ); ?><sub>*</sub></label>
									<input type="text" name="iwp_last_name" id="iwp_last_name">
								</div>
								<div class="input">
									<label><?php _e( 'Patronymic', 'iwp' ); ?></label>
									<input type="text" name="iwp_userinfo[patronymic]" id="iwp_patronymic">
								</div>
								<div class="input">
									<label><?php _e( 'Email', 'iwp' ); ?><sub>*</sub></label>
									<input type="email" name="iwp_email" id="iwp_email" required>
								</div>
								<div class="input">
									<label><?php _e( 'Password', 'iwp' ); ?><sub>*</sub></label>
									<input type="password" name="iwp_password" id="iwp_password" required>
								</div>
								<div class="input">
									<label><?php _e( 'Password Confirm', 'iwp' ); ?><sub>*</sub></label>
									<input type="password" name="iwp_confirm" id="iwp_confirm" required>
								</div>
							</div>
							<h3 style="background:#2263A8;"><?php _e( 'PERSONAL DATA', 'iwp' ); ?></h3>
							<div class="dfr">
								<div class="input">
									<input type="text" name="iwp_userinfo[fio_en]" id="iwp_fio_en" required>
									<label><?php _e( 'Last name, first name in English', 'iwp' ); ?><sub>*</sub></label>
								</div>
								<div class="input icon-calendar">
									<label><?php _e( 'Date of Birth', 'iwp' ); ?><sub>*</sub></label>
									<input type="text" name="iwp_userinfo[dob]" id="iwp_dob" required>
								</div>
								<div class="select">
									<label><?php _e( 'Gender', 'iwp' ); ?></label>
									<select name="iwp_userinfo[gender]" id="iwp_gender">
										<option value="male"><?php _e( 'Male', 'iwp' ); ?></option>
										<option value="female"><?php _e( 'Female', 'iwp' ); ?></option>
									</select>
								</div>
								<div class="input">
									<input type="text" name="iwp_userinfo[appeal]" id="iwp_appeal">
									<label><?php _e( 'Appeal (Mr., Ms. or any other)', 'iwp' ); ?></label>
								</div>
							</div>
							<h3 style="background:#72C1AD;"><?php _e( 'Contact details', 'iwp' ); ?></h3>
							<div class="dfr">
								<div class="select">
									<label><?php _e( 'Country of Residence', 'iwp' ); ?><sub>*</sub></label>
									<select name="iwp_userinfo[country]" id="iwp_country" required>
										<?php $country = $helpers->countryList(); ?>
										<option value="0"><?php _e( "Select a you country" ); ?></option>
										<?php if ( $country ): ?>
											<?php foreach ( $country as $key => $item ): ?>
												<option value="<?php echo ICL_LANGUAGE_CODE === 'en' ? $key : $item['name_ru']; ?>"><?php echo
													ICL_LANGUAGE_CODE === 'en' ? $key : $item['name_ru']; ?></option>
											<?php endforeach; ?>
										<?php endif; ?>
									</select>
								</div>
								<div class="input">
									<input type="text" name="iwp_userinfo[province]" id="iwp_province" required>
									<label><?php _e( 'Province', 'iwp' ); ?><sub>*</sub></label>
								</div>
								<div class="input">
									<input type="text" name="iwp_userinfo[city]" id="iwp_city" required>
									<label><?php _e( 'City', 'iwp' ); ?><sub>*</sub></label>
								</div>
								<div class="input">
									<input type="tel" name="iwp_userinfo[phone]" id="iwp_phone"
									required>
									<label><?php _e( 'Contact number', 'iwp' ); ?><sub>*</sub></label>
									<p class="description"><?php _e( 'Format:', 'iwp' ); ?> 123-456-7890</p>
								</div>
							</div>
							<h3 style="background:#F97C55;"><?php _e( 'Data for performance', 'iwp' ); ?></h3>
							<div class="dfr">
								<div class="input">
									<input type="text" name="iwp_userinfo[work]" id="iwp_work" required>
									<label><?php _e( 'Place of work / study (in full)', 'iwp' ); ?><sub>*</sub></label>
								</div>
								<div class="input">
									<input type="text" name="iwp_userinfo[organization]" id="iwp_organization" required>
									<label><?php _e( 'Structural division of the organization (in full)', 'iwp' ) ?>
										<sub>*</sub></label>
								</div>
								<div class="input">
									<input type="text" name="iwp_userinfo[degree]" id="iwp_degree" required>
									<label><?php _e( 'Academic title / Degree / Honorary sports title', 'iwp' ); ?>
										<sub>*</sub></label>
								</div>
								<div class="input">
									<input type="text" name="iwp_userinfo[position]" id="iwp_position" required>
									<label><?php _e( 'Position (in full)', 'iwp' ); ?><sub>*</sub></label>
								</div>
								<div class="textarea">
									<label><?php _e( 'Paper title (in full)', 'iwp' ); ?><sub>*</sub></label>
									<textarea name="iwp_userinfo[article_title]" id="iwp_article_title" required></textarea>
								</div>
								<div class="textarea">
									<label><?php _e( 'Coauthors separated by commas (full name in Russian and English)', 'iwp' ); ?>
										<sub>*</sub></label>
									<textarea name="iwp_userinfo[coauthors]" id="iwp_coauthors" required></textarea>
								</div>
								<div class="input">
									<input type="text" name="iwp_userinfo[congress_section]" id="iwp_congress_section" required>
									<label><?php _e( 'Number and title of the Congress section', 'iwp' ); ?><sub>*</sub></label>
								</div>
								<div class="select">
									<label><?php _e( 'Form of participation', 'iwp' ); ?><sub>*</sub></label>
									<select type="text" name="iwp_userinfo[form_participation]" id="iwp_form_participation" required>
										<option value="0"><?php _e( 'Choose a Form of participation', 'iwp' ); ?></option>
										<option value="full_time"><?php _e( 'Full time', 'iwp' ); ?></option>
										<option value="extramural"><?php _e( 'In-absentia', 'iwp' ); ?></option>
										<option value="listener"><?php _e( 'Online', 'iwp' ); ?></option>
									</select>
								</div>
								<div class="select">
									<label><?php _e( 'Form of performance', 'iwp' ); ?></label>
									<select name="iwp_userinfo[form_performance]" id="iwp_form_performance">
										<option value="0"><?php _e( 'Choose a presentation format', 'iwp' ); ?></option>
										<option value="oral"><?php _e( 'Oral presentation', 'iwp' ); ?></option>
										<option value="poster"><?php _e( 'Poster presentation', 'iwp' ); ?></option>
									</select>
								</div>
								<div class="select">
									<label><?php _e( 'The need for technical means', 'iwp' ) ?></label>
									<select name="iwp_userinfo[technical]" id="iwp_technical">
										<option value="0"><?php _e( 'Select a technical', 'iwp' ); ?></option>
										<option value="yes"><?php _e( 'Yes', 'iwp' ); ?></option>
										<option value="no"><?php _e( 'No', 'iwp' ); ?></option>
									</select>
								</div>
								<div class="input">
									<input type="text" name="iwp_userinfo[technical_list]" id="iwp_technical_list">
									<label><?php _e( 'List of technical means', 'iwp' ); ?></label>
								</div>
							</div>
							<div class="checkbox">
								<input id="checkbox_agreement" type="checkbox" name="iwp_agreement" value="agreement" required>
								<?php if ( 'en' === ICL_LANGUAGE_CODE ): ?>
									<label for="checkbox_agreement">I agree to the processing of my personal data in accordance with the
										<a
										href="/en/soglasie-na-obrabotku-personalnyh-dannyh/">Privacy
											Policy
											Terms</a></label>
								<?php else: ?>
									<label for="checkbox_agreement">Соглашаюсь на обработку моих персональных данных в соответствии с <a
										href="/soglasie-na-obrabotku-personalnyh-dannyh/">Условиями политики
											конфиденциальности</a></label>
								<?php endif; ?>
							</div>
							<input type="hidden" name="action" value="iwp_register_form"/>
							<input type="hidden" name="iwp_to_email" value="<?php echo get_option( 'admin_email' ) ?>"/>
							<input type="hidden" id="iwp_to_redirect" name="iwp_to_redirect"
							value="<?php echo get_permalink( get_option( 'iwp_account_page', true ) ); ?>"/>
							<?php wp_nonce_field( 'iwp_register_form', 'iwp_register_form_nonce' ); ?>
							<input class="button" type="submit" id="submit_from" value="<?php _e( 'Submit', 'iwp' ); ?>">
							<div class="dfr">
								<a
								href="<?php echo 'ru' === ICL_LANGUAGE_CODE ? get_permalink( get_option( 'iwp_forget_password_page', true ) ) : get_permalink( get_option( 'iwp_forget_password_page_en', true ) ); ?>"><?php _e( 'Forgot my password', 'iwp' );
									?></a>
								<?php
								?>
								| <a
								href="<?php echo 'ru' === ICL_LANGUAGE_CODE ? get_permalink( get_option( 'iwp_account_page', true ) )
								: get_permalink( get_option( 'iwp_account_page_en', true ) );
								?>"><?php _e( 'Login', 'iwp' );
									?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php endif; ?>
</section>
<?php get_footer(); ?>
