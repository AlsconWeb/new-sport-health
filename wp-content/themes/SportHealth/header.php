<?php
/**
 * Created 20.05.2021
 * Version 1.0.1
 * Last update 01.07.21
 * Author: Alex L
 *
 */

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<title><?php bloginfo( 'name' ); ?>| <?php bloginfo( 'description' ); ?></title>
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&amp;display=swap" rel="stylesheet">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-5JQY0GSRXK"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-5JQY0GSRXK');
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class( ! is_front_page() && ! is_singular( 'organizers' ) ? 'inner-page' : '' ); ?>>
<header>
	<div class="container">
		<div class="row row-cols-auto align-items-center">
			<div class="col">
				<a class="logo" href="<?php bloginfo( 'url' ); ?>">
					<?php if( is_front_page() || is_singular( 'organizers' ) ): ?>
						<img src="<?php echo get_theme_mod( 'customizer_logo' ); ?>" alt="Logo">
					<?php else: ?>
						<img src="<?php echo get_theme_mod( 'customizer_logo_white' ); ?>" alt="Logo">
					<?php endif; ?>
				</a>
			</div>
			<div class="col">
				<a class="button dfc"
				   href="<?php echo 'ru' === ICL_LANGUAGE_CODE ? get_permalink( get_option( 'iwp_account_page', true ) )
					   : get_permalink( get_option( 'iwp_account_page_en', true ) );
				   ?>"><?php _e( 'Account page', 'iwp' ); ?></a>
			</div>
			<div class="col">
				<div class="burger"><span></span><span></span><span> </span></div>
				<div class="menu-block">
					<div class="menu">
						<?php wp_nav_menu( [
							'theme_location' => 'top_header_menu',
							'container'      => '',
							'menu_class'     => '',
							'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						] ); ?>
					</div>
				</div>
			</div>
			<div class="col">
				<?php echo getLanguagesSwitcher(); ?>
			</div>
		</div>
	</div>
</header>