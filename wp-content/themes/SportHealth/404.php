<?php
/**
 * Created 27.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

get_header()
?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="title"><?php _e( 'Page not found', 'iwp' ); ?></h2>
				<h1 class="title"><?php _e( 'Error 404', 'iwp' ); ?></h1>
				<a class="btn icon-arrow-right"
				   href="<?php bloginfo( 'url' ); ?>"><?php _e( 'Go back to the main page', 'iwp' ); ?></a>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
